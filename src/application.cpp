/****************************************************************************
** $Id:  qt/application.cpp   3.0.5   edited Jun 5 14:13 $
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of an example program for Qt.  This example
** program may be used, distributed and modified without limitation.
**
*****************************************************************************/

#include "application.h"

#include <qfile.h>
#include <qfiledialog.h>
#include <qworkspace.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qtoolbar.h>
#include <qtoolbutton.h>
#include <qpopupmenu.h>
#include <qmenubar.h>
#include <qmovie.h>
#include <qmultilineedit.h>
#include <qfile.h>
#include <qfiledialog.h>
#include <qlabel.h>
#include <qstatusbar.h>
#include <qmessagebox.h>
#include <qprinter.h>
#include <qapplication.h>
#include <qpushbutton.h>
#include <qaccel.h>
#include <qtextstream.h>

#include <qpainter.h>
#include <qpaintdevicemetrics.h>
#include <qwhatsthis.h>
#include <qobjectlist.h>
#include <qvbox.h>
#include <qaction.h>
#include <kdatepicker.h>


//template<class type>
  
/*class MojaPtrLista<CastMembers>;
class MojaPtrLista<Elements>;
class MojaPtrLista<Stunts>;
class MojaPtrLista<PayElements>;    */

const char * fileOpenText = "Click this button to open a <em>new file</em>. <br><br>"
"You can also select the <b>Open command</b> from the File menu.";
const char * fileSaveText = "Click this button to save the file you are "
"editing.  You will be prompted for a file name.\n\n"
"You can also select the Save command from the File menu.\n\n"
"Note that implementing this function is left as an exercise for the reader.";
const char * filePrintText = "Click this button to print the file you "
"are editing.\n\n"
"You can also select the Print command from the File menu.";

ApplicationWindow::ApplicationWindow()
    : SchWin( 0, "Linux Scheduling 2005", WDestructiveClose )
{   //for(int i=0;i<1000000;i++)
       // for(int j=0;j<100;j++);

    int id;
   // stripBoard=0; ne treba posto je guarded pointer
    flagElm=false;
    flagBrkdwn=false;
    flagStrip=false;
    p_brk_sh=0;
    //Formiranje radnog prostora WS-Workspace
     QVBox* vb = new QVBox( this );
    vb->setFrameStyle( QFrame::StyledPanel | QFrame::Sunken );
    ws = new QWorkspace( vb );
    ws->setScrollBarsEnabled( TRUE );
    setCentralWidget( vb );

    statusBar()->message( "Ready", 2000 );
    
    QPixmap openIcon, saveIcon;
    
    fileTools = new QToolBar( this, "file operations" );
    addToolBar( fileTools, tr( "File Operations" ), DockLeft, TRUE );

    QPixmap newIcon( "./images/new1.png");
    /*QToolButton * fileNew
	= new QToolButton( newIcon, "New File", QString::null,
			   this, SLOT(newDoc()), fileTools, "new document" );   */
    QToolButton * fileNew= new QToolButton(fileTools,"new document");
    fileNew->setTextLabel("New File");
    fileNew->setIconSet(newIcon);
    connect(fileNew,SIGNAL(clicked()),this,SLOT(newDoc()));

    openIcon = QPixmap( "./images/open.png" );
   /* QToolButton * fileOpen
	= new QToolButton( openIcon, "Open File", QString::null,
			   this, SLOT(load()), fileTools, "open file" );*/
    QToolButton * fileOpen= new QToolButton(fileTools,"open file");
    fileOpen->setTextLabel("Open File");
    fileOpen->setIconSet(openIcon);
    connect(fileOpen,SIGNAL(clicked()),this,SLOT(fileOpen()));


    saveIcon = QPixmap( "./images/save.png" );
    /*QToolButton * fileSave
	= new QToolButton( saveIcon, "Save File", QString::null,
			   this, SLOT(save()), fileTools, "save file" );    */
               QToolButton * fileSave= new QToolButton(fileTools,"save file");
    fileSave->setTextLabel("Save File");
    fileSave->setIconSet(saveIcon);
    connect(fileSave,SIGNAL(clicked()),this,SLOT(save()));

#ifndef QT_NO_PRINTER
    printer = new QPrinter;
    QPixmap printIcon;

    printIcon = QPixmap( "./images/fileprint.png" );
    QToolButton * filePrint
	= new QToolButton( printIcon, "Print File", QString::null,
			   this, SLOT(print()), fileTools, "print file" );
     fileTools->addSeparator();
     
      QPixmap showbrkIcon=QPixmap("./images/showbrk.png");
    /* QToolButton* shbrkButtom
  = new QToolButton(showbrkIcon,"Show Breakdownsheet","",this,SLOT(showBrkdSh()), fileTools, "show breakdownsheet");*/
      QToolButton * shbrkButton= new QToolButton(fileTools,"show breakdownsheet");
    shbrkButton->setTextLabel("Show Breakdownsheet");
    shbrkButton->setIconSet(showbrkIcon);
    connect(shbrkButton,SIGNAL(clicked()),this,SLOT(openBrkdSh()));

     QPixmap elmanIcon=QPixmap("./images/elman.png");
     /*QToolButton* elmanButtom
  = new QToolButton(elmanIcon,"Open Element Manager","",this,SLOT(breakdownElMan()), fileTools, "open elmform"); */
  QToolButton * elmanButtom= new QToolButton(fileTools,"open elmform");
    elmanButtom->setTextLabel("Open Element Manager");
    elmanButtom->setIconSet(elmanIcon);
    connect(elmanButtom,SIGNAL(clicked()),this,SLOT(breakdownElMan()));

    //strip manager toolbutton
     QPixmap stripIcon=QPixmap("./images/strip.png");
      QToolButton * stripButton= new QToolButton(fileTools,"open stripboard");
    stripButton->setTextLabel("Open Stripboard");
    stripButton->setIconSet(stripIcon);
    connect(stripButton,SIGNAL(clicked()),this,SLOT(openStrip()));

  
    QWhatsThis::add( filePrint, filePrintText );
#endif

   (void)QWhatsThis::whatsThisButton( fileTools );

    QWhatsThis::add( fileOpen, fileOpenText );
    QWhatsThis::add( fileSave, fileSaveText );

    QPopupMenu * file = new QPopupMenu( this );
    menuBar()->insertItem( "&File", file );

    file->insertItem( "&New", this, SLOT(newDoc()), CTRL+Key_N );

    id = file->insertItem( openIcon, "&Open...",
			   this, SLOT(fileOpen()), CTRL+Key_O );
    file->setWhatsThis( id, fileOpenText );

    id = file->insertItem( saveIcon, "&Save",
			   this, SLOT(save()), CTRL+Key_S );
    file->setWhatsThis( id, fileSaveText );
    id = file->insertItem( "Save &As...", this, SLOT(saveAs()) );
    file->setWhatsThis( id, fileSaveText );     
#ifndef QT_NO_PRINTER
    file->insertSeparator();
    id = file->insertItem( printIcon, "&Print...",
			   this, SLOT(print()), CTRL+Key_P );
    file->setWhatsThis( id, filePrintText );
#endif
    file->insertSeparator();
    file->insertItem( "&Close", this, SLOT(closeWindow()), CTRL+Key_W );
    file->insertItem( "&Quit", qApp, SLOT( closeAllWindows() ), CTRL+Key_Q );

//Breakdown meni
	breakdownMenu = new QPopupMenu( this );
	//menuBar()->insertItem( trUtf8( "&Breakdown" ), breakdownMenu );

	menuBar()->insertItem(  "&Breakdown" , breakdownMenu );
	brkID=breakdownMenu->insertItem("&Breakdown Sheet",this,SLOT(showBrkdSh()), CTRL+Key_B);
	elmanID=breakdownMenu->insertItem("&Element Manager",this, SLOT(breakdownElMan()), CTRL+Key_E);
	//if ( ws->windowList().isEmpty() ) {
	breakdownMenu->setItemEnabled( brkID, FALSE );
	breakdownMenu->setItemEnabled( elmanID, FALSE);
	// }
	connect( ws, SIGNAL(windowActivated(QWidget*)), this, SLOT(brk_aboutToShow() ));




//Schedule meni
	scheduleMenu = new QPopupMenu(this);
	menuBar()->insertItem(trUtf8("&Schedule"),scheduleMenu);
	stripID=scheduleMenu->insertItem("Stripboard",this,SLOT(openStrip()));
    scheduleMenu->setItemEnabled(stripID,false);
//Date meni
  calendarMenu = new QPopupMenu(this);
	menuBar()->insertItem(trUtf8("&Calendar"),calendarMenu);
	setSDID=calendarMenu->insertItem("Set Start Date",this,SLOT(openCalendar()));
    calendarMenu->setItemEnabled(setSDID,false);

//Windows meni

    windowsMenu = new QPopupMenu( this );
    windowsMenu->setCheckable( TRUE );
    connect( windowsMenu, SIGNAL( aboutToShow() ),
	     this, SLOT( windowsMenuAboutToShow() ) );
    menuBar()->insertItem( "&Windows", windowsMenu );
//Hellp meni

    menuBar()->insertSeparator();
    QPopupMenu * help = new QPopupMenu( this );
    menuBar()->insertItem( "&Help", help );

    help->insertItem( "&About", this, SLOT(about()), Key_F1);
    help->insertItem( "About &Qt", this, SLOT(aboutQt()));
    help->insertSeparator();
    help->insertItem( "What's &This", this, SLOT(whatsThis()), SHIFT+Key_F1);

   
}


ApplicationWindow::~ApplicationWindow()
{
#ifndef QT_NO_PRINTER
    delete printer;
#endif
}
void ApplicationWindow::load(const QString& filename)
{
    
    //fileOpen();
    QFile file( filename );
    if ( !file.open( IO_ReadOnly ) ) {
	statusBar()->message( QString( "Failed to load \'%1\'" ).
				arg( filename ), 2000 );
	return;
    }
    this->setCaption(filename);

    ////////////////////////////////////////////////////////////////////////////////////////
     QDomDocument doc( "LinSchedML" );
  if( !doc.setContent( &file ) )
  {
    QMessageBox::warning( this, "Loading", "Failed to load file." );
    file.close();
    return;
  }

  file.close();
  //newDoc();
  ucitajSched(doc);   
}
void ApplicationWindow::fileOpen()
{
    newDoc();
    QString filename = QFileDialog::getOpenFileName(QString::null, "Scheduledies (*.sch *.xml)", this, "file open", "Linux Schedule -- File Open" );
    if ( !filename.isEmpty() )
    load( filename );
    else
    statusBar()->message( "File Open abandoned", 2000 );
}
void ApplicationWindow::openCalendar()
{
 // QWidget* parent=this;
  WFlags wf = WDestructiveClose;
    wf |=WType_TopLevel;
  // wf |= WStyle_NoBorder;
  if ( !kDatePicker )
        {
           // popup1 = new QFrame( parent ,0, wf);
            //popup1->setFrameStyle( QFrame::WinPanel|QFrame::Raised );
            //popup1->resize(290,240);
            //kDatePicker = new KDatePicker(popup1,QDate::currentDate(),"kDatePicker" );

            //za laptop bez wf-ne postoji konstruktor sa wf
            kDatePicker = new KDatePicker(ws,QDate::currentDate(),"kDatePicker");
            
           // kDatePicker->setFrameStyle(QFrame::WinPanel);
            //kDatePicker = new KDatePicker(ws,QDate::currentDate(),"kDatePicker",wf );
            //kDatePicker->setCloseButton(true);
            kDatePicker->setFontSize(24);
            kDatePicker->setCaption("Calendar");
            
            kDatePicker->setGeometry( QRect( 100,100, 480, 400 ) );                       
            // kDatePicker->setFocus();
            kDatePicker->setFixedWidth(480);
            kDatePicker->setFixedHeight(400);
            //if(p_brk_sh) p_brk_sh->showNormal();
            //if(p_el_man) p_el_man->showNormal();
            kDatePicker->polish();
            kDatePicker->showNormal();
             kDatePicker->setFocus();
            // popup1->move(QCursor::pos() );
           // popup1->show();
            connect(kDatePicker,SIGNAL(dateSelected(QDate)),this,SLOT(setSheetsDate(QDate)) );
            //connect(kDatePicker,SIGNAL(dateEntered(QDate)),this,SLOT(setSheetsDate(QDate) ) );
            connect(kDatePicker,SIGNAL(dateSelected(QDate)),this,SLOT(setDateOnFrame() ) );
           
        }
  else kDatePicker->setFocus();


}
void ApplicationWindow::setDateOnFrame()
{
    if(p_brk_sh)
        p_brk_sh->setDateOnFrame();
    }
void ApplicationWindow::setSheetsDate(QDate date)
{
     //Sheet* c_sheet;

//postavljanje izabranog datuma na svim kreiranim sheetovima
// ako je sheet daybreak preskace se i datum se uvecava
// radi kao slot pri postavljanju datuma na kalendaru
        QPtrListIterator<Sheet> it_sheet(sheets_list);
               Sheet* c_sheet;
              it_sheet.toFirst();
               while((c_sheet=it_sheet.current())!=0)
                {
                    ++it_sheet;
                    
                    if(c_sheet->getDayBreak())
                    {
                       // QMessageBox::warning(this,QString::null,"getDay is TRUE");
                        date=date.addDays(1);
                        continue;
                    }
                    else c_sheet->setDate(date);
                    c_sheet->setGlobalDate(date);
                }
  // this->postaviDatumeZaElemente(false);
    if(stripBoard)
     stripBoard->refreshStrip();
          

}
void ApplicationWindow::setSheetsDate()
{

//postavljanje izabranog datuma na svim kreiranim sheetovima
// poziva se iz stripboardform kod insert i remove daybreak
     QDate date = sheets_list.getFirst()->getDate();
        QPtrListIterator<Sheet> it_sheet(sheets_list);
               Sheet* c_sheet;
              it_sheet.toFirst();
               while((c_sheet=it_sheet.current())!=0)
                {
                    ++it_sheet;

                    if(c_sheet->getDayBreak())
                    {
                        //QMessageBox::warning(this,QString::null,"getDay is TRUE");
                        date=date.addDays(1);
                        continue;
                    }
                    else c_sheet->setDate(date);
                    c_sheet->setGlobalDate(date);
                }
     if(stripBoard)
     stripBoard->refreshStrip();
    this->postaviDatumeZaElemente(true);
    if(p_el_man)
         p_el_man->initTable();
}
///////////////////////////////////////////////////////
//////////////////////////////////////////////////////
void ApplicationWindow::unsetflagElm()
{
	flagElm=false;
	}
void ApplicationWindow::unsetflagBrkDwn()
{
	flagBrkdwn=false;
    if(flagStrip)
        stripBoard->close();
	}
void ApplicationWindow::unsetflagStrip()
{
    flagStrip=false;
    if(p_brk_sh)
        p_brk_sh->setStripBoard(0);
    }
////////////////////////////////////////////////////
///////////////////////////////////////////////////
 ElManForm* ApplicationWindow::breakdownElMan()
{                
    if(flagElm){ p_el_man->show();
                p_el_man->setFocus();
                }
      else
      if(flagBrkdwn)
        {
	        //p_el_man = new ElManForm(&stunts_ptr_list,ws,0,WDestructiveClose);
          p_el_man = new ElManForm(ws,&sheets_list,this,0,WDestructiveClose);
 	        p_el_man->show();
	        flagElm=true;
          connect(p_el_man,SIGNAL(destroyed() ),this,SLOT( unsetflagElm() ) );
	        return p_el_man;
        }
  return p_el_man;
}
////////////////////////////////////
//Prikazivanje breakdown sheet-a

void ApplicationWindow::openBrkdSh()
{
                      
    if(flagBrkdwn)
    {
         p_brk_sh->initForm();
         p_brk_sh->setFocus();
         }
    else p_brk_sh=newDoc();
    return;
  }
void ApplicationWindow::showBrkdSh()
{

    if(flagBrkdwn)
    {
         p_brk_sh->initForm();
         p_brk_sh->showNormal();
         p_brk_sh->setFocus();
    }
}
BrkDwnShForm* ApplicationWindow::newDoc()
{
    if(flagBrkdwn)
    {
        p_brk_sh->setFocus();
        bool p;
	      p=saveAs();
	      if(p)
	        {
 		        p_brk_sh->close(true);
                p_el_man->close(true);
                flagElm=false;
		        flagBrkdwn=false;
		        newDoc();
	        }
	      else
	        {
		        //BrkDwnShForm* w = (BrkDwnShForm*)ws->activeWindow();
		        return p_brk_sh;
	        }
     }
    else
     {
     	  p_brk_sh = new BrkDwnShForm( ws,this,&sheets_list,0, WDestructiveClose );
         
            flagBrkdwn=true;
 	      connect( p_brk_sh, SIGNAL( message(const QString&, int) ), statusBar(), SLOT( message(const QString&, int )) );
    	  connect(p_brk_sh,SIGNAL(destroyed() ),this,SLOT( unsetflagBrkDwn() ) );
		    connect( p_brk_sh, SIGNAL(destroyed()), this, SLOT(setDisabledBrk()));
	      connect(this,SIGNAL(stripBoardIsOpened(StripBoardForm*)),p_brk_sh,SLOT(setStripBoard(StripBoardForm*)) ); 
	      p_brk_sh->setCaption("Breakdown Sheet");
	      p_brk_sh->setIcon( QPixmap("./images/new.png") );
    	  // show the very first window in maximized mode
    	  if ( ws->windowList().isEmpty() )
	          p_brk_sh->showMaximized();
    	  else
	          p_brk_sh->show(); 
	    return p_brk_sh;
    }
    return 0;

}

void ApplicationWindow::openStrip()
{
    if(!sheets_list.isEmpty())
    {
    if(flagBrkdwn)
        {
           if(flagStrip)
           {
             stripBoard->showNormal();
             stripBoard->setFocus();
           }
           else
           {
             stripBoard=new StripBoardForm(ws,p_brk_sh,0,WDestructiveClose);
             emit stripBoardIsOpened(stripBoard);
             stripBoard->showNormal();
             flagStrip=true;
             connect(stripBoard,SIGNAL(destroyed()),this,SLOT(unsetflagStrip()));
            // connect(this,SIGNAL(stripBoardIsOpened(StripBoardForm*)),p_brk_sh,SLOT(setStripBoard(StripBoardForm*)) );
            }
        }
    }
}
    


 void ApplicationWindow::save()
{
    // BrkDwnShForm* p_brk_sh = (BrkDwnShForm*)ws->activeWindow();
  //  if ( p_brk_sh )
	//p_brk_sh->save();
    QDomDocument document( "LinSchedML" );
    document=setXMLDocument(document);
  if ( filename.isEmpty() ) {
        saveAs();
        return;
    }


    QFile f( filename );
    if ( !f.open( IO_WriteOnly ) ) {
        emit message( QString("Could not write to %1").arg(filename),
                      2000 );
        return;
    }
    QTextStream text_stream( &f );


    text_stream<<document.toString();


    f.close();

    setCaption( filename );

    emit message( QString( "File %1 saved" ).arg( filename ), 2000 );

}


bool ApplicationWindow::saveAs()
{
    QString fn = QFileDialog::getSaveFileName( filename, QString::null, this );
    if ( !fn.isEmpty() )
    {
        filename = fn;
        save();
	return true;
    }
    else
    {
        emit message( "Saving aborted", 2000 );
 return false;
    }

}

void ApplicationWindow::print()
{
#ifndef QT_NO_PRINTER
    if(p_brk_sh)
        p_brk_sh->print( printer );
#endif
}


void ApplicationWindow::closeWindow()
{
        switch( QMessageBox::information( this, "Linux Schaduling",
                                      "Do you want to save the changes"
                                      " to the document?",
                                      "Yes", "No", "Cancel",
                                      0, 1 ) ) {
    case 0:
        save();
        
        sheets_list.current()->setNumOfSheets(1);
        sheets_list.current()->setGlobalDate(QDate(0,0,0));
        sheets_list.clear();
        if(p_brk_sh)
        {
	        p_brk_sh->close();

         }
         if(p_el_man)
         {
            p_el_man->close();
         }
         if(kDatePicker)
            kDatePicker->close();
           pocistiListe();
        break;
    case 1:
        
        sheets_list.current()->setNumOfSheets(1);
        sheets_list.current()->setGlobalDate(QDate(0,0,0));
        sheets_list.clear();
        if(p_brk_sh)
        {
	        p_brk_sh->close();

         }
         if(p_el_man)
         {
            p_el_man->close();
         }
         if(kDatePicker)
            kDatePicker->close();
           pocistiListe();
        break;
    case 2:
    default: // just for sanity
        break;
 }      
           
}

void ApplicationWindow::about()
{
    QMessageBox::about( this, "Qt Application Example",
			"This example demonstrates simple use of\n "
			"Qt's Multiple Document Interface (MDI).");
}


void ApplicationWindow::aboutQt()
{
    QMessageBox::aboutQt( this, "Qt Application Example" );
}

///////////////////////////////////////////////////////////////////////////
void ApplicationWindow::brk_aboutToShow()
{
    breakdownMenu->setItemEnabled( brkID, TRUE );
    breakdownMenu->setItemEnabled( elmanID, TRUE);
    scheduleMenu->setItemEnabled(stripID,true);
    calendarMenu->setItemEnabled(setSDID,true);

}
void ApplicationWindow::setDisabledBrk()
{
	breakdownMenu->setItemEnabled( brkID, FALSE );
	breakdownMenu->setItemEnabled( elmanID, FALSE);
}

//////////////////////////////////////////////////////////////////////////////////
void ApplicationWindow::windowsMenuAboutToShow()
{
    windowsMenu->clear();
    int cascadeId = windowsMenu->insertItem("&Cascade", ws, SLOT(cascade() ) );
    int tileId = windowsMenu->insertItem("&Tile", ws, SLOT(tile() ) );
    int horTileId = windowsMenu->insertItem("Tile &Horizontally", this, SLOT(tileHorizontal() ) );
    if ( ws->windowList().isEmpty() ) {
	windowsMenu->setItemEnabled( cascadeId, FALSE );
	windowsMenu->setItemEnabled( tileId, FALSE );
	windowsMenu->setItemEnabled( horTileId, FALSE );
    }
    windowsMenu->insertSeparator();
    QWidgetList windows = ws->windowList();
    for ( int i = 0; i < int(windows.count()); ++i ) {
	int id = windowsMenu->insertItem(windows.at(i)->caption(),
					 this, SLOT( windowsMenuActivated( int ) ) );
	windowsMenu->setItemParameter( id, i );
	windowsMenu->setItemChecked( id, ws->activeWindow() == windows.at(i) );
    }
}

void ApplicationWindow::windowsMenuActivated( int id )
{
    QWidget* w = ws->windowList().at( id );
    if ( w )
	w->showNormal();
    w->setFocus();
}

void ApplicationWindow::tileHorizontal()
{
    // primitive horizontal tiling
    QWidgetList windows = ws->windowList();
    if ( !windows.count() )
	return;

    int heightForEach = ws->height() / windows.count();
    int y = 0;
    for ( int i = 0; i < int(windows.count()); ++i ) {
	QWidget *window = windows.at(i);
	if ( window->testWState( WState_Maximized ) ) {
	    // prevent flicker
	    window->hide();
	    window->showNormal();
	}
	int preferredHeight = window->minimumHeight()+window->parentWidget()->baseSize().height();
	int actHeight = QMAX(heightForEach, preferredHeight);

	window->parentWidget()->setGeometry( 0, y, ws->width(), actHeight );
	y += actHeight;
    }
}
void ApplicationWindow::closeEvent( QCloseEvent* ce )
{
    ce->accept();

}
///////////////////////////////////////////////////////////////////////////////////////////////
////////RAD SA XML-om za ucitavanje i smestanje podataka na disk
///////////////////////////////////////////////////////////////////////////////////////////////
bool ApplicationWindow::ucitajSched(const QDomDocument& doc)
{
   int sh_num=0;
    QDomElement root = doc.documentElement();
  if( root.tagName() != "linsched" )
  {
   QMessageBox::warning( this, "Loading", "Invalid file." );
    return false;
  }

  pocistiListe();
  QDomNode n_root = root.firstChild();
    while( !n_root.isNull() )
  {


    QDomNode n_categ=n_root.firstChild();
    int i=0;
    int p=0;
    while(!n_categ.isNull() )
    {
        QDomNode n_elem=n_categ.firstChild();

        while(!n_elem.isNull())
        {

            QDomElement e = n_elem.toElement();
            if(!e.isNull())
            {

                if( e.tagName() == "cast" )
                {
                  // QMessageBox::warning(this,QString::null,"nasao castr tag");
                    CastMembers* cast=new CastMembers( e );
                    el_cast_ptr_list.append( cast );
                    //el_cast_ptr_list.inSort(cast);
                }
                if( e.tagName() == "payelem" )
                {
                   // QMessageBox::warning(this,QString::null,"nasao bact tag");
                    PayElPtrList* tmp_pel_ptr_list=0;
                    switch(p)
                    {
                        case 0: tmp_pel_ptr_list=&el_bact_ptr_list;
                                break;
                        case 1: tmp_pel_ptr_list=&el_props_ptr_list;
                                break;
                        case 2: tmp_pel_ptr_list=&el_speceff_ptr_list;
                                break;
                        case 3: tmp_pel_ptr_list=&el_setdress_ptr_list;
                                break;
                        case 4: tmp_pel_ptr_list=&el_sound_ptr_list;
                                break;
                        case 5: tmp_pel_ptr_list=&el_mechanical_ptr_list;
                                break;
                        case 6: tmp_pel_ptr_list=&el_mkupheir_ptr_list;
                                break;
                        case 7: tmp_pel_ptr_list=&el_animals_ptr_list;
                                break;
                        case 8: tmp_pel_ptr_list=&el_anwrangler_ptr_list;
                                break;
                        case 9: tmp_pel_ptr_list=&el_music_ptr_list;
                                break;
                        case 10: tmp_pel_ptr_list=&el_green_ptr_list;
                                break;
                        case 11: tmp_pel_ptr_list=&el_specequ_ptr_list;
                                break;
                        case 12: tmp_pel_ptr_list=&el_secur_ptr_list;
                                break;
                        case 13: tmp_pel_ptr_list=&el_addlab_ptr_list;
                                break;
                        case 14: tmp_pel_ptr_list=&el_vieff_ptr_list;
                                break;
                     }
                    PayElements* p_pelem=new PayElements( e );
                    tmp_pel_ptr_list->append( p_pelem );
                }
                if( e.tagName() == "stunt" )
                {
                   // QMessageBox::warning(this,QString::null,"nasao bact tag");
                    Stunts* st=new Stunts( e );
                    el_stunts_ptr_list.append( st );
                }
                if( e.tagName() == "element" )
                {
                            BuiltInCategories* p_built=0;
                            switch(i)
                            {
                            case 0: p_built=&script_day_ptr_list;
                                    break;
                            case 1: p_built=&set_ptr_list;
                                    break;
                            case 2: p_built=&sequence_ptr_list;
                                    break;
                            case 3: p_built=&unit_ptr_list;
                                    break;
                            case 4: p_built=&location_ptr_list;
                                    break;
                            }

                            Elements* el=new Elements( e );
                            p_built->append( el );

                }

            }

         n_elem=n_elem.nextSibling();
        }

        n_categ = n_categ.nextSibling();
        if(n_categ.toElement().tagName()=="scriptday") i=0;
        if(n_categ.toElement().tagName()=="set") i=1;
        if(n_categ.toElement().tagName()=="sequence") i=2;
        if(n_categ.toElement().tagName()=="unit") i=3;
        if(n_categ.toElement().tagName()=="location") i=4;
        if(n_categ.toElement().tagName()=="music") p=9;
        if(n_categ.toElement().tagName()=="backactors") p=0;
        if(n_categ.toElement().tagName()=="props") p=1;
        if(n_categ.toElement().tagName()=="visueff") p=14;
        if(n_categ.toElement().tagName()=="sounds") p=4;
        if(n_categ.toElement().tagName()=="security") p=12;
        if(n_categ.toElement().tagName()=="setdress") p=3;
        if(n_categ.toElement().tagName()=="specequip") p=11;
        if(n_categ.toElement().tagName()=="speceff") p=2;
        if(n_categ.toElement().tagName()=="mecheff") p=5;
        if(n_categ.toElement().tagName()=="makeuphair") p=6;
        if(n_categ.toElement().tagName()=="greenary") p=10;
        if(n_categ.toElement().tagName()=="addlabor") p=13;
        if(n_categ.toElement().tagName()=="animalwrang") p=8;
        if(n_categ.toElement().tagName()=="animals") p=7;

    }
    QDomNode n_sheets=n_root.firstChild();

    while(!n_sheets.isNull())
    {
        // QMessageBox::warning(this,QString::null,"usao u petlju !n_sheet.isNull()");
       // QDomNode n_sh_elem=n_sheets.firstChild();
        QDomElement d_elem = n_sheets.toElement();
        if(!d_elem.isNull())
        {
           if(d_elem.tagName()=="sheet")
            {
                //QMessageBox::warning(this,QString::null,"usao u petlju sh.tagName()==sheet");
                ;
                Sheet *sheet = new Sheet(d_elem);
                if(sh_num==0) {
                //sheets_list.replace(sh_num,sheet);
                sheets_list.remove(sh_num);
                sheets_list.insert(sh_num,sheet); }
                
                else sheets_list.append(sheet);
                current_sheet->incNumOfSheets();
                //QMessageBox::warning(this,QString::null,"inc numOfSheets");
                //current_sheet->appendInfo((current_sheet),d_elem);
               //current_sheet=sheets_list.current();
                ++sh_num;
            }
           // if(!sheets_list.isEmpty())
             //   if(sheets_list.getFirst()->getDate().isValid())
               // this->setSheetsDate();
        }
        n_sheets=n_sheets.nextSibling();

    }
     n_root = n_root.nextSibling();
  }

    ///////////////////////////////////////////////////////////////////////////////////////




   this->postaviDatumeZaElemente(false);
   current_sheet=sheets_list.first();
  p_brk_sh->initForm();

   return true;
}
QDomDocument ApplicationWindow::setXMLDocument(QDomDocument& doc)
{
    root = doc.createElement( "linsched" );
    categories=doc.createElement("categories");
    castmembers=doc.createElement("castmembers");
    QDomElement backactors=doc.createElement("backactors");
    QDomElement animals=doc.createElement("animals");
    QDomElement animalwrang=doc.createElement("animalwrang");
    QDomElement addlabor=doc.createElement("addlabor");
    QDomElement greenary=doc.createElement("greenary");
    QDomElement makeuphair=doc.createElement("makeuphair");
    QDomElement mecheff=doc.createElement("mecheff");
    QDomElement speceff=doc.createElement("speceff");
    QDomElement specequip=doc.createElement("specequip");
    QDomElement setdress=doc.createElement("setdress");
    QDomElement security=doc.createElement("security");
    QDomElement sounds=doc.createElement("sounds");
    QDomElement visueff=doc.createElement("visueff");
    QDomElement props=doc.createElement("props");
    QDomElement music=doc.createElement("music");
    QDomElement stunts=doc.createElement("stunts");
    QDomElement scr_day=doc.createElement("scriptday");
    QDomElement set=doc.createElement("set");
    QDomElement location=doc.createElement("location");
    QDomElement unit=doc.createElement("unit");
    QDomElement sequence=doc.createElement("sequence");
    QDomElement sheets=doc.createElement("sheets");

        doc.appendChild(root);
        root.appendChild( categories );
        categories.appendChild(castmembers);
        categories.appendChild(backactors);
        categories.appendChild(props);
        categories.appendChild(visueff);
        categories.appendChild(sounds);
        categories.appendChild(security);
        categories.appendChild(setdress);
        categories.appendChild(specequip);
        categories.appendChild(speceff);
        categories.appendChild(mecheff);
        categories.appendChild(makeuphair);
        categories.appendChild(greenary);
        categories.appendChild(addlabor);
        categories.appendChild(animalwrang);
        categories.appendChild(animals);

        categories.appendChild(music);

        categories.appendChild(stunts);
        categories.appendChild(scr_day);
        categories.appendChild(set);
        categories.appendChild(location);
        categories.appendChild(unit);
        categories.appendChild(sequence);
        root.appendChild(sheets);

  QPtrListIterator<CastMembers> it(el_cast_ptr_list);
  CastMembers* cm;
  while((cm=it.current())!=0)
  {
      const CastMembers cast=(*cm);
      ++it;
      castmembers.appendChild( cm->ElementsToXMLNode( doc, cast ) );
  }
/*  QPtrListIterator<PayElements> it1(el_bact_ptr_list);
  PayElements* ba;
  while((ba=it1.current())!=0)
  {
      const PayElements cast=(*ba);
      ++it1;
      backactors.appendChild( ba->ElementsToXMLNode( doc, cast ) );
  } */
  QPtrListIterator<Stunts> it2(el_stunts_ptr_list);
        Stunts* st;
        while((st=it2.current())!=0)
        {
            const Stunts stunt=(*st);
            ++it2;
            stunts.appendChild( st->ElementsToXMLNode( doc, stunt ) );
        }
  int i=0;
  while(i<=4)
  {
        BuiltInCategories p_built;
        QDomElement pom;
        switch(i)
        {
            case 0: p_built=script_day_ptr_list;
                pom=scr_day;
                break;
            case 1: p_built=set_ptr_list;
                pom=set;
                break;
            case 2: p_built=sequence_ptr_list;
                pom=sequence;
                break;
            case 3: p_built=unit_ptr_list;
                pom=unit;
                break;
            case 4: p_built=location_ptr_list;
                pom=location;
                break;
        }

        QPtrListIterator<Elements> it3(p_built);
        Elements* built_el;
        while((built_el=it3.current())!=0)
        {
            const Elements be=(*built_el);
            ++it3;
            pom.appendChild( built_el->ElementsToXMLNode( doc, be ) );
        }
        ++i;
    }
     i=0;
  while(i<=14)
  {
        PayElPtrList p_payelm;
        QDomElement pom;
        switch(i)
        {
            case 0: p_payelm=el_addlab_ptr_list;
                pom=addlabor;
                break;
            case 1: p_payelm=el_animals_ptr_list;
                pom=animals;
                break;
            case 2: p_payelm=el_anwrangler_ptr_list;
                pom=animalwrang;
                break;
            case 3: p_payelm=el_setdress_ptr_list;
                pom=setdress;
                break;
            case 4: p_payelm=el_sound_ptr_list;
                pom=sounds;
                break;
            case 5: p_payelm=el_mechanical_ptr_list;
                pom=mecheff;
                break;
            case 6: p_payelm=el_mkupheir_ptr_list;
                pom=makeuphair;
                break;
            case 7: p_payelm=el_music_ptr_list;
                pom=music;
                break;
            case 8: p_payelm=el_green_ptr_list;
                pom=greenary;
                break;

            case 9: p_payelm=el_specequ_ptr_list;
                pom=specequip;
                break;
            case 10: p_payelm=el_secur_ptr_list;
                pom=security;
                break;
            case 11: p_payelm=el_vieff_ptr_list;
                pom=visueff;
                break;
            case 12: p_payelm=el_props_ptr_list;
                pom=props;
                break;
            case 13: p_payelm=el_speceff_ptr_list;
                pom=speceff;
                break;
            case 14: p_payelm=el_bact_ptr_list;
                pom=backactors;
                break;
        }

        QPtrListIterator<PayElements> it3(p_payelm);
        PayElements* pay_elm;
        while((pay_elm=it3.current())!=0)
        {
            const PayElements pe=(*pay_elm);
            ++it3;
            pom.appendChild( pay_elm->ElementsToXMLNode( doc, pe ) );
        }
        ++i;
    }
    QPtrListIterator<Sheet> it4(sheets_list);
        Sheet* p_sheet;
        while((p_sheet=it4.current())!=0)
        {
            const Sheet sh=(*p_sheet);
            ++it4;
            sheets.appendChild( p_sheet->SheetToXMLNode( doc, sh ) );
        }
  return doc;
}
void ApplicationWindow::pocistiListe()
{
    el_cast_ptr_list.clear();
    el_stunts_ptr_list.clear();
    el_bact_ptr_list.clear();
    el_props_ptr_list.clear();
    el_speceff_ptr_list.clear();
    el_setdress_ptr_list.clear();
    el_sound_ptr_list.clear();
    el_mechanical_ptr_list.clear();
    el_mkupheir_ptr_list.clear();
    el_animals_ptr_list.clear();
    el_anwrangler_ptr_list.clear();
    el_music_ptr_list.clear();
    el_green_ptr_list.clear();
    el_specequ_ptr_list.clear();
    el_secur_ptr_list.clear();
    el_addlab_ptr_list.clear();
    el_vieff_ptr_list.clear();
    script_day_ptr_list.clear();
    set_ptr_list.clear();
    unit_ptr_list.clear();
    sequence_ptr_list.clear();
    location_ptr_list.clear();
    sheets_list.clear();
    current_sheet->setNumOfSheets(1);
}
