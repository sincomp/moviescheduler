    /****************************************************************************
** $Id:  qt/application.h   3.0.5   edited Jun 5 14:11 $
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of an example program for Qt.  This example
** program may be used, distributed and modified without limitation.
**
*****************************************************************************/

#ifndef APPLICATION_H
#define APPLICATION_H


#include "schwin.h"
#include "elmanform.h"
#include "brkdwnshform.h"
#include "stripboardform.h"
#include <qdom.h>
#include <qguardedptr.h>

class QMultiLineEdit;
class QToolBar;
class QPopupMenu;
class QWorkspace;
class QMovie;
class QAction;
class QPrinter;
class KDatePicker;
class ApplicationWindow: public SchWin
{
    Q_OBJECT
public:

    QAction* breakdownBreakdown_SheetAction;
    QAction* breakdownElement_ManagerAction;
 
        ApplicationWindow();
    ~ApplicationWindow();
     void pocistiListe();
public slots:
    bool getFlagElm(){return flagElm;}
    void showBrkdSh();
    void openStrip();
    void setSheetsDate(QDate);
    void setSheetsDate();
    void fileOpen();
    void load(const QString&);
    ElManForm* getPointToElManForm() {return p_el_man; }
    BrkDwnShForm* getPointToBrkShForm() {return p_brk_sh;}
     ElManForm* breakdownElMan();
     void setDateOnFrame();
private slots:
    void openCalendar();
    BrkDwnShForm* newDoc();
    //void load();
    void save();
    bool saveAs();
    void print();
    void closeWindow();
    void tileHorizontal();
   

    
    void about();
    void aboutQt();
    void brk_aboutToShow();
    void windowsMenuAboutToShow();
    void windowsMenuActivated( int id );
    void unsetflagElm();
    void unsetflagBrkDwn();
    void unsetflagStrip();
	void setDisabledBrk();
    void openBrkdSh();
    //bool isStripCreated()const;
    //StripBoardForm* getPointtoStripBoard()const;
protected:
    void closeEvent( QCloseEvent*  );
signals:
    void stripBoardIsOpened(StripBoardForm*);
    void message(const QString&, int );
private:
    int brkID,elmanID,stripID,setSDID;
    bool flagElm;
    bool flagBrkdwn;
    bool flagStrip;
    QPrinter *printer;
    QWorkspace* ws;
    QToolBar *fileTools;
    QPopupMenu* windowsMenu;
    QPopupMenu* scheduleMenu;
    QPopupMenu* breakdownMenu;
    QPopupMenu* calendarMenu;
	QGuardedPtr<ElManForm>  p_el_man;
    QGuardedPtr<BrkDwnShForm> p_brk_sh;
    //SheetsPtrList sheets_list;
    QGuardedPtr<StripBoardForm> stripBoard;
    //QGuardedPtr<QFrame> popup1;
    QGuardedPtr<KDatePicker> kDatePicker;
  ////////////////////////
  QString filename;
  bool ucitajSched(const QDomDocument& data);
   QDomDocument setXMLDocument(QDomDocument&);
   QDomElement root;
   QDomElement categories;
   QDomElement castmembers;
   
};



#endif

