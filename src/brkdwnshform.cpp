

#include <qtable.h>
#include <qlineedit.h>
#include <qptrlist.h>
#include <qcombobox.h>
#include <qlistbox.h>
#include <qcursor.h>
#include <qframe.h>
#include <qstatusbar.h>

#include <qmessagebox.h>
#include <qprinter.h>
#include <qpainter.h>
#include "stripboardform.h"
#include <qlayout.h>
#include "brkdwnshform.h"
#include "cell.h"
#include "preview.h"
#include "stripframe.h"
#include "application.h"
#include <qpainter.h>

const char F_SEP=':';
const char REC_SEP='|';
BrkDwnShForm::BrkDwnShForm( QWidget* parent,ApplicationWindow* appw,SheetsPtrList* shl, const char* name, WFlags fl )
    : BrkDwnShForm_base( parent, name, fl )

{
    el_ptr_list=0;
    sheets_list=shl;
    stripBoard=0;
    ws=parent;
    app_win=appw;
    setCell();
    //dodaje prazan sheet u listu ako je list prazna
    //if(!sheets_list->first())
    if(sheets_list->isEmpty())
    {
        sheet=new Sheet;
        sheets_list->append(sheet);
        
    }  
        current_sheet=sheets_list->current();
        sheet=sheets_list->current();
    //inicijalizacija combobox - ova
        
        daynightComboBox->insertItem("",0);
        intextComboBox->insertItem("",0);
        if(current_sheet->getIntExtStr()=="")
        {
            current_sheet->setIntExt(0);
        }
        if(current_sheet->getDayNightStr()=="")
        {
            current_sheet->setDayNight(0);
        }
        current_sheet->setScriptDay(0);
        current_sheet->setSet(0);
        current_sheet->setUnit(0);
        current_sheet->setSequence(0);
        current_sheet->setLocation(0);
       
        sheets_list->setAutoDelete(true);
        elementsTable->setColumnWidth(0,245);
	    elementsTable->setColumnWidth(1,245);
	    elementsTable->setColumnWidth(2,245);
	    elementsTable->setColumnWidth(3,245);
        for(unsigned int r=0;r<21;++r)
            elementsTable->setRowHeight(r,15);
	        elementsTable->setShowGrid(false);
            //stripboard frame
            
         stripFrame = new StripFrame(centralWidget(),this,ws, "stripFrame" );
          //  this->setCentralWidget(stripFrame);
    stripFrame->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, stripFrame->sizePolicy().hasHeightForWidth() ) );
    stripFrame->setMinimumSize( QSize( 0, 31 ) );
    stripFrame->setMaximumSize( QSize( 1246, 31 ) );
    stripFrame->setPaletteBackgroundColor( QColor( 0, 147, 74 ) );
    stripFrame->setFocusPolicy( QFrame::TabFocus );
    stripFrame->setFrameShape( QFrame::StyledPanel );
    stripFrame->setFrameShadow( QFrame::Raised );

    BrkDwnShForm_baseLayout->addWidget( stripFrame, 0, 0 );
    
    
        
        
        //connect(elementsTable,SIGNAL( doubleClicked(int,int,int,const QPoint&) ),this,SLOT(openPop(const QPoint&)) );
    connect(elementsTable,SIGNAL( clicked(int,int,int,const QPoint&) ),this,SLOT(openPop()) );
    connect(this,SIGNAL(scrdayCmbBoxActivated(QComboBox*,const QString&)),this,SLOT(setBuiltInSheet(QComboBox*,const QString&)) );
    connect(this,SIGNAL(setCmbBoxActivated(QComboBox*,const QString&)),this,SLOT(setBuiltInSheet(QComboBox*,const QString&)) );
    connect(this,SIGNAL(seqCmbBoxActivated(QComboBox*,const QString&)),this,SLOT(setBuiltInSheet(QComboBox*,const QString&)) );
    connect(this,SIGNAL(locCmbBoxActivated(QComboBox*,const QString&)),this,SLOT(setBuiltInSheet(QComboBox*,const QString&)) );
    connect(this,SIGNAL(unitCmbBoxActivated(QComboBox*,const QString&)),this,SLOT(setBuiltInSheet(QComboBox*,const QString&)) );
    connect(this,SIGNAL(intextCmbBoxActivatd(int)),this,SLOT(postaviStripBoju(int)));
    connect(this,SIGNAL(daynightCmbBoxActivatd(int)),this,SLOT(postaviStripBoju(int)));
//osvezavanje tabele u elmanform  pri unosu built elemenata
     connect(this,SIGNAL(scrdayCmbBoxActivated(QComboBox*,const QString&)),this,SLOT(setDateOnFrame()) );
    connect(this,SIGNAL(setCmbBoxActivated(QComboBox*,const QString&)),this,SLOT(setDateOnFrame()) );
    connect(this,SIGNAL(seqCmbBoxActivated(QComboBox*,const QString&)),this,SLOT(setDateOnFrame()) );
    connect(this,SIGNAL(locCmbBoxActivated(QComboBox*,const QString&)),this,SLOT(setDateOnFrame()) );
    connect(this,SIGNAL(unitCmbBoxActivated(QComboBox*,const QString&)),this,SLOT(setDateOnFrame()) );
    connect(this,SIGNAL(setCmbBoxActivated(QComboBox*,const QString&)),this,SLOT(setDateOnFrame() ));     

    clearWState( WState_Polished );
    initForm();
    
}

/*
 *  Destroys the object and frees any allocated resources
 */
BrkDwnShForm::~BrkDwnShForm()
{
    // no need to delete child widgets, Qt does it all for us
}
/*
 * public slot
 */
void BrkDwnShForm::copySheet()
{
    qWarning( "BrkDwnShForm::copySheet() not yet implemented!" ); 
}

/*
 * public slot
 */
void BrkDwnShForm::deleteSheet()
{
      sheets_list->setAutoDelete(true);

      unsetSheetOnElements();

      sheets_list->remove();
      current_sheet=findNext(true);
            if(!current_sheet)
                 current_sheet=findPrev(true);
       // current_sheet=sheets_list->current();
        initForm();
      if(this->getPointtoStripBoard())
      {
        QMessageBox::information(this,QString::null,"STRIPBOARD!!!");
        this->getPointtoStripBoard()->refreshStrip();
      }
      qWarning( "BrkDwnShForm::deleteSheet() not yet implemented!" ); 
}
Sheet* BrkDwnShForm::findNext(bool ret)
{
    Sheet* sh;
    bool need_return=ret;
    Sheet* cur_sh=sheets_list->current();
    if(need_return)
        if(!sheets_list->current()->getDayBreak())
            return sh=sheets_list->current();
        else
        {
            while((sh=sheets_list->next()) !=0)
            {
                if(sh->getDayBreak())
                    if(sh!=sheets_list->getLast())
                        continue;
                    else
                    {
                        //last=true;
                        current_sheet=sheets_list->at(sheets_list->findRef(cur_sh));  
                        break;
                    }
                else return sh;
            }
       
        }
    else
        {
            while((sh=sheets_list->next()) !=0)
            {
                if(sh->getDayBreak())
                    if(sh!=sheets_list->getLast())
                        continue;
                    else
                    {
                        //last=true;
                        current_sheet=sheets_list->at(sheets_list->findRef(cur_sh));
                        break;
                    }
                else return sh;
            }

        }
     current_sheet=sheets_list->at(sheets_list->findRef(cur_sh));
        if(need_return)
            return 0;
        else return current_sheet;
       // return sh=findPrev();
}
Sheet* BrkDwnShForm::findPrev(bool ret)
{ 
    Sheet* sh;
    Sheet* cur_sh=sheets_list->current();
    bool need_return=ret;
    if(need_return)
        if(!sheets_list->current()->getDayBreak())
            return sh=sheets_list->current();
        else
        {
            //current_sheet=sheets_list->at(sheets_list->find(cur_sh));
            while((sh=sheets_list->prev()) !=0)
            {
                if(sh->getDayBreak())
                    if(sh!=sheets_list->getFirst())
                        continue;
                    else
                    {
                    //first=true;
                        current_sheet=sheets_list->at(sheets_list->findRef(cur_sh)); 
                        break;
                    }
                else return sh;
            }
        }
    else
        {
            while((sh=sheets_list->prev()) !=0)
            {
                if(sh->getDayBreak())
                    if(sh!=sheets_list->getFirst())
                        continue;
                    else
                    {
                        //last=true;
                        current_sheet=sheets_list->at(sheets_list->findRef(cur_sh));
                        break;
                    }
                else return sh;
            }

        }
    current_sheet=sheets_list->at(sheets_list->findRef(cur_sh)); 
       if(need_return)
            return 0;
       else
            return current_sheet;
}

void BrkDwnShForm::unsetSheetOnElements()
{
   CastMembers* cm;
      QPtrListIterator<CastMembers> it(el_cast_ptr_list);
        while((cm=it.current())!=0)
        {
            ++it;
            if(cm->getSheetNum(current_sheet->getSheetNum()) )
            cm->setSheetNum(current_sheet->getSheetNum());
        }
      unsetPayElemSheetNum();
      Stunts* st;
      QPtrListIterator<Stunts> itst(el_stunts_ptr_list);
        while((st=itst.current())!=0)
        {
            ++itst;
            if(st->getSheetNum(current_sheet->getSheetNum()) )
            st->setSheetNum(current_sheet->getSheetNum());
        }
      unsetBuiltElemSheetNum(script_day_ptr_list);
      unsetBuiltElemSheetNum(location_ptr_list);
      unsetBuiltElemSheetNum(unit_ptr_list);
      unsetBuiltElemSheetNum(set_ptr_list);
      unsetBuiltElemSheetNum(sequence_ptr_list);
}
 
int BrkDwnShForm::getElementNum(QPtrList<Elements>& bc,const QString& t)
{

    QPtrListIterator<Elements> it(bc);
                    Elements* elem;
                   // bc.first();
                    while((elem=it.current())!=0)
                    {
                        ++it;
                        if(elem->getElementName()==t)
                        {
                            elem_num=bc.findRef(elem);
                            break;
                        }
                    }
    return elem_num;
}
/*
 * public slot
 */
ApplicationWindow* BrkDwnShForm::getPointtoAppWin()const
{
        return app_win;
    
    }
SheetsPtrList* BrkDwnShForm::getPointtoSheetsList()const
{
    return sheets_list;
}
void BrkDwnShForm::gotoNextSheet()
{
    QPtrListIterator<Sheet> it_sheet(*sheets_list);
   // Sheet* c_sheet;
    if(!current_sheet->isEmpty())
    {
//postavljanje vrednosti sa tekuceg lista u tekucu promenljivu current_sheet
        current_sheet->setScenes(sceneLineEdit->text());
        current_sheet->setIntExt(intextComboBox->currentItem());
        current_sheet->setDayNight(daynightComboBox->currentItem());
        current_sheet->setPage1(pagesLineEdit->text() );
        current_sheet->setPage2(pages2LineEdit->text() );
        current_sheet->setSynopsis(synopsisLineEdit->text());
        current_sheet->setScriptPages(scrpageLineEdit->text());
        current_sheet->setGenNotes(genNotesLineEdit->text());

        if(sheets_list->current()==sheets_list->getLast())
        {
            sheet=new Sheet;
            QMessageBox::warning(this,QString::null,"inc numOfSheets"); 
            sheets_list->append(sheet);

            if(stripBoard)
                stripBoard->refreshStrip();
//ako predhodni sheet nije daybreak postavi isti datum ako jeste povecaj datu za jedan dan
        /*    Sheet* curr_sheet=sheets_list->current();
            Sheet* prev_sheet=sheets_list->prev();sheets_list->next();
            if(!prev_sheet->getDayBreak())
            sheets_list->last()->setDate(curr_sheet->getDate());
            else sheets_list->last()->setDate(curr_sheet->getDate().addDays(1));  */
            current_sheet=sheets_list->last();     

            //inicijalizacija combobox - ova
            current_sheet->setIntExt(0);
            current_sheet->setDayNight(0);
            current_sheet->setScriptDay(0);
            current_sheet->setSet(0);
            current_sheet->setUnit(0);
            current_sheet->setSequence(0);
            current_sheet->setLocation(0);
   
        }
        else
            
             current_sheet=findNext(false);
           
    }
     initForm();
    
    //qWarning( "BrkDwnShForm::gotoNextSheet() not yet implemented!" ); 
}

/*
 * public slot
 */
void BrkDwnShForm::gotoPrevSheet()
{
   // QPtrListIterator<Sheet> it_sheet(*sheets_list);
    //Sheet* c_sheet;
    if(sheets_list->current()!=sheets_list->getFirst())
    {
        
                            current_sheet->setScenes(sceneLineEdit->text());
                            current_sheet->setIntExt(intextComboBox->currentItem());
                            current_sheet->setDayNight(daynightComboBox->currentItem());
                            current_sheet->setPage1(pagesLineEdit->text() );
                            current_sheet->setPage2(pages2LineEdit->text() );
                            current_sheet->setSynopsis(synopsisLineEdit->text());
                            current_sheet->setScriptPages(scrpageLineEdit->text());
                            current_sheet->setGenNotes(genNotesLineEdit->text());               
      //   if(sheets_list->prev()->getDayBreak())
                 //sheets_list->prev();
                 current_sheet=findPrev(false);
         initForm();     
        
     }   
}

/*
 * public slot
 */
 void BrkDwnShForm::setDateOnFrame()
{
   if(stripFrame) 
     stripFrame->update();
// postavi nove datume za elemente forsirano
    this->postaviDatumeZaElemente(true);
     this->initElManTable();
    
}
void BrkDwnShForm::initElManTable()
{
   if(getPointtoAppWin()->getFlagElm())
   this->getPointtoAppWin()->getPointToElManForm()->initTable();
}      
void BrkDwnShForm::initForm()
{


            //SchWin::updateSheet();
            updateSheet();
            updatePayElmSheet(el_bact_ptr_list);
            updatePayElmSheet(el_anwrangler_ptr_list);
            updatePayElmSheet(el_animals_ptr_list);
            updatePayElmSheet(el_addlab_ptr_list);

            updatePayElmSheet(el_props_ptr_list);
            updatePayElmSheet(el_speceff_ptr_list);
            updatePayElmSheet(el_setdress_ptr_list);
            updatePayElmSheet(el_sound_ptr_list);
            updatePayElmSheet(el_mechanical_ptr_list);
            updatePayElmSheet(el_mkupheir_ptr_list);


            updatePayElmSheet(el_music_ptr_list);
            updatePayElmSheet(el_green_ptr_list);
            updatePayElmSheet(el_specequ_ptr_list);
            updatePayElmSheet(el_secur_ptr_list);

            updatePayElmSheet(el_vieff_ptr_list);

            updateBuiltSheet(script_day_ptr_list);
            updateBuiltSheet(set_ptr_list);
            updateBuiltSheet(location_ptr_list);
            updateBuiltSheet(unit_ptr_list);
            updateBuiltSheet(sequence_ptr_list); 
            initBuiltIn();

            initTable();
     if(sheets_list->current())
     {
          QString str;
          str.setNum(current_sheet->getSheetNum());
          sheetLineEdit->setText(str);
          sceneLineEdit->setText(current_sheet->getScenes());
          intextComboBox->setCurrentItem(current_sheet->getIntExt() );
          daynightComboBox->setCurrentItem(current_sheet->getDayNight());
          pagesLineEdit->setText(current_sheet->getPage1() );
          pages2LineEdit->setText(current_sheet->getPage2() );
          synopsisLineEdit->setText(current_sheet->getSynopsis() );
          scrpageLineEdit->setText(current_sheet->getScriptPages());
          genNotesLineEdit->setText(current_sheet->getGenNotes());

          scrdayComboBox->setCurrentText(current_sheet->getScriptDay());
          setComboBox->setCurrentText(current_sheet->getSet() );
          unitComboBox->setCurrentText(current_sheet->getUnit() );
          seqComboBox->setCurrentText(current_sheet->getSequence() );
          locComboBox->setCurrentText(current_sheet->getLocation() );
     }
     this->postaviStripBoju(current_sheet->getIntExt());
     stripFrame->update();
     //if(getPointtoAppWin()->getFlagElm())
     //this->getPointtoAppWin()->breakdownElMan()->initTable();
     //this->setDateOnFrame();
}
void BrkDwnShForm::initTable()
{
    for(int r=0;r<elementsTable->numRows();++r)
            for(int k=0;k<4;++k)
                elementsTable->setText(r,k,"");

    ///////////////////////////////////////////////////////////
      if(current_sheet)
      {

        int i=0;
        int j=0;

        for ( QStringList::Iterator it0 = current_sheet->cast_list.begin(); it0 != current_sheet->cast_list.end(); ++it0 )
        {

            elementsTable->setText( i, j, *it0 );
            ++i;
            if(i>14){
             i=0;
             ++j;
             }
        }
        for ( QStringList::Iterator it1 = current_sheet->bact_list.begin(); it1 != current_sheet->bact_list.end(); ++it1 )
        {
            elementsTable->setText( i, j, *it1 );
            ++i;
            if(i>14)
                {
                     i=0;
                     ++j;
                     }
        }
        for ( QStringList::Iterator it2 = current_sheet->stunts_list.begin(); it2 != current_sheet->stunts_list.end(); ++it2 )
        {
            elementsTable->setText( i, j, *it2 );
            ++i;
            if(i>14)
            {
                     i=0;
                     ++j;
                     }

        }
        for ( QStringList::Iterator it3 = current_sheet->addlab_list.begin(); it3 != current_sheet->addlab_list.end(); ++it3 )
        {
            elementsTable->setText( i, j, *it3 );
            ++i;
            if(i>14)
                {
                     i=0;
                     ++j;
                     }
        }

        for ( QStringList::Iterator it4 = current_sheet->animals_list.begin(); it4 != current_sheet->animals_list.end(); ++it4 )
        {
            elementsTable->setText( i, j, *it4 );
            ++i;
            if(i>14)
                {
                     i=0;
                     ++j;
                     }

        }
        for ( QStringList::Iterator it5 = current_sheet->anwrangler_list.begin(); it5 != current_sheet->anwrangler_list.end(); ++it5 )
        {
            elementsTable->setText( i, j, *it5 );
            ++i;
             if(i>14)
                {
                     i=0;
                     ++j;
                     }
        }
        for ( QStringList::Iterator it6 = current_sheet->setdress_list.begin(); it6 != current_sheet->setdress_list.end(); ++it6 )
        {

            elementsTable->setText( i, j, *it6 );
            ++i;
             if(i>14)
                {
                     i=0;
                     ++j;
                     }
        }
        for ( QStringList::Iterator it7 = current_sheet->sound_list.begin(); it7 != current_sheet->sound_list.end(); ++it7 )
        {
            elementsTable->setText( i, j, *it7 );
            ++i;
             if(i>14)
                {
                     i=0;
                     ++j;
                     }
        }
        for ( QStringList::Iterator it8 = current_sheet->mecheff_list.begin(); it8 != current_sheet->mecheff_list.end(); ++it8 )
        {
            elementsTable->setText( i, j, *it8 );
            ++i;
             if(i>14)
                {
                     i=0;
                     ++j;
                     }
        }
        for ( QStringList::Iterator it9 = current_sheet->mkuphair_list.begin(); it9 != current_sheet->mkuphair_list.end(); ++it9 )
        {
            elementsTable->setText( i, j, *it9 );
            ++i;
             if(i>14)
                {
                     i=0;
                     ++j;
                     }
        }
        for ( QStringList::Iterator it10 = current_sheet->greenary_list.begin(); it10 != current_sheet->greenary_list.end(); ++it10 )
        {
            elementsTable->setText( i, j, *it10 );
            ++i;
             if(i>14)
                {
                     i=0;
                     ++j;
                     }
        }
        for ( QStringList::Iterator it11 = current_sheet->security_list.begin(); it11 != current_sheet->security_list.end(); ++it11 )
        {

            elementsTable->setText( i, j, *it11 );
            ++i;
             if(i>14)
                {
                     i=0;
                     ++j;
                     }
        }
        for ( QStringList::Iterator it12 = current_sheet->specequip_list.begin(); it12 != current_sheet->specequip_list.end(); ++it12 )
        {
            elementsTable->setText( i, j, *it12 );
            ++i;
             if(i>14)
             {
                     i=0;
                     ++j;
                     }
        }
        for ( QStringList::Iterator it13 = current_sheet->viseff_list.begin(); it13 != current_sheet->viseff_list.end(); ++it13 )
        {
            elementsTable->setText( i, j, *it13);
            ++i;
             if(i>14)
             {
                     i=0;
                     ++j;
                     }
        }
        for ( QStringList::Iterator it14= current_sheet->props_list.begin(); it14 != current_sheet->props_list.end(); ++it14 )
        {
            elementsTable->setText( i, j, *it14 );
            ++i;
             if(i>14)
             {
                     i=0;
                     ++j;
                     }
        }
        for ( QStringList::Iterator it15 = current_sheet->speceff_list.begin(); it15 != current_sheet->speceff_list.end(); ++it15 )
        {
            elementsTable->setText( i, j, *it15 );
            ++i;
             if(i>14)
                {
                     i=0;
                     ++j;
                     }
        }
        for ( QStringList::Iterator it16 = current_sheet->music_list.begin(); it16 != current_sheet->music_list.end(); ++it16 )
        {
            elementsTable->setText( i, j, *it16 );
            ++i;
             if(i>14)
                {
                     i=0;
                     ++j;
                     }
        }

    }
//osvezavanje tabele na elmanform
    if(getPointtoAppWin()->getFlagElm())
     this->getPointtoAppWin()->getPointToElManForm()->initTable();
}

void BrkDwnShForm::initBuiltIn()
{
     QPtrListIterator<Elements> it(script_day_ptr_list);
    Elements *belem;
    scrdayComboBox->clear();
        while ( (belem = it.current()) != 0 )
            {
                ++it;
                scrdayComboBox->insertItem(QString::fromUtf8(belem->getElementName()));
            }
    QPtrListIterator<Elements> it2(set_ptr_list);
    setComboBox->clear();
        while ( (belem = it2.current()) != 0 )
            {
                ++it2;
                setComboBox->insertItem(tr(belem->getElementName()) );
            }
    QPtrListIterator<Elements> it3(unit_ptr_list);
    unitComboBox->clear();
        while ( (belem = it3.current()) != 0 )
            {
                ++it3;
                unitComboBox->insertItem(tr(belem->getElementName()));
            }
    QPtrListIterator<Elements> it4(sequence_ptr_list);
    seqComboBox->clear();
        while ( (belem = it4.current()) != 0 )
            {
                ++it4;
                seqComboBox->insertItem(tr(belem->getElementName()) );
            }
    QPtrListIterator<Elements> it5(location_ptr_list);
    locComboBox->clear();
        while ( (belem = it5.current()) != 0 )
            {
                ++it5;
                locComboBox->insertItem(tr(belem->getElementName()) );
            }
        //daynightComboBox->insertItem("",0);
        //intextComboBox->insertItem("",0);
        locComboBox->insertItem("",0);
        scrdayComboBox->insertItem("",0);
        seqComboBox->insertItem("",0);
        setComboBox->insertItem("",0);
        unitComboBox->insertItem("",0);
}


void BrkDwnShForm::newSheet()
{
    current_sheet=sheets_list->getLast();
    initForm();
    bool empty=current_sheet->isEmpty();
    if(!empty)                                     
    {
        Sheet* sheet=new Sheet();
        sheets_list->append(sheet);
        current_sheet=sheets_list->current();
        initForm();
    }
    qWarning( "BrkDwnShForm::newSheet() not yet full implemented!" );
}

void BrkDwnShForm::openPop()
{
    //WFlags wf=0;
    WFlags wf = WDestructiveClose;
    //wf |=WType_TopLevel;
    //wf |= WStyle_Customize;
    //wf |=WStyle_NoBorder;
    //wf |=WType_Modal;
    wf |= WType_Popup;

    //QWidget*parent=(QWidget*)elementsTable->item(elementsTable->currentRow(),elementsTable->currentColumn());
    //f=73745;
    QWidget* parent=this;
    QString contr_str;
    contr_str=elementsTable->item(elementsTable->currentRow(),elementsTable->currentColumn())->text();

//ovde se odacuju prazna mesta iz stringa
    bool b1=0,b2=0,b3=0,b4=0,b5=0,b6=0,b7=0,b8=0,b9=0,b10=0,b11=0,b12=0,b13=0,b14=0,b15=0,b16=0,b17=0;
    if(contr_str.stripWhiteSpace()=="Cast Members")
    {
        el_ptr_list = (QPtrList<Elements>*)&el_cast_ptr_list;//bilo na levoj strani (QPtrList<CastMembers>*)
        b1=true;
    }

    if(contr_str.stripWhiteSpace()=="Background Actors"){
        el_ptr_list=(QPtrList<Elements>*)&el_bact_ptr_list;//bilo na levoj strani (QPtrList<PayElements>*)
           b2=true;
    }

if(contr_str.stripWhiteSpace()=="Props"){
        el_ptr_list=(QPtrList<Elements>*)&el_props_ptr_list;
           b3=true;
    }
if(contr_str.stripWhiteSpace()=="Special Effects"){
        el_ptr_list=(QPtrList<Elements>*)&el_speceff_ptr_list;
           b4=true;
    }
if(contr_str.stripWhiteSpace()=="Set Dressing"){
        el_ptr_list=(QPtrList<Elements>*)&el_setdress_ptr_list;
           b5=true;
    }
if(contr_str.stripWhiteSpace()=="Sounds"){
        el_ptr_list=(QPtrList<Elements>*)&el_sound_ptr_list;
           b6=true;
    }
if(contr_str.stripWhiteSpace()=="Machanical Effects"){
        el_ptr_list=(QPtrList<Elements>*)&el_mechanical_ptr_list;
           b7=true;
    }
if(contr_str.stripWhiteSpace()=="Makeup/Hair"){
        el_ptr_list=(QPtrList<Elements>*)&el_mkupheir_ptr_list;
           b8=true;
    }
if(contr_str.stripWhiteSpace()=="Animals"){
        el_ptr_list=(QPtrList<Elements>*)&el_animals_ptr_list;
           b9=true;
    }
if(contr_str.stripWhiteSpace()=="Animal Wrangler"){
        el_ptr_list=(QPtrList<Elements>*)&el_anwrangler_ptr_list;
           b10=true;
    }
if(contr_str.stripWhiteSpace()=="Music"){
        el_ptr_list=(QPtrList<Elements>*)&el_music_ptr_list;
           b11=true;
    }
if(contr_str.stripWhiteSpace()== "Geenery") {
        el_ptr_list=(QPtrList<Elements>*)&el_green_ptr_list;
           b12=true;
    }
if(contr_str.stripWhiteSpace()=="Special Equipment"){
        el_ptr_list=(QPtrList<Elements>*)&el_specequ_ptr_list;
           b13=true;
    }
if(contr_str.stripWhiteSpace()=="Security"){
        el_ptr_list=(QPtrList<Elements>*)&el_secur_ptr_list;
           b14=true;
    }
if(contr_str.stripWhiteSpace()=="Additional Labor"){
        el_ptr_list=(QPtrList<Elements>*)&el_addlab_ptr_list;
           b15=true;
    }
if(contr_str.stripWhiteSpace()=="Visual Effects"){
        el_ptr_list=(QPtrList<Elements>*)&el_vieff_ptr_list;
           b16=true;
    }
    if(contr_str.stripWhiteSpace()=="Stunts"){
        el_ptr_list=(QPtrList<Elements>*)&el_stunts_ptr_list;//bilo na levoj strani u verziji Kdevelop2.x (QPtrList<Stunts>*)
           b17=true;
    }

    if(b1||b2||b3||b4||b5||b6||b7||b8||b9||b10||b11||b12||b13||b14||b15||b16||b17)
    {
        if ( !popup1 )
        {
            popup1 = new QFrame( parent ,0, wf);
            popup1->setFrameStyle( QFrame::WinPanel|QFrame::Raised );
            popup1->resize(170,230);
            listBox=new QListBox(popup1);
            listBox->setMargin(2);
            connect(listBox,SIGNAL(clicked(QListBoxItem*)),this,SLOT(setSheet(QListBoxItem*)) );
            listBox->setFixedWidth(170);
            listBox->setFixedHeight(230);
            popup1->move(QCursor::pos() );
            //popup1->move(mapToGlobal( parent->geometry().bottomLeft() ) );
            listBox->setFocus();

        updateListBox();
        popup1->show();
        }


           //listBox->move( QCursor::pos() );
            //listBox->move(pos());
   }       // listBox->show();

}
void BrkDwnShForm::prikaziSheetBr(unsigned int br)
{
   if(!sheets_list->at(br)->getDayBreak())
    this->current_sheet=sheets_list->at(br);
   
}
void BrkDwnShForm::setCell()
{

     for(int r=0;r<elementsTable->numRows();++r)
            for(int k=0;k<4;++k)
            {
                     Cell* cell= new Cell(elementsTable);
                     elementsTable->setItem(r,k,cell);
            }
}


/* 
 * public slot
 */
void BrkDwnShForm::save()
{
    
}


void BrkDwnShForm::saveAs()
{


}
void BrkDwnShForm::setStripBoard(StripBoardForm*s)
{
    stripBoard=s;
    }
void BrkDwnShForm::setSheet(QListBoxItem* item)
{
    int shnum=listBox->index(item);
    if(shnum>0){
    //if(shnum==-1) popup1->close();
    Elements* el;

        
        el=el_ptr_list->at(shnum-1);
        el->setSheetNum(current_sheet->getSheetNum());
        
        popup1->close();

//dodeljivanje startDatuma elementu koji je trenutno unet u raspored   
        setElementDate(el);
        updateSheet();
    
             


        updatePayElmSheet(el_bact_ptr_list);
            updatePayElmSheet(el_anwrangler_ptr_list);
            updatePayElmSheet(el_animals_ptr_list);
            updatePayElmSheet(el_addlab_ptr_list);

            updatePayElmSheet(el_props_ptr_list);
            updatePayElmSheet(el_speceff_ptr_list);
            updatePayElmSheet(el_setdress_ptr_list);
            updatePayElmSheet(el_sound_ptr_list);
            updatePayElmSheet(el_mechanical_ptr_list);
            updatePayElmSheet(el_mkupheir_ptr_list);


            updatePayElmSheet(el_music_ptr_list);
            updatePayElmSheet(el_green_ptr_list);
            updatePayElmSheet(el_specequ_ptr_list);
            updatePayElmSheet(el_secur_ptr_list);

            updatePayElmSheet(el_vieff_ptr_list);

            updateBuiltSheet(script_day_ptr_list);
            updateBuiltSheet(set_ptr_list);
            updateBuiltSheet(location_ptr_list);
            updateBuiltSheet(unit_ptr_list);
            updateBuiltSheet(sequence_ptr_list);
            initTable();
            updateItem(shnum);
        }
        else

    this->getPointtoAppWin()->breakdownElMan();
    popup1->close();
   
    //updateListBox();
    //popup1->close();
    //popup1->show();

}

void BrkDwnShForm::setBuiltInSheet(QComboBox* cb,const QString& el_name)
{
   BuiltInCategories built_in_ptr;
   if(cb==scrdayComboBox) built_in_ptr=script_day_ptr_list;
   if(cb==locComboBox) built_in_ptr=location_ptr_list;
   if(cb==seqComboBox) built_in_ptr=sequence_ptr_list;
   if(cb==setComboBox) built_in_ptr=set_ptr_list;
   if(cb==unitComboBox) built_in_ptr=unit_ptr_list;
   QPtrListIterator<Elements> it(built_in_ptr);
                    Elements* elem;
   if(el_name=="")
//ako je uneto prazno polje u combo box resetuj sve elemente za dati list
   while((elem=it.current())!=0)
                    {
                        ++it;
                        if(elem->getSheetNum(current_sheet->getSheetNum())) elem->setSheetNum(current_sheet->getSheetNum());
                    }

   else
   {
        int num_el=getElementNum(built_in_ptr,el_name);

        if(num_el>=0)
        {
            built_in_ptr.at(num_el)->setBuiltShNum(current_sheet->getSheetNum());
                   // bc.first();
                    while((elem=it.current())!=0)
                    {
                        ++it;
                        
                        if(elem==built_in_ptr.at(num_el)) continue;
                        if(elem->getSheetNum(current_sheet->getSheetNum()))
                        {
                             elem->setSheetNum(current_sheet->getSheetNum());
//postavi-skini datum za tekuci element
                             this->setElementDate(elem);
                        }
                    }
        }
   }


   
}


void BrkDwnShForm::updateListBox()
{
    listBox->clear();
  listBox->insertItem("Add New Element...");
            QString el,id,mark,point;

                 if(el_ptr_list->first())
                 {
                    //QPtrListIterator<CastMembers> it(el_cast_ptr_list);
                    QPtrListIterator<Elements> it(*el_ptr_list);
                    Elements* cm;

                    while((cm=it.current())!=0)
                    {
                        ++it;
                        point=".";
                        id=cm->getBoardID();
                        if(id=="")  point="";
                        el=cm->getElementName();
                        if(cm->getSheetNum(current_sheet->getSheetNum()) )
                       // mark=QString::fromUtf8(" ✓",4);
                       mark=QString::fromUtf8(" √",6);
                        else mark="";

                        //formiranje stringa za prikazivanje u listBox meniju
                        //id+=point;id+=el;id+=mark;
                        el+=mark;
                        listBox->insertItem(el);
                    }
                 }
}
void BrkDwnShForm::updateItem(int index)
{
    //QListBoxItem* item;
    QString el,id,mark;
    Elements* cm;
    cm=el_ptr_list->at(index-1);
    
    el=cm->getElementName();
    if(cm->getSheetNum(current_sheet->getSheetNum()) )
                        //mark=QString::fromUtf8(" ✓",4);
                        mark=QString::fromUtf8(" √",6);
                        else mark="";
                        //formiranje stringa za prikazivanje u listBox meniju
                        id=cm->getBoardID();
                       // if(id.isEmpty()) id+="";
                       // else id+=".";
                       // id+=el;id+=mark;
                       el+=mark;
    listBox->removeItem(index);
    listBox->insertItem(el,index);
    listBox->setCurrentItem(index);
}




void BrkDwnShForm::scrdayCmbBoxActivated(const QString& t)
{
    emit scrdayCmbBoxActivated(scrdayComboBox,t);
}
void BrkDwnShForm::locCmbBoxActivated(const QString& t)
{
    emit locCmbBoxActivated(locComboBox,t);
}
void BrkDwnShForm::setCmbBoxActivated(const QString& t)
{
    emit setCmbBoxActivated(setComboBox,t);
}
void BrkDwnShForm::unitCmbBoxActivated(const QString& t)
{
    emit unitCmbBoxActivated(unitComboBox,t);
}
void BrkDwnShForm::seqCmbBoxActivated(const QString& t)
{
    emit seqCmbBoxActivated(seqComboBox,t);
}
void BrkDwnShForm::intextCmbBoxActivated(int nbr)
{
     emit intextCmbBoxActivatd(nbr);
}

void BrkDwnShForm::daynightCmbBoxActivated(int nbr)
{
    emit daynightCmbBoxActivatd(nbr);
}
void BrkDwnShForm::postaviStripBoju(int nbr)
{
    if(nbr==0||intextComboBox->currentItem()==0||daynightComboBox->currentItem()==0)
       stripFrame->setPaletteBackgroundColor( QColor(255, 255, 255 ) );
       else
        if(intextComboBox->currentItem()==1)
            if(daynightComboBox->currentItem()==1)
                 stripFrame->setPaletteBackgroundColor( QColor(217, 113, 65 )  );
                 else
                 stripFrame->setPaletteBackgroundColor( QColor(0, 185, 127 ) );
        else
            if(daynightComboBox->currentItem()==1)
                 stripFrame->setPaletteBackgroundColor( QColor(255, 236, 128));
                 else
                 stripFrame->setPaletteBackgroundColor( QColor(0, 147, 174) );
        
}
QColor* BrkDwnShForm::preuzmiStripBoju(Sheet*s)
{
    QColor* c;
    if((s->getIntExtStr()=="")||(s->getDayNightStr()==""))
       return c =new QColor(255, 255, 255 );
       else
        if(s->getIntExt()==1)
            if(s->getDayNight()==1)
                 return c =new QColor(217, 113, 65);
                 else
                 return  c =new QColor(0, 185, 127);
        else
            if(s->getDayNight()==1)
                 return c =new QColor(255, 236, 128 );
                 else
                 return c =new QColor(0, 147, 174);
}



void BrkDwnShForm::print( QPrinter*printer )
{

//  Preview *preview=new Preview(this,current_sheet,"",printer,Qt::WType_TopLevel);
     Preview *preview=new Preview(ws,current_sheet,"",printer);
   QColor c(white);
   c.setHsv(0,0,255);
   preview->setPalette(QPalette(c));
   preview->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
   // preview->update();
   preview->showNormal();
}
