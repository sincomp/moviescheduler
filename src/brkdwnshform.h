#ifndef BRKDWNSHFORM_H
#define BRKDWNSHFORM_H
#include "brkdwnshform_base.h"
#include "sheet.h"
#include <qptrlist.h>
#include <qguardedptr.h>
#include<qlistbox.h>
class ApplicationWindow;
class StripFrame;
class QListBox;
class BrkDwnShForm : public BrkDwnShForm_base
{ 
    Q_OBJECT

public:
    BrkDwnShForm( QWidget* parent = 0,ApplicationWindow* appw=0,SheetsPtrList* shl=0, const char* name = 0, WFlags fl = 0); 
    ~BrkDwnShForm();
    StripBoardForm* getPointtoStripBoard()const{return stripBoard;}
 //   StripFrame* getPointtoStripFrame()const;
    ApplicationWindow* getPointtoAppWin()const;
    SheetsPtrList* getPointtoSheetsList()const;
    QColor* preuzmiStripBoju(Sheet*);
  //  bool isStripFCreated();
public slots:
    
    void copySheet();
    void deleteSheet();
    void gotoNextSheet();
    void gotoPrevSheet();
    void newSheet();
    void save();
    
    void saveAs();
    void initForm();
    void initTable();
    void initBuiltIn();
    //void openPop(const QPoint&); 
    void openPop();
    void setSheet(QListBoxItem*);
    void prikaziSheetBr(unsigned int);
    //void updateSheet();
    void updateListBox();
    void intextCmbBoxActivated(int);
    void daynightCmbBoxActivated(int);
    void scrdayCmbBoxActivated(const QString&);
    void locCmbBoxActivated(const QString&);
    void setCmbBoxActivated(const QString&);
    void unitCmbBoxActivated(const QString&);
     void seqCmbBoxActivated(const QString&);
     void print( QPrinter* );
    void postaviStripBoju(int);
    void setDateOnFrame();
    Sheet* findNext(bool);
    Sheet* findPrev(bool);
    void setStripBoard(StripBoardForm*);
signals:

    void intextCmbBoxActivatd(int);
    void daynightCmbBoxActivatd(int);
    void scrdayCmbBoxActivated(QComboBox*,const QString&);
    void locCmbBoxActivated(QComboBox*,const QString&);
    void seqCmbBoxActivated(QComboBox*,const QString&);
    void unitCmbBoxActivated(QComboBox*,const QString&);
    void setCmbBoxActivated(QComboBox*,const QString&);
private slots:
    void setBuiltInSheet(QComboBox*,const QString&);
   void initElManTable(); 
private:
   int getElementNum(QPtrList<Elements>&,const QString&); 
   void setCell();
   void updateItem(int);
   void unsetSheetOnElements();
   SheetsPtrList* sheets_list;
   Sheet* sheet; //pristupna promenljiva, pomocna kaja ce prihvatati elemente iz sheets_list
   QPtrList<Elements>* el_ptr_list;
   QGuardedPtr<QFrame> popup1;
   QGuardedPtr<QListBox> listBox;
   int elem_num;
   
   QGuardedPtr<StripFrame> stripFrame;
   QGuardedPtr<StripBoardForm> stripBoard;
  //StripBoardForm* stripBoard;
   QWidget* ws;
   ApplicationWindow* app_win;
};

#endif // BRKDWNSHFORM_H
