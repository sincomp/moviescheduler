/****************************************************************************
** Form implementation generated from reading ui file 'brkdwnshform_frame_.ui'
**
** Created: Fri Apr 22 02:10:18 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.2.1   edited May 19 14:22 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "brkdwnshform_base.h"
//ovo je ubaceno
#include "stripboardform.h"
/////////////////////////
#include <qvariant.h>
#include <qpushbutton.h>
#include <qcombobox.h>
#include <qlineedit.h>
#include <qlabel.h>
#include <qtable.h>
#include <qtoolbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qaction.h>
#include <qmenubar.h>
#include <qpopupmenu.h>
#include <qtoolbar.h>
#include <qimage.h>
#include <qpixmap.h>

static const char* const image0_data[] = { 
"22 22 52 1",
". c None",
"# c #000000",
"V c #2b2b2b",
"d c #323232",
"m c #343434",
"h c #363636",
"c c #3b3b3b",
"b c #3e3e3e",
"e c #3f3f3f",
"a c #454545",
"r c #969696",
"X c #979797",
"l c #989898",
"q c #a1a1a1",
"U c #a3a3a3",
"S c #a7a7a7",
"z c #a8a8a8",
"Q c #ababab",
"O c #b0b0b0",
"N c #b3b3b3",
"K c #b8b8b8",
"I c #bbbbbb",
"W c #bdbdbd",
"F c #c1c1c1",
"C c #c4c4c4",
"R c #c5c5c5",
"y c #c6c6c6",
"T c #c7c7c7",
"u c #c9c9c9",
"P c #cacaca",
"p c #cdcdcd",
"k c #cfcfcf",
"g c #d2d2d2",
"M c #d4d4d4",
"L c #d8d8d8",
"x c #d9d9d9",
"J c #dddddd",
"H c #dedede",
"G c #e1e1e1",
"E c #e4e4e4",
"D c #e6e6e6",
"B c #e8e8e8",
"A c #eaeaea",
"w c #ededed",
"v c #eeeeee",
"t c #f2f2f2",
"s c #f3f3f3",
"o c #f5f5f5",
"n c #f6f6f6",
"j c #f8f8f8",
"i c #f9f9f9",
"f c #fcfcfc",
"......................",
"....#############.....",
"....#abcccccd#e###....",
"....#bffffffg##h###...",
"....#cijjjjjk#l#m###..",
"....#cnooooop#qr#m###.",
"....#cstttttu########.",
"....#cvwwwwwxyyyz####.",
"....#cABBBBBBBBBC####.",
"....#cDEEEEEEEEEF####.",
"....#cGHHHHHHHHHI####.",
"....#cJxxxxxxxxxK####.",
"....#cLMMMMMMMMMN####.",
"....#cMkkkkkkkkkO####.",
"....#ckPPPPPPPPPQ####.",
"....#cPRRRRRRRRRS####.",
"....#cTFFFFFFFFFU####.",
"....#VWqqqqqqqqqX####.",
"....#################.",
"....#################.",
"....#################.",
"....#################."};

static const char* const image1_data[] = { 
"22 22 68 2",
"Qt c None",
".# c #000000",
"#b c #202020",
".c c #232323",
".i c #2b2b2b",
".b c #2d2d2d",
".a c #343434",
".q c #3a3a3a",
".h c #3c3c3c",
".d c #444444",
".I c #4d4d4d",
".B c #4e4e4e",
".x c #515151",
".r c #595959",
".8 c #808080",
".5 c #919191",
".w c #959595",
".9 c #969696",
".p c #979797",
".0 c #999999",
".W c #9f9f9f",
".v c #a1a1a1",
"## c #a6a6a6",
".S c #a9a9a9",
".6 c #ababab",
"#. c #acacac",
".3 c #afafaf",
".7 c #b0b0b0",
".F c #b1b1b1",
".1 c #b4b4b4",
".D c #bababa",
".Y c #bebebe",
".z c #bfbfbf",
".C c #c0c0c0",
".E c #c2c2c2",
".U c #c3c3c3",
".M c #c7c7c7",
".u c #c8c8c8",
".O c #c9c9c9",
"#a c #cacaca",
".H c #cbcbcb",
".o c #cecece",
".g c #d2d2d2",
".X c #d3d3d3",
".R c #d6d6d6",
".y c #d7d7d7",
".4 c #d8d8d8",
".T c #d9d9d9",
".J c #dcdcdc",
".2 c #dddddd",
".N c #dfdfdf",
".s c #e1e1e1",
".Z c #e2e2e2",
".K c #e4e4e4",
".V c #e7e7e7",
".Q c #e9e9e9",
".t c #eaeaea",
".P c #ececec",
".k c #ededed",
".j c #eeeeee",
".l c #efefef",
".L c #f1f1f1",
".m c #f3f3f3",
".G c #f4f4f4",
".n c #f5f5f5",
".A c #f8f8f8",
".f c #fcfcfc",
".e c #fdfdfd",
"QtQtQtQtQtQt.#.#.#.#.#.#.#.#.#.#.#.#.#QtQtQt",
"QtQtQtQtQtQt.#.a.b.b.b.b.b.b.c.#.d.#.#.#QtQt",
"QtQtQtQtQtQt.#.b.e.f.f.f.f.f.g.#.#.h.#.#.#Qt",
"Qt.#.#.#.#.#.#.i.j.k.j.l.m.n.o.#.p.#.q.#.#.#",
"Qt.#.a.b.b.b.b.b.b.c.#.r.s.t.u.#.v.w.#.q.#.#",
"Qt.#.b.e.f.f.f.f.f.g.#.#.x.y.z.#.#.#.#.#.#.#",
"Qt.#.b.A.A.A.A.A.A.o.#.p.#.B.C.D.C.E.F.#.#.#",
"Qt.#.b.G.m.m.m.m.m.H.#.v.w.#.I.o.J.K.o.#.#.#",
"Qt.#.b.L.l.l.l.l.l.M.#.#.#.#.#.D.o.N.O.#.#.#",
"Qt.#.b.P.Q.Q.Q.Q.Q.R.E.E.E.F.#.S.E.T.U.#.#.#",
"Qt.#.b.V.K.K.K.K.K.K.K.K.K.o.#.W.D.X.Y.#.#.#",
"Qt.#.b.Z.N.N.N.N.N.N.N.N.N.O.#.0.1.o.D.#.#.#",
"Qt.#.b.2.T.T.T.T.T.T.T.T.T.U.#.w.3.M.1.#.#.#",
"Qt.#.b.4.X.X.X.X.X.X.X.X.X.Y.#.5.6.U.7.#.#.#",
"Qt.#.b.X.o.o.o.o.o.o.o.o.o.D.#.8.9#.##.#.#.#",
"Qt.#.b.o.M.M.M.M.M.M.M.M.M.1.#.#.#.#.#.#.#.#",
"Qt.#.b#a.U.U.U.U.U.U.U.U.U.7.#.#.#.#.#.#.#.#",
"Qt.##b.Y#.#.#.#.#.#.#.#.#.##.#.#.#.#.#.#.#.#",
"Qt.#.#.#.#.#.#.#.#.#.#.#.#.#.#.#.#.#.#.#.#.#",
"Qt.#.#.#.#.#.#.#.#.#.#.#.#.#.#.#.#QtQtQtQtQt",
"Qt.#.#.#.#.#.#.#.#.#.#.#.#.#.#.#.#QtQtQtQtQt",
"Qt.#.#.#.#.#.#.#.#.#.#.#.#.#.#.#.#QtQtQtQtQt"};

static const char* const image2_data[] = { 
"22 22 127 2",
"Qt c None",
".# c #000000",
"#5 c #410000",
"#Z c #420000",
"#R c #430000",
"#G c #440000",
"#6 c #4c0000",
"#3 c #4d0000",
".5 c #4f0000",
".2 c #500000",
".T c #520000",
".K c #530000",
"#7 c #600000",
"#8 c #650000",
"#U c #6a0000",
"#x c #790000",
"#Y c #7e0000",
".C c #7f0000",
"#Q c #800000",
"#F c #830000",
"#. c #850000",
"#2 c #860000",
"#w c #890000",
".1 c #990000",
".B c #9d0000",
".J c #a00000",
"#4 c #a10000",
"#y c #a30000",
"#L c #a90000",
"#X c #ab0000",
"#1 c #ac0000",
"#c c #ae0000",
"#T c #b00000",
"#P c #b10000",
"#0 c #b20000",
"#E c #b40000",
"#H c #b50000",
"#v c #b90000",
"#n c #ba0000",
"#W c #bd0000",
"#A c #be0000",
"#K c #c00000",
"#r c #c10000",
"#V c #c10808",
"#i c #c20000",
"#S c #c30000",
"#I c #c40000",
"#z c #c60000",
"#J c #c70000",
".9 c #c80000",
"#o c #c90000",
"#O c #cc2d2d",
"#q c #cd0000",
"#h c #ce0000",
"#D c #ce2b2b",
"#p c #d10000",
"#m c #d20000",
".0 c #d30000",
".r c #d40000",
"#u c #d42b2b",
".4 c #d50000",
"#g c #d60000",
".S c #d70000",
"#l c #d72b2b",
"#N c #d76262",
".U c #d80000",
"#f c #da2222",
"#M c #da7171",
".A c #db0000",
"#C c #db6a6a",
"#b c #dc0000",
".L c #dd0000",
"#t c #df6a6a",
"#a c #e02d2d",
".8 c #e10000",
"#k c #e16a6a",
".7 c #e31313",
".y c #e40000",
"#e c #e46868",
".V c #e50000",
".6 c #e53434",
".3 c #e60000",
"#B c #e69999",
".m c #e70000",
".W c #e90000",
"## c #e97272",
"#s c #e99a9a",
".M c #ea0000",
"#j c #ea9a9a",
".X c #eb1313",
".Z c #ec2222",
".Y c #ed2d2d",
"#d c #ed9a9a",
".F c #ee0000",
".N c #ef0000",
".x c #f10000",
".R c #f12b2b",
".E c #f20000",
".O c #f23434",
".D c #f30000",
".q c #f40000",
".Q c #f46868",
".I c #f52b2b",
".P c #f57272",
".w c #f60000",
".v c #f70000",
".l c #f80000",
".u c #f82b2b",
".H c #f86a6a",
".t c #f95858",
".s c #f96a6a",
".G c #fa9a9a",
".e c #fb0000",
".k c #fb2a2a",
".p c #fb2d2d",
".z c #fb9a9a",
".j c #fc6363",
".o c #fc6a6a",
".h c #fd0606",
".d c #fd5858",
".g c #fd6262",
".i c #fd9393",
".n c #fd9a9a",
".c c #fe9393",
".f c #fe9999",
".a c #ff6b6b",
".b c #ff7171",
"QtQtQtQt.#.#.#.#.#QtQtQtQtQtQt.#.#.#.#.#QtQt",
"QtQt.#.#.#.a.#.#.#.#QtQtQtQt.#.#.b.#.#.#.#.#",
"QtQt.#.#.c.d.e.#.#.#.#QtQt.#.#.f.g.h.#.#.#.#",
"Qt.#.#.i.j.k.l.m.#.#.#.#.#.#.n.o.p.q.r.#.#.#",
"Qt.#.s.t.u.v.w.x.y.#.#.#.#.z.s.u.v.A.B.C.#.#",
"Qt.#.#.D.E.D.D.D.F.y.#.#.G.H.I.D.A.J.K.#.#.#",
"Qt.#.#.#.L.M.F.N.F.F.O.P.Q.R.N.S.B.T.#.#.#.#",
"Qt.#.#.#.#.U.V.W.M.M.X.Y.Z.M.0.1.2.#.#.#.#.#",
"QtQt.#.#.#.#.U.y.3.3.3.3.3.4.1.5.#.#.#.#.#.#",
"QtQtQt.#.#.#.#.6.7.8.8.8.8.9#..#.#.#.#.#.#Qt",
"QtQtQtQt.#.#.####a#b#b#b#b.0#c.#.#.#.#.#QtQt",
"QtQtQtQt.#.##d#e#f.S.S.S.S#g#h#i.#.#.#.#QtQt",
"QtQtQt.#.##j#k#l#m#i#n#o#p#m#p#q#r.#.#.#.#Qt",
"QtQt.#.##s#t#u#h#v#w#x#y#z#q#h#q#o#A.#.#.#.#",
"Qt.#.##B#C#D.9#E#F#G.#.##H#I#J.9#J#K#L.#.#.#",
"Qt.##M#N#O#I#P#Q#R.#.#.#.##H#K#S#I#T#F#U.#.#",
"Qt.#.##V#W#X#Y#Z.#.#.#.#.#.##0#v#1#2#3.#.#.#",
"Qt.#.#.##4#x#5.#.#.#.#.#.#.#.#.J.C#6.#.#.#.#",
"Qt.#.#.#.##7.#.#.#.#.#.#.#.#.#.##8.#.#.#.#.#",
"QtQt.#.#.#.#.#.#.#.#.#QtQt.#.#.#.#.#.#.#.#.#",
"QtQt.#.#.#.#.#.#.#.#QtQtQtQt.#.#.#.#.#.#.#.#",
"QtQtQtQt.#.#.#.#.#QtQtQtQtQtQt.#.#.#.#.#QtQt"};

static const char* const image3_data[] = { 
"22 22 108 2",
"Qt c None",
".# c #000000",
".c c #001a00",
".a c #002c00",
".s c #002d00",
".P c #004300",
".l c #004c00",
".I c #006100",
".g c #006200",
".A c #007900",
".1 c #027402",
".S c #028602",
".2 c #028702",
".d c #046f04",
"#. c #047404",
".b c #047504",
".t c #048a04",
"#q c #049704",
".T c #049804",
".m c #049b04",
"#z c #04a404",
"#r c #04a604",
"#g c #04a804",
".L c #04aa04",
".K c #04bf04",
".Q c #04c004",
".0 c #066504",
".f c #068206",
".k c #068906",
".r c #068d06",
".z c #069306",
".H c #069706",
".O c #069c06",
".V c #06a106",
".5 c #06a606",
"#b c #06ab06",
"#A c #06af06",
"#k c #06b006",
"#s c #06b206",
"#h c #06b306",
".M c #06b406",
".U c #06b506",
"#B c #06b906",
"#H c #06be06",
".B c #06c106",
".J c #06c406",
"#L c #06c706",
"#O c #06d506",
".R c #08c806",
".W c #0a1c0a",
"#G c #0aa20a",
".C c #0ac10a",
"#t c #0cb80c",
"#i c #0cbb0c",
"## c #0cbc0c",
".N c #0cbd0c",
"#y c #0d960d",
"#l c #0f1c0f",
"#P c #0f560f",
".u c #0fbe0f",
"#f c #127412",
"#p c #128710",
".9 c #137012",
"#j c #15bf15",
".D c #15c115",
".Z c #187018",
".h c #189e18",
"#M c #1a311a",
"#a c #22c122",
".3 c #22c222",
".E c #22c322",
"#u c #254425",
"#F c #279d27",
"#x c #289528",
"#o c #309b30",
".v c #30c130",
".6 c #315631",
".e c #319e31",
".n c #38b938",
".4 c #38c438",
".F c #38c538",
"#C c #3a683a",
"#e c #44b144",
"#I c #4c874c",
".8 c #4cb54c",
"#c c #4d894d",
"#K c #4dba4d",
".X c #50b750",
".w c #52c752",
".G c #52c852",
"#E c #55c155",
"#m c #569856",
"#N c #5fa15f",
".Y c #5fd45f",
"#w c #62cf62",
"#v c #6fc66f",
".x c #72ce72",
"#n c #72e372",
"#D c #76d776",
"#J c #76d976",
"#d c #79e779",
".i c #7ac67a",
".7 c #7ee37e",
".o c #8ad28a",
".y c #92d592",
".p c #b0dfb0",
".q c #cbe8cb",
".j c #dff0df",
"QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt.#QtQtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQtQtQt.#.a.b.#QtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQt.#.c.d.e.f.#QtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQt.#.g.h.i.j.k.#QtQtQtQt",
"QtQtQtQtQtQtQtQtQt.#.l.m.n.o.p.q.r.#QtQtQtQt",
"QtQtQtQtQtQtQt.#.s.t.u.v.w.x.y.p.z.#QtQtQtQt",
"QtQtQtQtQtQt.c.A.B.C.D.E.F.G.x.y.H.#QtQtQtQt",
"QtQtQtQt.#.I.J.K.L.M.N.D.E.F.w.x.O.#QtQtQtQt",
"QtQtQt.P.Q.R.S.S.T.L.U.N.D.E.F.w.V.#QtQtQtQt",
"QtQt.W.X.Y.Z.0.1.2.T.L.U.N.D.3.4.5.#QtQtQtQt",
"QtQtQt.#.6.7.8.9#..2.T.L.M##.D#a#b.#QtQtQtQt",
"QtQtQtQt.#.##c#d#e#f.2.T#g#h#i#j#k.#QtQtQtQt",
"QtQtQtQtQtQt.##l#m#n#o#p#q#r#s#t.U.#QtQtQtQt",
"QtQtQtQtQtQtQtQt.##u#v#w#x#y#z#A#B.#QtQtQtQt",
"QtQtQtQtQtQtQtQtQt.#.##C#D#E#F#G#H.#QtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQt.##l#I#J#K#L.#QtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQtQt.##M#N#O.#QtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQtQtQtQt.##P.#QtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt"};

static const char* const image4_data[] = { 
"22 22 108 2",
"Qt c None",
".# c #000000",
".f c #001a00",
".b c #002c00",
".z c #002d00",
".V c #004300",
".r c #004c00",
".O c #006100",
".k c #006200",
".H c #007900",
".0 c #027402",
".S c #028602",
".Z c #028702",
".e c #046f04",
".9 c #047404",
".a c #047504",
".y c #048a04",
"#o c #049704",
".R c #049804",
".q c #049b04",
"#w c #04a404",
"#n c #04a604",
"#g c #04a804",
".L c #04aa04",
".M c #04bf04",
".U c #04c004",
".1 c #066504",
".c c #068206",
".g c #068906",
".l c #068d06",
".s c #069306",
".A c #069706",
".I c #069c06",
".P c #06a106",
".W c #06a606",
".6 c #06ab06",
"#v c #06af06",
"#c c #06b006",
"#m c #06b206",
"#f c #06b306",
".K c #06b406",
".Q c #06b506",
"#u c #06b906",
"#C c #06be06",
".G c #06c106",
".N c #06c406",
"#I c #06c706",
"#M c #06d506",
".T c #08c806",
".5 c #0a1c0a",
"#D c #0aa20a",
".F c #0ac10a",
"#l c #0cb80c",
"#e c #0cbb0c",
".8 c #0cbc0c",
".J c #0cbd0c",
"#x c #0d960d",
"#t c #0f1c0f",
"#P c #0f560f",
".x c #0fbe0f",
"#h c #127412",
"#p c #128710",
"#. c #137012",
"#d c #15bf15",
".E c #15c115",
".2 c #187018",
".j c #189e18",
"#O c #1a311a",
".7 c #22c122",
".Y c #22c222",
".D c #22c322",
"#B c #254425",
"#E c #279d27",
"#y c #289528",
"#q c #309b30",
".w c #30c130",
"#b c #315631",
".d c #319e31",
".p c #38b938",
".X c #38c438",
".C c #38c538",
"#H c #3a683a",
"#i c #44b144",
"#L c #4c874c",
"## c #4cb54c",
"#k c #4d894d",
"#J c #4dba4d",
".4 c #50b750",
".v c #52c752",
".B c #52c852",
"#F c #55c155",
"#s c #569856",
"#N c #5fa15f",
".3 c #5fd45f",
"#z c #62cf62",
"#A c #6fc66f",
".u c #72ce72",
"#r c #72e372",
"#G c #76d776",
"#K c #76d976",
"#j c #79e779",
".i c #7ac67a",
"#a c #7ee37e",
".o c #8ad28a",
".t c #92d592",
".n c #b0dfb0",
".m c #cbe8cb",
".h c #dff0df",
"QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQtQt.#QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQt.#.a.b.#QtQtQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQt.#.c.d.e.f.#QtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQt.#.g.h.i.j.k.#QtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQt.#.l.m.n.o.p.q.r.#QtQtQtQtQtQtQtQtQt",
"QtQtQtQt.#.s.n.t.u.v.w.x.y.z.#QtQtQtQtQtQtQt",
"QtQtQtQt.#.A.t.u.B.C.D.E.F.G.H.fQtQtQtQtQtQt",
"QtQtQtQt.#.I.u.v.C.D.E.J.K.L.M.N.O.#QtQtQtQt",
"QtQtQtQt.#.P.v.C.D.E.J.Q.L.R.S.S.T.U.VQtQtQt",
"QtQtQtQt.#.W.X.Y.E.J.Q.L.R.Z.0.1.2.3.4.5QtQt",
"QtQtQtQt.#.6.7.E.8.K.L.R.Z.9#.###a#b.#QtQtQt",
"QtQtQtQt.##c#d#e#f#g.R.Z#h#i#j#k.#.#QtQtQtQt",
"QtQtQtQt.#.Q#l#m#n#o#p#q#r#s#t.#QtQtQtQtQtQt",
"QtQtQtQt.##u#v#w#x#y#z#A#B.#QtQtQtQtQtQtQtQt",
"QtQtQtQt.##C#D#E#F#G#H.#.#QtQtQtQtQtQtQtQtQt",
"QtQtQtQt.##I#J#K#L#t.#QtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQt.##M#N#O.#QtQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQt.##P.#QtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt",
"QtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQtQt"};


/*
 *  Constructs a BrkDwnShForm_base as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 */
BrkDwnShForm_base::BrkDwnShForm_base( QWidget* parent, const char* name, WFlags fl )
    : SchWin( parent, name, fl ),
      image0( (const char **) image0_data ),
      image1( (const char **) image1_data ),
      image2( (const char **) image2_data ),
      image3( (const char **) image3_data ),
      image4( (const char **) image4_data )
{
    (void)statusBar();
    if ( !name )
	setName( "BrkDwnShForm_base" );
    setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)5, 0, 0, sizePolicy().hasHeightForWidth() ) );
    setCentralWidget( new QWidget( this, "qt_central_widget" ) );
    BrkDwnShForm_baseLayout = new QGridLayout( centralWidget(), 1, 1, 11, 6, "BrkDwnShForm_baseLayout"); 
    QSpacerItem* spacer = new QSpacerItem( 20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed );
    BrkDwnShForm_baseLayout->addItem( spacer, 0, 0 );

    layout16 = new QGridLayout( 0, 1, 1, 0, 6, "layout16"); 

    daynightComboBox = new QComboBox( FALSE, centralWidget(), "daynightComboBox" );
    daynightComboBox->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)4, (QSizePolicy::SizeType)0, 0, 0, daynightComboBox->sizePolicy().hasHeightForWidth() ) );
    daynightComboBox->setMaximumSize( QSize( 100, 32767 ) );

    layout16->addMultiCellWidget( daynightComboBox, 1, 1, 8, 9 );

    pagesLineEdit = new QLineEdit( centralWidget(), "pagesLineEdit" );
    pagesLineEdit->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)4, 0, 0, pagesLineEdit->sizePolicy().hasHeightForWidth() ) );
    pagesLineEdit->setMaximumSize( QSize( 85, 32767 ) );

    layout16->addMultiCellWidget( pagesLineEdit, 1, 1, 10, 11 );

    intextComboBox = new QComboBox( FALSE, centralWidget(), "intextComboBox" );
    intextComboBox->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)4, (QSizePolicy::SizeType)0, 0, 0, intextComboBox->sizePolicy().hasHeightForWidth() ) );
    intextComboBox->setMaximumSize( QSize( 100, 32767 ) );

    layout16->addMultiCellWidget( intextComboBox, 1, 1, 4, 5 );

    TextLabel1 = new QLabel( centralWidget(), "TextLabel1" );

    layout16->addWidget( TextLabel1, 1, 13 );
    QSpacerItem* spacer_2 = new QSpacerItem( 670, 16, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout16->addItem( spacer_2, 0, 7 );

    sceneLineEdit = new QLineEdit( centralWidget(), "sceneLineEdit" );
    sceneLineEdit->setMaximumSize( QSize( 110, 32767 ) );

    layout16->addMultiCellWidget( sceneLineEdit, 1, 1, 2, 3 );
    QSpacerItem* spacer_3 = new QSpacerItem( 20, 16, QSizePolicy::Minimum, QSizePolicy::Minimum );
    layout16->addItem( spacer_3, 0, 1 );
    QSpacerItem* spacer_4 = new QSpacerItem( 50, 16, QSizePolicy::Minimum, QSizePolicy::Minimum );
    layout16->addItem( spacer_4, 0, 3 );

    TextLabel1_2 = new QLabel( centralWidget(), "TextLabel1_2" );
    TextLabel1_2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)4, (QSizePolicy::SizeType)0, 0, 0, TextLabel1_2->sizePolicy().hasHeightForWidth() ) );

    layout16->addWidget( TextLabel1_2, 0, 0 );

    TextLabel3 = new QLabel( centralWidget(), "TextLabel3" );
    TextLabel3->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)4, (QSizePolicy::SizeType)0, 0, 0, TextLabel3->sizePolicy().hasHeightForWidth() ) );

    layout16->addWidget( TextLabel3, 0, 4 );

    setComboBox = new QComboBox( FALSE, centralWidget(), "setComboBox" );
    setComboBox->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, setComboBox->sizePolicy().hasHeightForWidth() ) );

    layout16->addMultiCellWidget( setComboBox, 1, 1, 6, 7 );

    sheetLineEdit = new QLineEdit( centralWidget(), "sheetLineEdit" );
    sheetLineEdit->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)4, (QSizePolicy::SizeType)0, 0, 0, sheetLineEdit->sizePolicy().hasHeightForWidth() ) );
    sheetLineEdit->setMaximumSize( QSize( 50, 32767 ) );

    layout16->addMultiCellWidget( sheetLineEdit, 1, 1, 0, 1 );

    pages2LineEdit = new QLineEdit( centralWidget(), "pages2LineEdit" );
    pages2LineEdit->setMaximumSize( QSize( 30, 32767 ) );

    layout16->addWidget( pages2LineEdit, 1, 12 );

    TextLabel4 = new QLabel( centralWidget(), "TextLabel4" );
    TextLabel4->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, TextLabel4->sizePolicy().hasHeightForWidth() ) );

    layout16->addWidget( TextLabel4, 0, 6 );
    QSpacerItem* spacer_5 = new QSpacerItem( 50, 16, QSizePolicy::Minimum, QSizePolicy::Minimum );
    layout16->addItem( spacer_5, 0, 5 );
    QSpacerItem* spacer_6 = new QSpacerItem( 80, 20, QSizePolicy::Minimum, QSizePolicy::Minimum );
    layout16->addMultiCell( spacer_6, 0, 0, 11, 12 );

    TextLabel6 = new QLabel( centralWidget(), "TextLabel6" );
    TextLabel6->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)4, (QSizePolicy::SizeType)0, 0, 0, TextLabel6->sizePolicy().hasHeightForWidth() ) );

    layout16->addWidget( TextLabel6, 0, 10 );
    QSpacerItem* spacer_7 = new QSpacerItem( 30, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout16->addItem( spacer_7, 0, 9 );

    TextLabel5 = new QLabel( centralWidget(), "TextLabel5" );
    TextLabel5->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)4, (QSizePolicy::SizeType)0, 0, 0, TextLabel5->sizePolicy().hasHeightForWidth() ) );

    layout16->addWidget( TextLabel5, 0, 8 );

    TextLabel2 = new QLabel( centralWidget(), "TextLabel2" );
    TextLabel2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)4, (QSizePolicy::SizeType)0, 0, 0, TextLabel2->sizePolicy().hasHeightForWidth() ) );

    layout16->addWidget( TextLabel2, 0, 2 );

    BrkDwnShForm_baseLayout->addLayout( layout16, 1, 0 );

    layout17 = new QGridLayout( 0, 1, 1, 0, 6, "layout17"); 

    TextLabel13 = new QLabel( centralWidget(), "TextLabel13" );

    layout17->addWidget( TextLabel13, 0, 2 );

    TextLabel14 = new QLabel( centralWidget(), "TextLabel14" );
    TextLabel14->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)1, (QSizePolicy::SizeType)5, 0, 0, TextLabel14->sizePolicy().hasHeightForWidth() ) );

    layout17->addWidget( TextLabel14, 2, 0 );
    QSpacerItem* spacer_8 = new QSpacerItem( 750, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout17->addItem( spacer_8, 0, 3 );

    locComboBox = new QComboBox( FALSE, centralWidget(), "locComboBox" );
    locComboBox->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, locComboBox->sizePolicy().hasHeightForWidth() ) );
    locComboBox->setMaximumSize( QSize( 390, 32767 ) );

    layout17->addMultiCellWidget( locComboBox, 1, 1, 0, 1 );

    TextLabel12 = new QLabel( centralWidget(), "TextLabel12" );

    layout17->addWidget( TextLabel12, 0, 0 );
    QSpacerItem* spacer_9 = new QSpacerItem( 1180, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout17->addMultiCell( spacer_9, 2, 2, 1, 3 );
    QSpacerItem* spacer_10 = new QSpacerItem( 330, 16, QSizePolicy::Minimum, QSizePolicy::Minimum );
    layout17->addItem( spacer_10, 0, 1 );

    genNotesLineEdit = new QLineEdit( centralWidget(), "genNotesLineEdit" );

    layout17->addMultiCellWidget( genNotesLineEdit, 1, 1, 2, 3 );

    BrkDwnShForm_baseLayout->addLayout( layout17, 3, 0 );

    elementsTable = new QTable( centralWidget(), "elementsTable" );
    elementsTable->setNumCols( elementsTable->numCols() + 1 );
    elementsTable->horizontalHeader()->setLabel( elementsTable->numCols() - 1, QString::null );
    elementsTable->setNumCols( elementsTable->numCols() + 1 );
    elementsTable->horizontalHeader()->setLabel( elementsTable->numCols() - 1, QString::null );
    elementsTable->setNumCols( elementsTable->numCols() + 1 );
    elementsTable->horizontalHeader()->setLabel( elementsTable->numCols() - 1, QString::null );
    elementsTable->setNumCols( elementsTable->numCols() + 1 );
    elementsTable->horizontalHeader()->setLabel( elementsTable->numCols() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( elementsTable->numRows() + 1 );
    elementsTable->verticalHeader()->setLabel( elementsTable->numRows() - 1, QString::null );
    elementsTable->setNumRows( 21 );
    elementsTable->setNumCols( 4 );

    BrkDwnShForm_baseLayout->addWidget( elementsTable, 4, 0 );

    layout15 = new QGridLayout( 0, 1, 1, 0, 6, "layout15"); 

    scrpageLineEdit = new QLineEdit( centralWidget(), "scrpageLineEdit" );
    scrpageLineEdit->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, scrpageLineEdit->sizePolicy().hasHeightForWidth() ) );
    scrpageLineEdit->setMaximumSize( QSize( 110, 32767 ) );

    layout15->addMultiCellWidget( scrpageLineEdit, 3, 3, 0, 2 );

    TextLabel8 = new QLabel( centralWidget(), "TextLabel8" );

    layout15->addMultiCellWidget( TextLabel8, 2, 2, 0, 1 );

    TextLabel7 = new QLabel( centralWidget(), "TextLabel7" );

    layout15->addWidget( TextLabel7, 0, 0 );

    seqComboBox = new QComboBox( FALSE, centralWidget(), "seqComboBox" );
    seqComboBox->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, seqComboBox->sizePolicy().hasHeightForWidth() ) );
    seqComboBox->setMaximumSize( QSize( 750, 32767 ) );

    layout15->addMultiCellWidget( seqComboBox, 3, 3, 7, 8 );

    scrdayComboBox = new QComboBox( FALSE, centralWidget(), "scrdayComboBox" );
    scrdayComboBox->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, scrdayComboBox->sizePolicy().hasHeightForWidth() ) );
    scrdayComboBox->setMaximumSize( QSize( 300, 32767 ) );

    layout15->addMultiCellWidget( scrdayComboBox, 3, 3, 3, 4 );

    TextLabel10 = new QLabel( centralWidget(), "TextLabel10" );

    layout15->addWidget( TextLabel10, 2, 5 );

    unitComboBox = new QComboBox( FALSE, centralWidget(), "unitComboBox" );
    unitComboBox->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, unitComboBox->sizePolicy().hasHeightForWidth() ) );
    unitComboBox->setMaximumSize( QSize( 100, 32767 ) );

    layout15->addMultiCellWidget( unitComboBox, 3, 3, 5, 6 );

    TextLabel11 = new QLabel( centralWidget(), "TextLabel11" );

    layout15->addWidget( TextLabel11, 2, 7 );

    TextLabel9 = new QLabel( centralWidget(), "TextLabel9" );

    layout15->addWidget( TextLabel9, 2, 3 );

    synopsisLineEdit = new QLineEdit( centralWidget(), "synopsisLineEdit" );

    layout15->addMultiCellWidget( synopsisLineEdit, 1, 1, 0, 8 );
    QSpacerItem* spacer_11 = new QSpacerItem( 1188, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout15->addMultiCell( spacer_11, 0, 0, 1, 8 );
    QSpacerItem* spacer_12 = new QSpacerItem( 70, 16, QSizePolicy::Minimum, QSizePolicy::Minimum );
    layout15->addItem( spacer_12, 2, 6 );
    QSpacerItem* spacer_13 = new QSpacerItem( 20, 16, QSizePolicy::Minimum, QSizePolicy::Minimum );
    layout15->addItem( spacer_13, 2, 2 );
    QSpacerItem* spacer_14 = new QSpacerItem( 670, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout15->addItem( spacer_14, 2, 8 );
    QSpacerItem* spacer_15 = new QSpacerItem( 210, 20, QSizePolicy::Minimum, QSizePolicy::Minimum );
    layout15->addItem( spacer_15, 2, 4 );

    BrkDwnShForm_baseLayout->addLayout( layout15, 2, 0 );

    // actions
    copySheetAction = new QAction( this, "copySheetAction" );
    newSheetAction = new QAction( this, "newSheetAction" );
    delSheetAction = new QAction( this, "delSheetAction" );
    gotoPrevAction = new QAction( this, "gotoPrevAction" );
    gotoNextAction = new QAction( this, "gotoNextAction" );


    // toolbars
    toolsToolbar = new QToolBar( QString(""), this, DockTop ); 


    newToolButton = new QToolButton( toolsToolbar, "newToolButton" );
    newToolButton->setIconSet( QIconSet( image0 ) );

    copyToolButton = new QToolButton( toolsToolbar, "copyToolButton" );
    copyToolButton->setIconSet( QIconSet( image1 ) );

    deleteToolButton = new QToolButton( toolsToolbar, "deleteToolButton" );
    deleteToolButton->setIconSet( QIconSet( image2 ) );
    toolsToolbar->addSeparator();

    gotoPrevToolButton = new QToolButton( toolsToolbar, "gotoPrevToolButton" );
    gotoPrevToolButton->setIconSet( QIconSet( image3 ) );

    gotoNextToolButton = new QToolButton( toolsToolbar, "gotoNextToolButton" );
    gotoNextToolButton->setIconSet( QIconSet( image4 ) );


    // menubar
    menubar = new QMenuBar( this, "menubar" );


    languageChange();
    resize( QSize(1024, 768).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( gotoNextToolButton, SIGNAL( clicked() ), this, SLOT( gotoNextSheet() ) );
    connect( copyToolButton, SIGNAL( clicked() ), this, SLOT( copySheet() ) );
    connect( newToolButton, SIGNAL( clicked() ), this, SLOT( newSheet() ) );
    connect( gotoPrevToolButton, SIGNAL( clicked() ), this, SLOT( gotoPrevSheet() ) );
    connect( deleteToolButton, SIGNAL( clicked() ), this, SLOT( deleteSheet() ) );
    connect( scrdayComboBox, SIGNAL( activated(const QString&) ), this, SLOT( scrdayCmbBoxActivated(const QString&) ) );
    connect( unitComboBox, SIGNAL( activated(const QString&) ), this, SLOT( unitCmbBoxActivated(const QString&) ) );
    connect( seqComboBox, SIGNAL( activated(const QString&) ), this, SLOT( seqCmbBoxActivated(const QString&) ) );
    connect( locComboBox, SIGNAL( activated(const QString&) ), this, SLOT( locCmbBoxActivated(const QString&) ) );
    connect( setComboBox, SIGNAL( activated(const QString&) ), this, SLOT( setCmbBoxActivated(const QString&) ) );
    connect( intextComboBox, SIGNAL( activated(int) ), this, SLOT( intextCmbBoxActivated(int) ) );
    connect( daynightComboBox, SIGNAL( activated(int) ), this, SLOT( daynightCmbBoxActivated(int) ) );

    // tab order
    setTabOrder( sheetLineEdit, sceneLineEdit );
    setTabOrder( sceneLineEdit, intextComboBox );
    setTabOrder( intextComboBox, setComboBox );
    setTabOrder( setComboBox, daynightComboBox );
    setTabOrder( daynightComboBox, pagesLineEdit );
    setTabOrder( pagesLineEdit, pages2LineEdit );
    setTabOrder( pages2LineEdit, synopsisLineEdit );
    setTabOrder( synopsisLineEdit, scrpageLineEdit );
    setTabOrder( scrpageLineEdit, scrdayComboBox );
    setTabOrder( scrdayComboBox, unitComboBox );
    setTabOrder( unitComboBox, seqComboBox );
    setTabOrder( seqComboBox, locComboBox );
    setTabOrder( locComboBox, genNotesLineEdit );
    setTabOrder( genNotesLineEdit, elementsTable );

    // buddies
    TextLabel1_2->setBuddy( sheetLineEdit );
    TextLabel2->setBuddy( sceneLineEdit );
}

/*
 *  Destroys the object and frees any allocated resources
 */
BrkDwnShForm_base::~BrkDwnShForm_base()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void BrkDwnShForm_base::languageChange()
{
    setCaption( tr( "Breakdown Sheet" ) );
    daynightComboBox->clear();
    daynightComboBox->insertItem( tr( "Day" ) );
    daynightComboBox->insertItem( tr( "Night" ) );
    intextComboBox->clear();
    intextComboBox->insertItem( tr( "Internal" ) );
    intextComboBox->insertItem( tr( "External" ) );
    TextLabel1->setText( tr( "/8" ) );
    TextLabel1_2->setText( tr( "Sheet" ) );
    TextLabel3->setText( tr( "Int/Ext" ) );
    TextLabel4->setText( tr( "Set" ) );
    TextLabel6->setText( tr( "Page(s)" ) );
    TextLabel5->setText( tr( "Day/Night" ) );
    TextLabel2->setText( tr( "Scene(s)" ) );
    TextLabel13->setText( tr( "General Notes" ) );
    TextLabel14->setText( tr( "Elements" ) );
    TextLabel12->setText( tr( "Location" ) );
    elementsTable->horizontalHeader()->setLabel( 0, QString::null );
    elementsTable->horizontalHeader()->setLabel( 1, QString::null );
    elementsTable->horizontalHeader()->setLabel( 2, QString::null );
    elementsTable->horizontalHeader()->setLabel( 3, QString::null );
    elementsTable->verticalHeader()->setLabel( 0, QString::null );
    elementsTable->verticalHeader()->setLabel( 1, QString::null );
    elementsTable->verticalHeader()->setLabel( 2, QString::null );
    elementsTable->verticalHeader()->setLabel( 3, QString::null );
    elementsTable->verticalHeader()->setLabel( 4, QString::null );
    elementsTable->verticalHeader()->setLabel( 5, QString::null );
    elementsTable->verticalHeader()->setLabel( 6, QString::null );
    elementsTable->verticalHeader()->setLabel( 7, QString::null );
    elementsTable->verticalHeader()->setLabel( 8, QString::null );
    elementsTable->verticalHeader()->setLabel( 9, QString::null );
    elementsTable->verticalHeader()->setLabel( 10, QString::null );
    elementsTable->verticalHeader()->setLabel( 11, QString::null );
    elementsTable->verticalHeader()->setLabel( 12, QString::null );
    elementsTable->verticalHeader()->setLabel( 13, QString::null );
    elementsTable->verticalHeader()->setLabel( 14, QString::null );
    elementsTable->verticalHeader()->setLabel( 15, QString::null );
    elementsTable->verticalHeader()->setLabel( 16, QString::null );
    elementsTable->verticalHeader()->setLabel( 17, QString::null );
    elementsTable->verticalHeader()->setLabel( 18, QString::null );
    elementsTable->verticalHeader()->setLabel( 19, QString::null );
    elementsTable->verticalHeader()->setLabel( 20, QString::null );
    TextLabel8->setText( tr( "Script Page(s)" ) );
    TextLabel7->setText( tr( "Synopsis" ) );
    TextLabel10->setText( tr( "Unit" ) );
    TextLabel11->setText( tr( "Sequence" ) );
    TextLabel9->setText( tr( "Script Day" ) );
    copySheetAction->setText( tr( "Action" ) );
    newSheetAction->setText( tr( "Action" ) );
    delSheetAction->setText( tr( "Action" ) );
    gotoPrevAction->setText( tr( "Action" ) );
    gotoNextAction->setText( tr( "Action" ) );
    toolsToolbar->setLabel( tr( "Toolbar_2" ) );
    newToolButton->setText( tr( "", "New Sheet" ) );
    copyToolButton->setText( tr( "", "Copy Sheet" ) );
    deleteToolButton->setText( tr( "", "Delete Sheet" ) );
    gotoPrevToolButton->setText( tr( "", "Goto Previous Sheet" ) );
    gotoNextToolButton->setText( tr( "", "Goto Next Sheet" ) );
}

void BrkDwnShForm_base::copySheet()
{
    qWarning( "BrkDwnShForm_base::copySheet(): Not implemented yet" );
}

void BrkDwnShForm_base::deleteSheet()
{
    qWarning( "BrkDwnShForm_base::deleteSheet(): Not implemented yet" );
}

void BrkDwnShForm_base::gotoNextSheet()
{
    qWarning( "BrkDwnShForm_base::gotoNextSheet(): Not implemented yet" );
}

void BrkDwnShForm_base::gotoPrevSheet()
{
    qWarning( "BrkDwnShForm_base::gotoPrevSheet(): Not implemented yet" );
}

void BrkDwnShForm_base::newSheet()
{
    qWarning( "BrkDwnShForm_base::newSheet(): Not implemented yet" );
}

void BrkDwnShForm_base::save()
{
    qWarning( "BrkDwnShForm_base::save(): Not implemented yet" );
}

void BrkDwnShForm_base::saveAs()
{
    qWarning( "BrkDwnShForm_base::saveAs(): Not implemented yet" );

}

void BrkDwnShForm_base::initForm()
{
    qWarning( "BrkDwnShForm_base::initForm(): Not implemented yet" );
}

void BrkDwnShForm_base::setSheet(int)
{
    qWarning( "BrkDwnShForm_base::setSheet(int): Not implemented yet" );
}

void BrkDwnShForm_base::updateListBox()
{
    qWarning( "BrkDwnShForm_base::updateListBox(): Not implemented yet" );
}

void BrkDwnShForm_base::scrdayCmbBoxActivated(const QString&)
{
    qWarning( "BrkDwnShForm_base::scrdayCmbBoxActivated(const QString&): Not implemented yet" );
}

void BrkDwnShForm_base::setCmbBoxActivated(const QString&)
{
    qWarning( "BrkDwnShForm_base::setCmbBoxActivated(const QString&): Not implemented yet" );
}

void BrkDwnShForm_base::seqCmbBoxActivated(const QString&)
{
    qWarning( "BrkDwnShForm_base::seqCmbBoxActivated(const QString&): Not implemented yet" );
}

void BrkDwnShForm_base::locCmbBoxActivated(const QString&)
{
    qWarning( "BrkDwnShForm_base::locCmbBoxActivated(const QString&): Not implemented yet" );
}

void BrkDwnShForm_base::intextCmbBoxActivated(int)
{
    qWarning( "BrkDwnShForm_base::intextCmbBoxActivated(int): Not implemented yet" );
}

void BrkDwnShForm_base::unitCmbBoxActivated(const QString&)
{
    qWarning( "BrkDwnShForm_base::unitCmbBoxActivated(const QString&): Not implemented yet" );
}

void BrkDwnShForm_base::daynightCmbBoxActivated(int)
{
    qWarning( "BrkDwnShForm_base::daynightCmbBoxActivated(int): Not implemented yet" );
}
void BrkDwnShForm_base::setStripBoard(StripBoardForm*)
{
    
}
