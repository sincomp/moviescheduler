/****************************************************************************
** Form interface generated from reading ui file 'brkdwnshform_frame_.ui'
**
** Created: Fri Apr 22 02:10:18 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.2.1   edited May 19 14:22 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef BRKDWNSHFORM_BASE_H
#define BRKDWNSHFORM_BASE_H

#include <qvariant.h>
#include <qpixmap.h>
//#include <qmainwindow.h>
#include "schwin.h"
class StripBoardForm;

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QAction;
class QActionGroup;
class QToolBar;
class QPopupMenu;
class QComboBox;
class QLineEdit;
class QLabel;
class QTable;
class QToolButton;

class BrkDwnShForm_base : public SchWin
{
    Q_OBJECT

public:
    BrkDwnShForm_base( QWidget* parent = 0, const char* name = 0, WFlags fl = WType_TopLevel );
    ~BrkDwnShForm_base();

    QComboBox* daynightComboBox;
    QLineEdit* pagesLineEdit;
    QComboBox* intextComboBox;
    QLabel* TextLabel1;
    QLineEdit* sceneLineEdit;
    QLabel* TextLabel1_2;
    QLabel* TextLabel3;
    QComboBox* setComboBox;
    QLineEdit* sheetLineEdit;
    QLineEdit* pages2LineEdit;
    QLabel* TextLabel4;
    QLabel* TextLabel6;
    QLabel* TextLabel5;
    QLabel* TextLabel2;
    QLabel* TextLabel13;
    QLabel* TextLabel14;
    QComboBox* locComboBox;
    QLabel* TextLabel12;
    QLineEdit* genNotesLineEdit;
    QTable* elementsTable;
    QLineEdit* scrpageLineEdit;
    QLabel* TextLabel8;
    QLabel* TextLabel7;
    QComboBox* seqComboBox;
    QComboBox* scrdayComboBox;
    QLabel* TextLabel10;
    QComboBox* unitComboBox;
    QLabel* TextLabel11;
    QLabel* TextLabel9;
    QLineEdit* synopsisLineEdit;
    QToolButton* newToolButton;
    QToolButton* copyToolButton;
    QToolButton* deleteToolButton;
    QToolButton* gotoPrevToolButton;
    QToolButton* gotoNextToolButton;
    QMenuBar *menubar;
    QToolBar *toolsToolbar;
    QAction* copySheetAction;
    QAction* newSheetAction;
    QAction* delSheetAction;
    QAction* gotoPrevAction;
    QAction* gotoNextAction;

public slots:
    virtual void copySheet();
    virtual void deleteSheet();
    virtual void gotoNextSheet();
    virtual void gotoPrevSheet();
    virtual void newSheet();
    virtual void save();
    //ispravljeno sa bool saveAs na void saveAs
    virtual void saveAs();
    virtual void initForm();
    virtual void setSheet(int);
    virtual void updateListBox();
    virtual void scrdayCmbBoxActivated(const QString&);
    virtual void setCmbBoxActivated(const QString&);
    virtual void seqCmbBoxActivated(const QString&);
    virtual void locCmbBoxActivated(const QString&);
    virtual void intextCmbBoxActivated(int);
    virtual void unitCmbBoxActivated(const QString&);
    virtual void daynightCmbBoxActivated(int);
    virtual void setStripBoard(StripBoardForm*);
   
signals:
    void message(const QString&, int );

protected:
    QString filename;

    QGridLayout* BrkDwnShForm_baseLayout;
    QGridLayout* layout16;
    QGridLayout* layout17;
    QGridLayout* layout15;

protected slots:
    virtual void languageChange();

private:
    QPixmap image0;
    QPixmap image1;
    QPixmap image2;
    QPixmap image3;
    QPixmap image4;

};

#endif // BRKDWNSHFORM_BASE_H
