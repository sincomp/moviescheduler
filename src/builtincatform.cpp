#include<qlineedit.h>

#include "builtincatform.h"

/* 
 *  Constructs a BuiltInCatForm which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f' 
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
BuiltInCatForm::BuiltInCatForm(BuiltInCategories *bupl,ElManForm* parent,int br_el,  const char* name, bool modal, WFlags fl )
    : BuiltInCatForm_base( parent, name, modal, fl )
{
   un_built_ptr_list=bupl;
   broj_elementa=br_el;
   nameLineEdit->setText("New Element");
   nameLineEdit->setFocus();
   un_built_ptr_list->setAutoDelete(TRUE);
   p_parent=parent;
   initForm(); 
}

/*  
 *  Destroys the object and frees any allocated resources
 */
BuiltInCatForm::~BuiltInCatForm()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 * public slot
 */
void BuiltInCatForm::napuniKategoriju()
{
    if((pbcat=un_built_ptr_list->at(broj_elementa))==0)
        pbcat=new Elements;
    pbcat->setElementName(nameLineEdit->text());
    //pbcat->setSheetNum(p_parent->current_sheet->getSheetNum());
    //pbcat->setSheetNum(3);
        unsigned int s=0;
        QString blank="";
		if(broj_elementa<0)
		{
				un_built_ptr_list->append(pbcat);
				s=1;
				this->accept();
        }
		if(!s)
		{
      //za laptop ne postoji replace
			//un_built_ptr_list->replace(broj_elementa,pbcat);
     // un_built_ptr_list->remove(broj_elementa);
      //un_built_ptr_list->insert(broj_elementa,pbcat);
           if(broj_elementa>=0) this->accept();
		}
       /* if(un_built_ptr_list->first()->getElementName()!=QString::fromUtf8("…",3))
        {
            pbcat=new Elements;
            pbcat->setElementName(blank);
            un_built_ptr_list->insert(0,pbcat);

        }    */
        p_parent->initTable();
        p_parent->updateBuiltSheet(*un_built_ptr_list);
        //qWarning( "BuiltInCatForm::napuniKategoriju() not yet implemented!" );
}

/*
 * public slot
 */
void BuiltInCatForm::initForm()
{
    if(broj_elementa>=0)
    {
        Elements *pcat;
        pcat=un_built_ptr_list->at(broj_elementa);
        nameLineEdit->setText(QString(pcat->getElementName()) );
        
    }
    //qWarning( "BuiltInCatForm::initTable() not yet implemented!" );
}


