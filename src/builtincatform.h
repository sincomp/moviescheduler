#ifndef BUILTINCATFORM_H
#define BUILTINCATFORM_H
#include "builtincatform_base.h"

class BuiltInCatForm : public BuiltInCatForm_base
{
    Q_OBJECT

public:
    BuiltInCatForm(BuiltInCategories *bupl ,ElManForm* parent = 0,int br_el=-1, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~BuiltInCatForm();

public slots:
    void napuniKategoriju();
    void initForm();

};

#endif // BUILTINCATFORM_H
