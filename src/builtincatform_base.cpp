/****************************************************************************
** Form implementation generated from reading ui file 'builtincatform_base.ui'
**
** Created: Sat Mar 5 15:21:00 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.2.1   edited May 19 14:22 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "builtincatform_base.h"

#include <qvariant.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a BuiltInCatForm_base as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
BuiltInCatForm_base::BuiltInCatForm_base( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
	setName( "BuiltInCatForm_base" );
//    setModal( TRUE );
    BuiltInCatForm_baseLayout = new QGridLayout( this, 1, 1, 11, 6, "BuiltInCatForm_baseLayout"); 

    textLabel1 = new QLabel( this, "textLabel1" );

    BuiltInCatForm_baseLayout->addWidget( textLabel1, 0, 0 );

    nameLineEdit = new QLineEdit( this, "nameLineEdit" );
    nameLineEdit->setMaximumSize( QSize( 350, 32767 ) );

    BuiltInCatForm_baseLayout->addMultiCellWidget( nameLineEdit, 1, 1, 0, 2 );

    okPushButton = new QPushButton( this, "okPushButton" );

    BuiltInCatForm_baseLayout->addWidget( okPushButton, 2, 1 );

    cancelPushButton = new QPushButton( this, "cancelPushButton" );

    BuiltInCatForm_baseLayout->addWidget( cancelPushButton, 2, 2 );
    languageChange();
    resize( QSize(306, 110).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( okPushButton, SIGNAL( clicked() ), this, SLOT( napuniKategoriju() ) );
    connect( cancelPushButton, SIGNAL( clicked() ), this, SLOT( close() ) );

    // tab order
    setTabOrder( nameLineEdit, okPushButton );
    setTabOrder( okPushButton, cancelPushButton );
}

/*
 *  Destroys the object and frees any allocated resources
 */
BuiltInCatForm_base::~BuiltInCatForm_base()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void BuiltInCatForm_base::languageChange()
{
    setCaption( tr( "Built-In Categories" ) );
    textLabel1->setText( tr( "Enter item name:" ) );
    okPushButton->setText( tr( "&OK" ) );
    okPushButton->setAccel( QKeySequence( tr( "Alt+O" ) ) );
    cancelPushButton->setText( tr( "Cancel" ) );
}

void BuiltInCatForm_base::napuniKategoriju()
{
    qWarning( "BuiltInCatForm_base::napuniKategoriju(): Not implemented yet" );
}

void BuiltInCatForm_base::initForm()
{
    qWarning( "BuiltInCatForm_base::initForm(): Not implemented yet" );
}

