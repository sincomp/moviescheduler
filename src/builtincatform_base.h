/****************************************************************************
** Form interface generated from reading ui file 'builtincatform_base.ui'
**
** Created: Sat Mar 5 15:20:53 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.2.1   edited May 19 14:22 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef BUILTINCATFORM_BASE_H
#define BUILTINCATFORM_BASE_H

#include <qvariant.h>
#include <qdialog.h>
//#include"categories.h"
#include "elmanform.h"

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QLabel;
class QLineEdit;
class QPushButton;

class BuiltInCatForm_base : public QDialog
{
    Q_OBJECT

public:
    BuiltInCatForm_base( QWidget* parent = 0, const char* name = 0, bool modal = true, WFlags fl = 0 );
    ~BuiltInCatForm_base();

    QLabel* textLabel1;
    QLineEdit* nameLineEdit;
    QPushButton* okPushButton;
    QPushButton* cancelPushButton;

public slots:
    virtual void napuniKategoriju();
    virtual void initForm();

protected:
    Elements* pbcat;
    int broj_elementa;
    ElManForm* p_parent;
    BuiltInCategories *un_built_ptr_list;

    QGridLayout* BuiltInCatForm_baseLayout;

protected slots:
    virtual void languageChange();

};

#endif // BUILTINCATFORM_BASE_H
