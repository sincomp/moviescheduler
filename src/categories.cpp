/***************************************************************************
                          categories.cpp  -  description
                             -------------------
    begin                : Tue Mar 22 2005
    copyright            : (C) 2005 by Sinisa Denic
    email                : sincomp@ptt.yu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include"categories.h"
#include<qtextstream.h>
#include <qstringlist.h>

QString NUL="";
CastMembers::CastMembers()
{
  elementName = "";
  boardID = "" ;
  locked =  0;
  excl = 0;
//  sheets_num = e.attribute( "sheets_num", "" );
  occurrence=0;
  finishedDate = "";
  totalDays=0;
  startDate="";
  fullName="";
  phone="";
  agent="";
  agentPhone="";
  address="";
  is_start_set=false;
  is_el_removed=false;
}
CastMembers::CastMembers( const QDomElement &e )
{
  elementName = e.attribute( "elementName", "" );
  boardID = e.attribute( "boardID", "" );
  locked = e.attribute( "locked", "" ).toInt();
  excl= e.attribute( "excl", "" ).toInt();
  num_string = e.attribute( "num_string", "" );
 // occurrence =e.attribute( "occurrence", "" ).toInt();
  finishedDate = "";//e.attribute( "finishedDate", "" );
 // totalDays = e.attribute( "totalDays", "" ).toInt();
  //startDate=e.attribute( "startDate", "" );
  fullName=e.attribute( "fullName", "" );
  phone=e.attribute( "phone","" );
  agent=e.attribute( "agent","" );
  agentPhone=e.attribute( "agentPhone","");
  address=e.attribute( "address","" );
  appendSheetNum(num_string);
  is_start_set=e.attribute( "isStartSet","").toInt();
  is_el_removed=false;
}
 
QDomElement CastMembers::ElementsToXMLNode(QDomDocument& d,const CastMembers& cast)
{
      
      QDomElement cn = d.createElement( "cast" );
   cn.setAttribute( "elementName", cast.elementName );
    cn.setAttribute( "boardID", cast.boardID );
    cn.setAttribute( "locked", QString::number(cast.locked) );
    cn.setAttribute( "excl", QString::number(cast.excl) );
//  cn.setAttribute( "sheets_num", cast.sheets_num );
  cn.setAttribute( "num_string", getSheetsNumString());
 // cn.setAttribute( "occurrence", QString::number(cast.occurrence) );
 // cn.setAttribute( "finishedDate", cast.finishedDate );
 // cn.setAttribute( "totalDays", QString::number(cast.totalDays));
 // cn.setAttribute( "startDate", cast.startDate );
   cn.setAttribute( "fullName", cast.fullName );
   cn.setAttribute( "phone", cast.phone );
   cn.setAttribute( "agent", cast.agent );
   cn.setAttribute( "agentPhone", cast.agentPhone );
   cn.setAttribute( "address", cast.address );
   cn.setAttribute( "isStartSet","0" );
   

   return cn;
}

QString CastMembers::getFullName()const {if(!fullName.isEmpty())return fullName ;
    else return NUL;}

QString CastMembers::getAddress()const { if(!address.isEmpty())return address ;
    else return NUL;}

QString CastMembers::getPhone()const { if(!phone.isEmpty())return phone ;
    else return NUL;}

QString CastMembers::getAgent()const { if(!agent.isEmpty())return agent ;
    else return NUL;}

QString CastMembers::getAgentPhone()const { if(!agentPhone.isEmpty())return agentPhone ;
    else return NUL;}

PayElements::PayElements()
{
  elementName = "";
  boardID = "" ;
  locked =  0;
  excl = 0;
  num_string="";
  occurrence=0;
  finishedDate = "";
  totalDays=0;
  startDate="";
  pay="";
  ppay="";
  minimum="";
}
PayElements::PayElements( const QDomElement &e )
{
  elementName = e.attribute( "elementName", "" );
  boardID = e.attribute( "boardID", "" );
  locked =e.attribute( "locked","" ).toInt();
  excl= e.attribute( "excl", "" ).toInt();
  num_string = e.attribute( "num_string", "" );
  //QString::number(occurrence) =e.attribute( "occurrence", "" );
  finishedDate = "";//e.attribute( "finishedDate", "" );
//  QString::number(totalDays) = e.attribute( "totalDays", "" );
  //startDate=e.attribute( "startDate", "" );
  pay=e.attribute( "pay", "" );
  ppay=e.attribute( "ppay", "");
  minimum=e.attribute( "minimum", "");
  appendSheetNum(num_string);
}

QDomElement PayElements::ElementsToXMLNode(QDomDocument& d,const PayElements& cast)
{

      QDomElement cn = d.createElement( "payelem" );
   cn.setAttribute( "elementName", cast.elementName );
    cn.setAttribute( "boardID", cast.boardID );
    cn.setAttribute( "locked", cast.locked );
    cn.setAttribute( "excl", cast.excl );
//  cn.setAttribute( "sheets_num", cast.sheets_num );
  cn.setAttribute( "num_string", getSheetsNumString());
  cn.setAttribute( "occurrence", cast.occurrence );
  cn.setAttribute( "finishedDate", cast.finishedDate );
  cn.setAttribute( "totalDays", cast.totalDays );
  cn.setAttribute( "startDate", cast.startDate );
  cn.setAttribute( "pay", cast.pay);
  cn.setAttribute( "ppay", cast.ppay);
  cn.setAttribute( "minimum", cast.minimum);
   return cn;
}
QString PayElements::getPay()const  {if(!pay.isEmpty())return  pay;
        else return NUL;}
QString PayElements::getPay1()const  {if(!ppay.isEmpty())return  ppay;
        else return NUL;}
QString PayElements::getMinimum()const  {if(!minimum.isEmpty())return minimum;
        else return NUL;}

//Stunts class
Stunts::Stunts()
{
  elementName = "";
  boardID = "" ;
  locked =  0;
  excl = 0;
  num_string="";
  occurrence=0;
  finishedDate = "";
  totalDays=0;
  startDate="";
  notes="";
  contactNames=""; 
}
Stunts::Stunts( const QDomElement &e )
{
  elementName = e.attribute( "elementName", "" );
  boardID = e.attribute( "boardID", "" );
  locked =e.attribute( "locked","" ).toInt();
  excl= e.attribute( "excl", "" ).toInt();
  num_string = e.attribute( "num_string", "" );
 // QString::number(occurrence) =e.attribute( "occurrence", "" );
  finishedDate ="";// e.attribute( "finishedDate", "" );
 // QString::number(totalDays) = e.attribute( "totalDays", "" );
 // startDate=e.attribute( "startDate", "" );
  contactNames=e.attribute( "contactNames", "" );
  notes=e.attribute( "notes", "");
  appendSheetNum(num_string);
}

QDomElement Stunts::ElementsToXMLNode(QDomDocument& d,const Stunts& cast)
{

      QDomElement cn = d.createElement( "stunt" );
   cn.setAttribute( "elementName", cast.elementName );
    cn.setAttribute( "boardID", cast.boardID );
    cn.setAttribute( "locked", cast.locked );
    cn.setAttribute( "excl", cast.excl );
//  cn.setAttribute( "sheets_num", cast.sheets_num );
  cn.setAttribute( "num_string", getSheetsNumString());
  cn.setAttribute( "occurrence", cast.occurrence );
  cn.setAttribute( "finishedDate", cast.finishedDate );
  cn.setAttribute( "totalDays", cast.totalDays );
  cn.setAttribute( "startDate", cast.startDate );
  cn.setAttribute( "contactNames", cast.contactNames);
  cn.setAttribute( "notes", cast.notes);
  
   return cn;
}



QString Stunts::getContactNames()const{if(!contactNames.isEmpty())return contactNames;
        else return NUL;}
QString Stunts::getNotes()const {if(!notes.isEmpty())return notes;
        else return NUL;}


