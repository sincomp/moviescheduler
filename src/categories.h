#include <string.h>
#include "elements.h"
#include <qptrlist.h>
#ifndef MYPTRLIST_H
#define MYPTRLIST_H

template<class type>
class MyPrtList
#ifdef QPTRLIST_H
    :public QPtrList<type>
#endif
{
    public:
    MyPrtList(){};
    MyPrtList( const MyPrtList<type>& list):QPtrList<type>(list){}
   void sort(int);
    bool operator==( const MyPrtList<type> &) const ;
    
    // protected:
     int compareItems(QPtrCollection::Item s1, QPtrCollection::Item s2);

     private:

     unsigned int i,j,len;//len - number elements 
     type temp;
};
#endif
template<class type>
bool MyPrtList<type>::operator==(const MyPrtList<type>& list) const
{
   return  this == &list ;
   } 
template<class type>
int MyPrtList<type>::compareItems(QPtrCollection::Item s1, QPtrCollection::Item s2)
{
     if ( *((type*)s1) == *((type*)s2) ) return 0;
      return ( *((type*)s1) < *((type*)s2) ? -1 : 1) ;
      }


template<class type>
void MyPrtList<type>::sort(int bk)
{
    len=this->count();
    for(i=0;i<this->count()-1;++i)
        for(j=i+1;j<this->count();++j)
        {
            this->at(i)->setSortBy(bk);
            this->at(j)->setSortBy(bk);
            if(compareItems(this->at(i),this->at(j))>0)
            {
                temp=*this->at(i);
                *this->at(i)=*this->at(j);
                *this->at(j)=temp;
            }
        }   

}         
class CastMembers : public Elements
{
  private:
	
	QString fullName;
	QString address;
	QString phone;
	QString agent;
	QString agentPhone;
 public:
    CastMembers();
 	CastMembers(const QDomElement&);
   // virtual ~CastMembers(){};
	QDomElement ElementsToXMLNode(QDomDocument& d,const CastMembers& cm);
	inline void setFullName(const QString& name) {fullName =  name ; }
	inline void setAddress(const QString& add) { address = add ; }
	inline void setPhone(const QString& phn) { phone = phn ; }
	inline void setAgent(const QString& ag) { agent = ag ; }
	inline void setAgentPhone(const QString& agph) { agentPhone = agph ; }

	// get metodi

	
	 QString getFullName()const ;

     QString getAddress()const ;
    
	QString getPhone()const ;
	 QString getAgent()const ;
    
	 QString getAgentPhone()const ;
};
  typedef MyPrtList<CastMembers> CastPtrList;


class PayElements:public Elements
{
    private:
        QString pay;
        QString ppay;
        QString minimum;
    public:
        PayElements();
        PayElements(const QDomElement&);
        QDomElement ElementsToXMLNode(QDomDocument& d,const PayElements& cm);
        inline void setPay(const QString& p) {pay=p;}
        inline void setPay1(const QString& p1) {ppay=p1;}
        inline void setMinimum(const QString& m) {minimum=m;}

        QString getPay()const;
        QString getPay1()const;
        QString getMinimum()const;
};
typedef MyPrtList<PayElements> PayElPtrList;
//typedef QPtrList<PayElements> PayElPtrList;


class Stunts:public Elements
{
    private:
        QString contactNames;
        QString notes;
    public:
        Stunts() ;
        Stunts(const QDomElement&);
        QDomElement ElementsToXMLNode(QDomDocument& d,const Stunts& cm);
        inline void setContactNames(const QString& c){contactNames=c;}
        inline void setNotes(const QString& c){notes=c;}

        QString getContactNames()const;
        QString getNotes()const;
    };
    
  typedef MyPrtList<Stunts> StuntsPtrList;

 typedef MyPrtList<Elements>BuiltInCategories;
 //typedef QPtrList<Elements>BuiltInCategories;
