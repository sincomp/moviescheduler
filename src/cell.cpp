/***************************************************************************
                          cell.cpp  -  description
                             -------------------
    begin                : Tue Mar 15 2005
    copyright            : (C) 2005 by Sinisa Denic
    email                : sincomp@ptt.yu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include <qlineedit.h>
//#include <qpainter.h>
#include "cell.h"
#include <qregexp.h>
Cell::Cell(QTable *table):QTableItem(table,Never,QString::null)
{

}

/*int Cell::alignment() const 
{
    if(text().startsWith("     "))
    {
//        formatContent();
         return AlignLeft | AlignVCenter;
    }
    else
       return Align | AlignVCenter;
        
}

void Cell::formatContent()
{
        text()=text().stripWhiteSpace();
        //QTableItem::setText(text());
} */

void Cell::paint( QPainter* p, const QColorGroup& cg, const QRect& cr,bool selected)
{
    QColorGroup g( cg );
    //nadji string sa 1 ili najvise 2 razmaka iza kojih ne sledi novi razmak
    QRegExp re("^\\s{1,2}(?=\\s)");
    
    //if(text().startsWith(" "))
    if(re.search( text() ) )
    {
        //p->setPen(blue);
        //p->drawText(cr,Qt::AlignLeft,text());
         //za ispunjen uslov koristimo plavu boju za ispis teksta
         
        g.setColor( QColorGroup::Text, blue );
    }
    
    QTableItem::paint( p, g, cr, selected );
} 

