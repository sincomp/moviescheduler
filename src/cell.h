/***************************************************************************
                          cell.h  -  description
                             -------------------
    begin                : Tue Mar 15 2005
    copyright            : (C) 2005 by Sinisa Denic
    email                : sincomp@ptt.yu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef CELL_H
#define CELL_H

#include <qtable.h>


class Cell : public QTableItem
{
public:
    Cell(QTable* table=0);
   // QString text() const;
   // int alignment() const;
    void paint(QPainter*,const QColorGroup&,const QRect&,bool);
    
private:
   // void formatContent();
  
};

#endif
