/***************************************************************************
                          elements.cpp  -  description
                             -------------------
    begin                : Thu Mar 17 2005
    copyright            : (C) 2005 by Sinisa Denic
    email                : sincomp@ptt.yu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include"elements.h"
#include<qstringlist.h>
#include<qdatetime.h>
//const char F_SEP=':';
Elements::Elements()
{
    occurrence=0;
  finishedDate = "";
  totalDays=0;
  startDate="";
  is_start_set=false;
  is_el_removed=false;
  sort_by=0;
  }
Elements::Elements( const QDomElement &e )
{
  elementName = e.attribute( "elementName", "" );
  boardID = e.attribute( "boardID", "" );
  locked = e.attribute( "locked", "" ).toInt();
  excl = e.attribute( "excl", "" ).toInt();
//  sheets_num = e.attribute( "sheets_num", "" );
  num_string = e.attribute( "num_string", "" );
  occurrence=0;
  finishedDate = "";
  totalDays=0;
  startDate="";
 // occurrence = e.attribute( "occurrence", "" ).toInt();
 // finishedDate = e.attribute( "finishedDate", "" );
 // totalDays = e.attribute( "totalDays", "" ).toInt();
 // startDate=e.attribute( "startDate", "" );
 is_start_set=e.attribute( "isStartSet","").toInt();
 is_el_removed=false;
 sort_by=0;
  appendSheetNum(num_string);    
}

QDomElement Elements::ElementsToXMLNode( QDomDocument &d,const Elements& el )
{
     QDomElement cn = d.createElement( "element" );
    cn.setAttribute( "elementName", el.elementName );
    cn.setAttribute( "boardID", el.boardID);
    cn.setAttribute( "locked", QString::number(el.locked));
    cn.setAttribute( "excl",QString::number(el.excl));
//  cn.setAttribute( "sheets_num", el.sheets_num );
  cn.setAttribute( "num_string", getSheetsNumString());
  //cn.setAttribute( "occurrence",QString::number(el.occurrence) );
 // cn.setAttribute( "finishedDate", el.finishedDate );
 // cn.setAttribute( "totalDays",QString::number(el.totalDays));
//  cn.setAttribute( "startDate", el.startDate );
    //cn.setAttribute( "isStartSet",QString::number(el.is_start_set) );
    cn.setAttribute( "isStartSet","0" );

  return cn;
}
void Elements::setSheetNum( int shn){
            QValueList<int>::iterator it;
            bool sh_exist=false;
            if(sheets_num.isEmpty())
            {
                sheets_num.append(shn);
              this->incOccur();
                is_start_set=true;
                }
            else
            {
                for(it=sheets_num.begin();it!=sheets_num.end();++it)
                {
                    if((*it)==shn)
                        sh_exist=true;
                    if(!sheets_num.isEmpty()&&sh_exist)
                    {
                        it=sheets_num.remove(it);
                        
                        this->decOccur();
                        is_el_removed=true;
                        if(this->getOccurrence()==0)
                        {
                           is_start_set=false;
                           totalDays=0;
                           }
                        
                        break;
                    }
                }
                if(!sh_exist)
                {
                    sheets_num.append(shn);
                   this->incOccur();
                    is_start_set=true;
                }
            }
}
void Elements::setBuiltShNum(int bsn)
{
      QValueList<int>::iterator it;
        //int* tekuci;
            bool sh_exist=false;
            if(sheets_num.isEmpty())
            {
             sheets_num.append(bsn);
             //is_start_set=true;
            }
            else
            {
                for(it=sheets_num.begin();it!=sheets_num.end();++it)
                {
                    if((*it)==bsn)
                        sh_exist=true;
                }                 
                if(!sh_exist)
                {
                     sheets_num.append(bsn);
                    // is_start_set=true;
                }
            }
}
bool Elements::getSheetNum(int nmb){
            QValueList< int>::iterator it;
            for(it=sheets_num.begin();it!=sheets_num.end();++it){
                if((*it)==nmb){
                    return true;
                    break;
                }
            }
            return false;
}

Elements& Elements::operator=(const Elements& el)
{
    elementName=el.getElementName();
    boardID=el.getBoardID();
    locked=el.getLocked();
    excl=el.getExcludeFromStripBoard();
    occurrence=el.getOccurrence();
    startDate=el.getStartDate();
    finishedDate=el.getFinishedDate();
    totalDays=el.getTotalDays();
    sheets_num=el.sheets_num;
    num_string=el.num_string;
    start_day=el.start_day;
    //nije dovrseno ali radi i bez ovoga
    //this is not finished but it is working
    return *this;
}
bool Elements::operator<(const Elements& el2)
{
    bool rez=0;
    switch(sort_by)
    {
        case 0:
            rez=elementName < el2.getElementName();
            break;
        case 1:
            rez=occurrence < el2.getOccurrence();
            break;
        case 2:
            rez=start_day < el2.start_day;
            break;
        case 3:
            rez=totalDays < el2.totalDays;
            break;
    }
    return rez;
}
bool Elements::operator==(const Elements& el)
{
    bool rez=false;
    switch(sort_by)
    {
        case 0:
             rez==elementName,el.getElementName();
            break;
        case 1:
             rez=occurrence==el.getOccurrence();
            break;
        case 2:
            rez=start_day == el.start_day;
            break;
        case 3:
            rez=totalDays == el.totalDays;
            break;
    }
    return rez;
}
QString& Elements::getSheetsNumString()
{
    QString* num_string=new QString;
    num_string->remove(0,num_string->length());
    QString s="";
    if(!sheets_num.isEmpty())
    {
        QValueList<int>::iterator it_ns;
        for(it_ns=sheets_num.begin();it_ns!=sheets_num.end();++it_ns)
             { 
               s.setNum(*it_ns);
               (*num_string)+=s+=',';
             }
        
        return (*num_string);
    }
    else
    {
         (*num_string)="---";
         
         return (*num_string);
    }

}
QString Elements::getBoardID()const
{
            const QString nul="";
            if(!boardID.isEmpty()) return boardID;
            else return nul;     
}
QString Elements::getElementName()const
{
            const QString nul=QString::fromUtf8("…",3);
            if(!elementName.isEmpty()) return elementName;
        else return nul;
}
void Elements::appendSheetNum(QString& snm)
{
    int i=0,num_of;
    QStringList nums=QStringList::split(",",snm); 
    num_of=nums.count();
    while(i<num_of)
    {
        sheets_num.append(nums[i].toInt());
        ++i;
    }
}
