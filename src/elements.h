/***************************************************************************
                          elements.h  -  description
                             -------------------
    begin                : Wed Feb 16 2005
    copyright            : (C) 2005 by Sinisa Denic
    email                : sincomp@ptt.yu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 #ifndef ELEMENTI_H
#define ELEMENTI_H

#include<qdom.h>
#include<qvaluelist.h>
#include<qdatetime.h>
 class Elements
  {
    
   //enum SortBy{name,occur};  
   public:
	// set metode
      Elements();
      virtual~Elements(){};
      Elements( const QDomElement &e );
      
    //  QDomElement createXMLNode( QDomDocument &d );
      virtual QDomElement ElementsToXMLNode( QDomDocument &d,const Elements& el );
	    inline void setBoardID(const QString&  iboardID) { boardID = iboardID; }
	    inline void setLocked(bool lck) { locked = lck; }
        inline void setExcludeFromStripBoard(bool ex) { excl = ex ; }
	    inline void setElementName(const QString& elName) { elementName=elName ; }//  inline void setElementName(const char* elName) { elementName=elName ; }
        inline void setOccurrence(unsigned int occur) { occurrence = occur; }
	    inline void setStartDate(const QString&  date) { startDate = date; }
	    inline void setFinishedDate(const QString&  fdate) { finishedDate = fdate; }
	    inline void setTotalDays(int total) { totalDays = total; }
	    inline void setCoTravel(const QString&  ctr) { coTravel = ctr; }
	    inline void setWork(unsigned int wrk) { work = wrk; }
	    inline void setHold(unsigned int hld) { hold = hld; }
	    inline void setHoliday(unsigned int hol) { holiday = hol; }
        void setSheetNum( int shn);
        void setBuiltShNum(int bsn);
        void appendSheetNum(QString&);
        void setStartIsSet(bool s){is_start_set=s;}
        void setElemIsRemoved(bool rem){is_el_removed=rem;}
        void setStartDay(const QDate& sd){start_day=sd;}
        void setSortBy(int sb){sort_by=sb;}
         // get metode
         Elements& operator=(const Elements&);
        bool operator<(const Elements&);
         bool operator==(const Elements& );
	    QString getBoardID()const;
	    bool getLocked()const { return locked; }
        inline bool getExcludeFromStripBoard()const { return excl ; }
        QString getElementName()const;
        unsigned int getOccurrence()const { return occurrence ; }
        void incOccur() { ++occurrence; }
        void decOccur() {  if(occurrence >= 1)
                                    --occurrence; }
	    QString getStartDate()const  { return startDate ; }
	    QString getFinishedDate()const  { return finishedDate ; }
	    unsigned int getTotalDays()const { return totalDays ; }
	    QString  getCoTravel()const  { return coTravel ; }
	    unsigned int getWork() { return work ; }
	    unsigned int getHold() { return hold ; }
	    unsigned int getHoliday() { return holiday ; }
        bool getSheetNum(int nmb);
        QString& getSheetsNumString();
        bool isStartSet(){return is_start_set;}
        bool isElemRemoved(){return is_el_removed;}
        QDate getStartDay() const{return start_day;}
        int  getSortBy() const{return sort_by;}
     protected:
        QString  boardID;
	    bool locked;
        bool excl;
	    QString elementName;
        unsigned int occurrence;
	    QString startDate;
	    QString finishedDate;
	    int totalDays;
	    QString coTravel;
	    unsigned int work;
	    unsigned int hold;
	    unsigned int holiday;
        QString num_string;
        QValueList<int> sheets_num;
        bool is_start_set;
        bool is_el_removed;
        QDate start_day;
        int sort_by;
        //QTextStream& operator<<(const Elements& el);
        //QTextStream s;
       
};
//QTextStream &operator<<( QTextStream&, const Elements&);
#endif
