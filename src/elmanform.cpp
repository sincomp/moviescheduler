/****************************************************************************
** Form implementation generated from reading ui file 'elmanform.ui'
**
** Created: Sun Jan 16 13:44:51 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.2.1   edited May 19 14:22 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "elmanform.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qheader.h>
#include <qlistview.h>
#include <qtable.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qaction.h>
#include <qmenubar.h>
#include <qpopupmenu.h>
#include <qtoolbar.h>
#include <qtoolbutton.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qcombobox.h>
#include <qlabel.h>
#include"./images/document1.xpm"

#include "./elmanform.ui.h"


/*
 *  Constructs a ElManForm as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 */
//ElManForm::ElManForm(StuntsPtrList *cpl, QWidget* parent, const char* name, WFlags fl )
//    : QMainWindow( parent, name, fl )
ElManForm::ElManForm(QWidget* parent,SheetsPtrList* shl,ApplicationWindow* appw, const char* name, WFlags fl )
    : SchWin( parent, name, fl )
{
    sheets_list=shl;
    app_window=appw;
    br_redova=0;
    (void)statusBar();
    if ( !name )
	    setName( "ElManForm" );
     QPixmap newIcon;
    
      
    tulbar_operacije = new QToolBar( this, "el_operacije" );
    addToolBar( tulbar_operacije, tr( "Element operations" ), DockLeft, TRUE );
      newIcon = QPixmap( document1_xpm );
    QToolButton * newElement = new QToolButton( newIcon, "New Element", QString::null,this, SLOT(openForms()), tulbar_operacije, "new_element" );
         
    QPixmap editIcon=QPixmap("./images/editicon1.png");
    QToolButton* editButtom
  = new QToolButton(editIcon,"Edit Current Element","",this,SLOT(editCurrElem()), tulbar_operacije, "edit element");

    QPixmap copyIcon=QPixmap("./images/copy.xpm");
    QToolButton* copyButtom
  = new QToolButton(copyIcon,"Copy Current Element","",this,SLOT(copyCurrElem()), tulbar_operacije, "copy element");

    QPixmap delIcon=QPixmap("./images/delete1.png");
    QToolButton* delButtom
  = new QToolButton(delIcon,"Delete Current Element","",this,SLOT(delCurrElem()), tulbar_operacije, "delete element");

     tulbar_operacije->addSeparator();                 
/*    saveIcon = QPixmap( filesave );
    QToolButton * fileSave                                        
	= new QToolButton( saveIcon, "Save File", QString::null,
			   this, SLOT(save()), fileTools, "save file" );   */


        
    setEnabled( TRUE );
   //  setMinimumSize( QSize( 302, 153 ) );
    setCentralWidget( new QWidget( this, "qt_central_widget" ) );
    setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)5, 0, 0, sizePolicy().hasHeightForWidth() ) );
    ElManFormLayout = new QGridLayout( centralWidget(), 1, 1, 11, 6 , "ElManFormLayout"); 

  /*  Layout10 = new QHBoxLayout( 0, 0, 6, "Layout10"); 
    QSpacerItem* spacer = new QSpacerItem( 670, 0, QSizePolicy::Expanding, QSizePolicy::Minimum );
   Layout10->addItem( spacer );

   okPushButton_2 = new QPushButton( centralWidget(), "okPushButton_2" );
    okPushButton_2->setAutoDefault( TRUE );
    okPushButton_2->setDefault( TRUE );
    Layout10->addWidget( okPushButton_2 );

    cancelPushButton_2 = new QPushButton( centralWidget(), "cancelPushButton_2" );
    cancelPushButton_2->setAutoDefault( TRUE );
    Layout10->addWidget( cancelPushButton_2 );
    textLabel1 = new QLabel( centralWidget(), "textLabel1" );
    Layout10->addWidget( textLabel1 );
    sortComboBox = new QComboBox( FALSE, centralWidget(), "sortComboBox" );
    sortComboBox->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, sortComboBox->sizePolicy().hasHeightForWidth() ) );
    Layout10->addWidget( sortComboBox );
    ElManFormLayout->addLayout( Layout10, 1, 0 ); */

    Layout12 = new QHBoxLayout( 0, 0, 3, "Layout12"); 

    listCategories = new QListView( centralWidget(), "listCategories" );
    listCategories->addColumn( QString::null );
    listCategories->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)7, 0, 0, listCategories->sizePolicy().hasHeightForWidth() ) );
    Layout12->addWidget( listCategories );

    elementsTable = new MojaTabela( centralWidget(),this,"elementsTable" );
    elementsTable->setNumCols( elementsTable->numCols() + 1 );
    elementsTable->horizontalHeader()->setLabel( elementsTable->numCols() - 1, tr( "BoardID" ) );
    elementsTable->setNumCols( elementsTable->numCols() + 1 );
    elementsTable->horizontalHeader()->setLabel( elementsTable->numCols() - 1, tr( "Locked" ) );
    elementsTable->setNumCols( elementsTable->numCols() + 1 );
    elementsTable->horizontalHeader()->setLabel( elementsTable->numCols() - 1, tr( "Element Name" ) );
    elementsTable->setNumCols( elementsTable->numCols() + 1 );
    elementsTable->horizontalHeader()->setLabel( elementsTable->numCols() - 1, tr( "Occurrence" ) );
    elementsTable->setNumCols( elementsTable->numCols() + 1 );
    elementsTable->horizontalHeader()->setLabel( elementsTable->numCols() - 1, tr( "Strat Date" ) );
    elementsTable->setNumCols( elementsTable->numCols() + 1 );
    elementsTable->horizontalHeader()->setLabel( elementsTable->numCols() - 1, tr( "Finish Date" ) );
    elementsTable->setNumCols( elementsTable->numCols() + 1 );
    elementsTable->horizontalHeader()->setLabel( elementsTable->numCols() - 1, tr( "Total Days" ) );
    elementsTable->setNumCols( elementsTable->numCols() + 1 );
    elementsTable->horizontalHeader()->setLabel( elementsTable->numCols() - 1, tr( "Co.Travel" ) );
    elementsTable->setNumCols( elementsTable->numCols() + 1 );
    elementsTable->horizontalHeader()->setLabel( elementsTable->numCols() - 1, tr( "Work" ) );
    elementsTable->setNumCols( elementsTable->numCols() + 1 );
    elementsTable->horizontalHeader()->setLabel( elementsTable->numCols() - 1, tr( "Hold" ) );
    elementsTable->setNumCols( elementsTable->numCols() + 1 );
    elementsTable->horizontalHeader()->setLabel( elementsTable->numCols() - 1, tr( "Holiday" ) );
    
	  //elementsTable->setNumRows( br_redova );
  
    elementsTable->setNumCols( 11 );
    elementsTable->setShowGrid( TRUE );
    elementsTable->setReadOnly( TRUE );
    elementsTable->setSorting( TRUE );
    
    elementsTable->setColumnWidth(0,60);
    elementsTable->setColumnWidth(1,70);
    elementsTable->setColumnWidth(2,120);
    elementsTable->setColumnWidth(3,80);
    elementsTable->setColumnWidth(4,75);
    elementsTable->setColumnWidth(5,75);
    elementsTable->setColumnWidth(6,80);
    elementsTable->setColumnWidth(7,70);
    elementsTable->setColumnWidth(8,50);
    elementsTable->setColumnWidth(9,50);
    elementsTable->setColumnWidth(10,60);

    
          
////////////////////
//for ( int j = 0; j < br_redova; ++j )
// elementsTable->setItem( j, 1, new QCheckTableItem( elementsTable, "Check me" ) );

//   elementsTable->setItem(0,1,new QCheckTableItem( elementsTable, "Locked" ));
//////////////////////
    Layout12->addWidget( elementsTable );

    ElManFormLayout->addLayout( Layout12, 1, 0 );

    layout11 = new QHBoxLayout( 0, 0, 6, "layout11");
    QSpacerItem* spacer = new QSpacerItem( 285, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout11->addItem( spacer );

    textLabel1 = new QLabel( centralWidget(), "textLabel1" );
    layout11->addWidget( textLabel1 );
//da dodam i tulbar u layout11
    //layout11->addWidget(newElement);

    sortComboBox = new QComboBox( FALSE, centralWidget(), "sortComboBox" );
    sortComboBox->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, sortComboBox->sizePolicy().hasHeightForWidth() ) );
    layout11->addWidget( sortComboBox );
    QSpacerItem* spacer_2 = new QSpacerItem( 475, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout11->addItem( spacer_2 );

    ElManFormLayout->addLayout( layout11, 0, 0 );
    // toolbars
    languageChange();
    initTable();
    resize( QSize(1027, 768).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
    
    // signals and slots connections

    connect( listCategories, SIGNAL( rightButtonPressed(QListViewItem*,const QPoint&,int) ), this, SLOT( newElemPopup() ) );
    connect( elementsTable, SIGNAL( doubleClicked(int,int,int,const QPoint&) ), this, SLOT( editCurrElem() ) );
    connect( listCategories, SIGNAL( selectionChanged() ), this, SLOT( initTable() ) );
    connect( sortComboBox, SIGNAL( activated(int)),this,SLOT(sortList(int)) );

    // tab order
    setTabOrder( elementsTable, listCategories );
//    setTabOrder( listCategories, okPushButton_2 );
  //  setTabOrder( okPushButton_2, cancelPushButton_2 );
}

/*
 *  Destroys the object and frees any allocated resources
 */

ElManForm::~ElManForm()
{
    
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void ElManForm::languageChange()
{
    setCaption( tr( "Element Manager" ) );
//    okPushButton_2->setText( tr( "OK" ) );
//    cancelPushButton_2->setText( tr( "Cancel" ) );
    listCategories->header()->setLabel( 0, QString::null );
    listCategories->clear();
	listCategories->setSorting(-1,TRUE);
//	QListViewItem * item;
    item_Categories = new QListViewItem( listCategories, 0 );
    item_Categories->setText( 0, tr( "Categories" ) );
    item_Categories->setPixmap( 0, QPixmap( "./images/open.png" ) );
    item_Categories->setOpen( TRUE );

    item_CastMembers = new QListViewItem( item_Categories, 0 );
    item_CastMembers->setText( 0, tr( "Cast Members" ) );
    item_CastMembers->setPixmap( 0, QPixmap( "./images/new.png" ) );

    item_Categories->setOpen( TRUE );

    item_BackActors = new QListViewItem( item_Categories, item_CastMembers );
    item_BackActors->setText( 0, tr( "Background Actors" ) );
    item_BackActors->setPixmap( 0, QPixmap( "./images/new.png" ) );

    item_Categories->setOpen( TRUE );

    item_Stunts = new QListViewItem( item_Categories, item_BackActors );
    item_Stunts->setText( 0, tr( "Stunts" ) );
    item_Stunts->setPixmap( 0, QPixmap( "./images/new.png" ) );
    //Item Vehicles
    item_Vehicles = new QListViewItem( item_Categories, item_Stunts );
    item_Vehicles->setText( 0, tr( "Vehicles" ) );
    item_Vehicles->setPixmap( 0, QPixmap( "./images/new.png" ) );
    //item Props
    item_Props = new QListViewItem( item_Categories, item_Vehicles );
    item_Props->setText( 0, tr( "Props" ) );
    item_Props->setPixmap( 0, QPixmap( "./images/new.png" ) );
    //item Camera
    item_Camera = new QListViewItem( item_Categories, item_Props );
    item_Camera->setText( 0, tr( "Camera" ) );
    item_Camera->setPixmap( 0, QPixmap( "./images/new.png" ) );

    //item_Spec Effects

    item_Spec_Eff = new QListViewItem( item_Categories, item_Camera );
    item_Spec_Eff->setText( 0, tr( "Special Effects" ) );
    item_Spec_Eff->setPixmap( 0, QPixmap( "./images/new.png" ) );

    //item Wardobe

     item_Wardrobe = new QListViewItem( item_Categories, item_Spec_Eff );
    item_Wardrobe->setText( 0, tr( "Wardrobe" ) );
    item_Wardrobe->setPixmap( 0, QPixmap( "./images/new.png" ) );

    //item Makeup/Hair

    item_Makeup_Hair = new QListViewItem( item_Categories, item_Wardrobe );
    item_Makeup_Hair->setText( 0, tr( "Makeup/Hair" ) );
    item_Makeup_Hair->setPixmap( 0, QPixmap( "./images/new.png" ) );

    //item Animals

    item_Animals = new QListViewItem( item_Categories, item_Makeup_Hair );
    item_Animals->setText( 0, tr( "Animals" ) );
    item_Animals->setPixmap( 0, QPixmap( "./images/new.png" ) );

    //item Animal Wrangler

    item_Animal_Wrangler = new QListViewItem( item_Categories, item_Animals );
    item_Animal_Wrangler->setText( 0, tr( "Animal Wrangler" ) );
    item_Animal_Wrangler->setPixmap( 0, QPixmap( "./images/new.png" ) );

    // item Music
    item_Music = new QListViewItem( item_Categories, item_Animal_Wrangler );
    item_Music->setText( 0, tr( "Music" ) );
    item_Music->setPixmap( 0, QPixmap( "./images/new.png" ) );

    //item Sounds
    item_Sounds = new QListViewItem( item_Categories, item_Music );
    item_Sounds->setText( 0, tr( "Sounds" ) );
    item_Sounds->setPixmap( 0, QPixmap( "./images/new.png" ) );

    //item Art Department
    item_Art_Dep = new QListViewItem( item_Categories, item_Sounds );
    item_Art_Dep->setText( 0, tr( "Art Department" ) );
    item_Art_Dep->setPixmap( 0, QPixmap( "./images/new.png" ) );

    //item Set Dressing
    item_Set_Dress = new QListViewItem( item_Categories, item_Art_Dep );
    item_Set_Dress->setText( 0, tr( "Set Dressing" ) );
    item_Set_Dress->setPixmap( 0, QPixmap( "./images/new.png" ) );

    //item Geenery
    item_Greenery = new QListViewItem( item_Categories, item_Set_Dress );
    item_Greenery->setText( 0, tr( "Geenery" ) );
    item_Greenery->setPixmap( 0, QPixmap( "./images/new.png" ) );

    //item Spec Equipment
    item_Spec_Equip = new QListViewItem( item_Categories, item_Greenery );
    item_Spec_Equip->setText( 0, tr( "Special Equipment" ) );
    item_Spec_Equip->setPixmap( 0, QPixmap( "./images/new.png" ) );

    //item Security
    item_Security = new QListViewItem( item_Categories, item_Spec_Equip );
    item_Security->setText( 0, tr( "Security" ) );
    item_Security->setPixmap( 0, QPixmap( "./images/new.png" ) );

    //item Additinal Labor
    item_Add_Labor = new QListViewItem( item_Categories, item_Security );
    item_Add_Labor->setText( 0, tr( "Additinal Labor" ) );
    item_Add_Labor->setPixmap( 0, QPixmap( "./images/new.png" ) );

    //item Visual Effects
    item_Vis_Eff = new QListViewItem( item_Categories, item_Add_Labor );
    item_Vis_Eff->setText( 0, tr( "Visual Effects" ) );
    item_Vis_Eff->setPixmap( 0, QPixmap( "./images/new.png" ) );

     //item Machanical Effects
    item_Mech_Eff = new QListViewItem( item_Categories, item_Vis_Eff );
    item_Mech_Eff->setText( 0, tr( "Machanical Effects" ) );
    item_Mech_Eff->setPixmap( 0, QPixmap( "./images/new.png" ) );

     //item Miscellaneous
    item_Misc = new QListViewItem( item_Categories, item_Mech_Eff );
    item_Misc->setText( 0, tr( "Miscellaneous" ) );
    item_Misc->setPixmap( 0, QPixmap( "./images/new.png" ) );

     //item Notes
    item_Notes = new QListViewItem( item_Categories, item_Misc );
    item_Notes->setText( 0, tr( "Notes" ) );
    item_Notes->setPixmap( 0, QPixmap( "./images/new.png" ) );

    //item Built-In Categories
    item_Built_Categories = new QListViewItem( listCategories, item_Categories );
    item_Built_Categories->setText( 0, tr( "Built-In Categories" ) );
    item_Built_Categories->setPixmap( 0, QPixmap( "./images/open.png" ) );
    item_Built_Categories->setOpen( TRUE );

    //item Script Day
    item_Script_Day = new QListViewItem( item_Built_Categories, 0 );
    item_Script_Day->setText( 0, tr( "Script Day" ) );
    item_Script_Day->setPixmap( 0, QPixmap( "./images/new.png" ) );

    //item Sequince
    item_Sequence = new QListViewItem( item_Built_Categories, item_Script_Day );
    item_Sequence->setText( 0, tr( "Sequence" ) );
    item_Sequence->setPixmap( 0, QPixmap( "./images/new.png" ) );

     //item Unit
    item_Unit = new QListViewItem( item_Built_Categories, item_Sequence );
    item_Unit->setText( 0, tr( "Unit" ) );
    item_Unit->setPixmap( 0, QPixmap( "./images/new.png" ) );

     //item Location
    item_Location = new QListViewItem( item_Built_Categories, item_Unit );
    item_Location->setText( 0, tr( "Location" ) );
    item_Location->setPixmap( 0, QPixmap( "./images/new.png" ) );

     //item Set
    item_Set = new QListViewItem( item_Built_Categories, item_Location );
    item_Set->setText( 0, tr( "Set" ) );
    item_Set->setPixmap( 0, QPixmap( "./images/new.png" ) );
    listCategories->setCurrentItem(item_CastMembers);

    elementsTable->horizontalHeader()->setLabel( 0, tr( "BoardID" ) );
    elementsTable->horizontalHeader()->setLabel( 1, tr( "Locked" ) );
    elementsTable->horizontalHeader()->setLabel( 2, tr( "Element Name" ) );
    elementsTable->horizontalHeader()->setLabel( 3, tr( "Occurrence" ) );
    elementsTable->horizontalHeader()->setLabel( 4, tr( "Strat Date" ) );
    elementsTable->horizontalHeader()->setLabel( 5, tr( "Finish Date" ) );
    elementsTable->horizontalHeader()->setLabel( 6, tr( "Total Days" ) );
    elementsTable->horizontalHeader()->setLabel( 7, tr( "Co.Travel" ) );
    elementsTable->horizontalHeader()->setLabel( 8, tr( "Work" ) );
    elementsTable->horizontalHeader()->setLabel( 9, tr( "Hold" ) );
    elementsTable->horizontalHeader()->setLabel( 10, tr( "Holiday" ) );
    textLabel1->setText( tr( "sort by:" ) );
    sortComboBox->clear();
    sortComboBox->insertItem( tr( "name" ) );
    sortComboBox->insertItem( tr( "occurence" ) );
    sortComboBox->insertItem( tr( "start date" ) );
    sortComboBox->insertItem( tr( "total days" ) );
    //init();
}

