/****************************************************************************
** Form interface generated from reading ui file 'elmanform.ui'
**
** Created: Sun Jan 16 13:44:34 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.2.1   edited May 19 14:22 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef ELMANFORM_H
#define ELMANFORM_H

#include <qvariant.h>
#include <qtable.h>
#include"sheet.h"
#include"schwin.h"
class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QAction;
class QActionGroup;
class QToolBar;
class QPopupMenu;
class QPushButton;
class QListView;
class QListViewItem;
class QTable;
class UnosElemForm;
class ApplicationWindow;
class QComboBox;
class QLabel;
class ElManForm : public SchWin
{
    Q_OBJECT

public:
     ElManForm( QWidget* parent = 0,SheetsPtrList* shl=0,ApplicationWindow* appw=0, const char* name = 0, WFlags fl = WType_TopLevel );
    ~ElManForm();

    
    QListView* listCategories;
    QTable* elementsTable;
    QComboBox* sortComboBox;
    
public slots:
   // virtual void save();
   // virtual bool saveAs();
     void newElemPopup();
//     void openForms(int);
     void openForms();
  //   void updateSheet();
  //   void updateBuiltSheet(BuiltInCategories&);//QPtrList<Elements>&);
     void editCurrElem();
     void copyCurrElem();
     void delCurrElem();
     void initTable();
     ApplicationWindow* getPointtoAppWin()const{return app_window;}
     //void delElemUpdateSheet();
     void sortList(int);
protected:
    QString filename;
    QGridLayout* ElManFormLayout;
    QHBoxLayout* Layout10;
    QHBoxLayout* Layout12;
protected slots:
    virtual void languageChange();
    
private:
    SheetsPtrList* sheets_list;
    Sheet* sheet;
    ApplicationWindow* app_window;
    QDialog *pUnosForm;
//    UnosElemForm *pUnosElemForm;
    QToolBar* tulbar_operacije;
    QListViewItem* item_BackActors;
    QListViewItem* item_Categories;
    QListViewItem* item_CastMembers;
    QListViewItem* item_Stunts;
    QListViewItem* item_Vehicles;
    QListViewItem* item_Props;
    QListViewItem* item_Camera;
    QListViewItem* item_Spec_Eff;
    QListViewItem* item_Wardrobe;
    QListViewItem* item_Makeup_Hair;
    QListViewItem* item_Animals;
    QListViewItem* item_Animal_Wrangler;
    QListViewItem* item_Music;
    QListViewItem* item_Sounds;
    QListViewItem* item_Art_Dep;
    QListViewItem* item_Set_Dress;
    QListViewItem* item_Greenery;
    QListViewItem* item_Spec_Equip;
    QListViewItem* item_Security;
    QListViewItem* item_Add_Labor;
    QListViewItem* item_Vis_Eff;
    QListViewItem* item_Mech_Eff;
    QListViewItem* item_Misc;
    QListViewItem* item_Notes;
    QListViewItem* item_Built_Categories;
    QListViewItem* item_Script_Day;
    QListViewItem* item_Sequence;
    QListViewItem* item_Unit;
    QListViewItem* item_Location;
    QListViewItem* item_Set;
    QListViewItem* pom;
    
    QLabel* textLabel1;
    QHBoxLayout* layout11;
signals:
    void message(const QString&, int );    
	

};
class MojaTabela:public QTable
{
    Q_OBJECT
    public:
    MojaTabela ( QWidget* parent=0,ElManForm* p_elf=0,const char* name=0):QTable(parent,name){p_elman_form=p_elf;};
    protected slots:
    void columnClicked(int);
    private:
    ElManForm* p_elman_form;
};


#endif // ELMANFORM_H
