/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/
#include<qpopupmenu.h>
#include <qfile.h>
#include <qfiledialog.h>
#include <qdatastream.h>
#include"unoselemform.h"
#include"payelform.h"
#include"stuntsformimpl.h"
#include"builtincatform.h"
#include"application.h"
//#include<qcombobox.h>
//#include"mojaptrlista.h"
//class Stunts;
//class BuiltIn;
CastPtrList SchWin::el_cast_ptr_list;
StuntsPtrList SchWin::el_stunts_ptr_list;
PayElPtrList SchWin::el_bact_ptr_list;
PayElPtrList SchWin::el_props_ptr_list,SchWin::el_speceff_ptr_list,SchWin::el_setdress_ptr_list,
    SchWin::el_sound_ptr_list,SchWin::el_mechanical_ptr_list,SchWin::el_mkupheir_ptr_list,
    SchWin::el_animals_ptr_list,SchWin::el_anwrangler_ptr_list,
    SchWin::el_music_ptr_list,SchWin::el_green_ptr_list,SchWin::el_specequ_ptr_list,
    SchWin::el_secur_ptr_list,SchWin::el_addlab_ptr_list,SchWin::el_vieff_ptr_list;
BuiltInCategories SchWin::location_ptr_list,SchWin::script_day_ptr_list,
SchWin::sequence_ptr_list,SchWin::unit_ptr_list,SchWin::set_ptr_list;
//Sheet SchWin::sheet;
unsigned int br_redova;


void ElManForm::newElemPopup()
{
	if((listCategories->currentItem()==item_CastMembers)
	||(listCategories->currentItem()==item_BackActors)
    ||(listCategories->currentItem()==item_Stunts)
    ||(listCategories->currentItem()==item_Script_Day)
    ||(listCategories->currentItem()==item_Sequence)
    ||(listCategories->currentItem()==item_Unit)
    ||(listCategories->currentItem()==item_Location)
    ||(listCategories->currentItem()==item_Set) 
    ||(listCategories->currentItem()==item_Add_Labor)
    ||(listCategories->currentItem()==item_Animal_Wrangler)
    ||(listCategories->currentItem()==item_Animals )
    ||(listCategories->currentItem()==item_Sounds )
    ||(listCategories->currentItem()==item_Set_Dress)
    ||(listCategories->currentItem()==item_Mech_Eff)
    ||(listCategories->currentItem()==item_Makeup_Hair)
    ||(listCategories->currentItem()==item_Music)
    ||(listCategories->currentItem()==item_Greenery )
    ||(listCategories->currentItem()==item_Spec_Equip  )
    ||(listCategories->currentItem()==item_Security)
    ||(listCategories->currentItem()==item_Spec_Eff )
    ||(listCategories->currentItem()==item_Vis_Eff )
    ||(listCategories->currentItem()==item_Props))
	{
    	QPopupMenu* newElem = new QPopupMenu(this);
		Q_CHECK_PTR( newElem );
		newElem->insertItem("New Element",this,SLOT(openForms() ) );
		newElem->exec( QCursor::pos() );
	}
	else
	return;
}


void ElManForm::editCurrElem()
{
        int br_el=-1;
        br_el=elementsTable->currentRow();
        if (br_el>=0)
        {
            if((listCategories->currentItem())==item_CastMembers)
		    {
  			    pUnosForm=new UnosElemForm(&el_cast_ptr_list,this,br_el);
  			    pUnosForm->show();

		    }
        QListViewItem* item_temp=listCategories->currentItem();
	    if(item_temp==item_BackActors||item_temp==item_Add_Labor||item_temp==item_Animal_Wrangler||item_temp==item_Animals||item_temp==item_Sounds||item_temp==item_Set_Dress||item_temp==item_Mech_Eff
        ||item_temp==item_Makeup_Hair||item_temp==item_Music||item_temp==item_Greenery||item_temp==item_Spec_Equip||item_temp==item_Security||item_temp==item_Spec_Eff||item_temp==item_Vis_Eff||item_temp==item_Props)
          {
            PayElPtrList* p_temp=0;
            if(item_temp==item_Add_Labor)
                p_temp=&el_addlab_ptr_list;
            if(item_temp==item_Animals)
                p_temp=&el_animals_ptr_list;
            if(item_temp==item_Animal_Wrangler)
                p_temp=&el_anwrangler_ptr_list;
            if(item_temp==item_BackActors)
                p_temp=&el_bact_ptr_list;
            if(item_temp==item_Sounds)
                p_temp=&el_sound_ptr_list;
            if(item_temp==item_Set_Dress)
                p_temp=&el_setdress_ptr_list;
            if(item_temp==item_Mech_Eff)
                p_temp=&el_mechanical_ptr_list;
            if(item_temp==item_Makeup_Hair)
                p_temp=&el_mkupheir_ptr_list;
            if(item_temp==item_Music)
                p_temp=&el_music_ptr_list;
            if(item_temp==item_Greenery)
                p_temp=&el_green_ptr_list;
            if(item_temp==item_Spec_Equip)
                p_temp=&el_specequ_ptr_list;
            if(item_temp==item_Security)
                p_temp=&el_secur_ptr_list;
            if(item_temp==item_Spec_Eff)
                p_temp=&el_speceff_ptr_list;
            if(item_temp==item_Vis_Eff)
                p_temp=&el_vieff_ptr_list;
            if(item_temp==item_Props)
                p_temp=& el_props_ptr_list;


            pUnosForm=new PayElForm(p_temp,this,br_el);
            pUnosForm->show();
		}
    if((listCategories->currentItem())==item_Stunts)
		{
			pUnosForm=new StuntsFormImpl (&el_stunts_ptr_list,this,br_el);
    		pUnosForm->show();
       }
   if((listCategories->currentItem())==item_Script_Day)
        {
            pUnosForm=new BuiltInCatForm (&script_day_ptr_list,this,br_el);
            pUnosForm->show();
        }
   if(listCategories->currentItem()==item_Sequence)
        {
            pUnosForm=new BuiltInCatForm (&sequence_ptr_list,this,br_el);
            pUnosForm->show();
        }
   if(listCategories->currentItem()==item_Unit)
        {
            pUnosForm=new BuiltInCatForm (&unit_ptr_list,this,br_el);
            pUnosForm->show();
        }
   if(listCategories->currentItem()==item_Location)
        {
            pUnosForm=new BuiltInCatForm (&location_ptr_list,this,br_el);
            pUnosForm->show();
        }
   if(listCategories->currentItem()==item_Set)
        {
            pUnosForm=new BuiltInCatForm (&set_ptr_list,this,br_el);
            pUnosForm->show();
        }                
	        
   }
	else
	return;
}
void ElManForm::openForms()
{

	if((listCategories->currentItem())==item_CastMembers)
		{
  			pUnosForm=new UnosElemForm(&el_cast_ptr_list,this);
  			pUnosForm->show();

		}
        QListViewItem* item_temp=listCategories->currentItem();
	if(item_temp==item_BackActors||item_temp==item_Add_Labor||item_temp==item_Animal_Wrangler||item_temp==item_Animals||item_temp==item_Sounds||item_temp==item_Set_Dress||item_temp==item_Mech_Eff
    ||item_temp==item_Makeup_Hair||item_temp==item_Music||item_temp==item_Greenery||item_temp==item_Spec_Equip||item_temp==item_Security||item_temp==item_Spec_Eff||item_temp==item_Vis_Eff||item_temp==item_Props)
          {
            PayElPtrList* p_temp=0;
            if(item_temp==item_Add_Labor)
                p_temp=&el_addlab_ptr_list;
            if(item_temp==item_Animals)
                p_temp=&el_animals_ptr_list;
            if(item_temp==item_Animal_Wrangler)
                p_temp=&el_anwrangler_ptr_list;
            if(item_temp==item_BackActors)
                p_temp=&el_bact_ptr_list;
            if(item_temp==item_Sounds)
                p_temp=&el_sound_ptr_list;
            if(item_temp==item_Set_Dress)
                p_temp=&el_setdress_ptr_list;
            if(item_temp==item_Mech_Eff)
                p_temp=&el_mechanical_ptr_list;
            if(item_temp==item_Makeup_Hair)
                p_temp=&el_mkupheir_ptr_list;
            if(item_temp==item_Music)
                p_temp=&el_music_ptr_list;
            if(item_temp==item_Greenery)
                p_temp=&el_green_ptr_list;
            if(item_temp==item_Spec_Equip)
                p_temp=&el_specequ_ptr_list;
            if(item_temp==item_Security)
                p_temp=&el_secur_ptr_list;
            if(item_temp==item_Spec_Eff)
                p_temp=&el_speceff_ptr_list;
            if(item_temp==item_Vis_Eff)
                p_temp=&el_vieff_ptr_list;
            if(item_temp==item_Props)
                p_temp=& el_props_ptr_list;
                

            pUnosForm=new PayElForm(p_temp,this);
            pUnosForm->show();
		}
    if((listCategories->currentItem())==item_Stunts)
		{
			pUnosForm=new StuntsFormImpl (&el_stunts_ptr_list,this);
    		pUnosForm->show();
       }
   if((listCategories->currentItem())==item_Script_Day)
        {
            pUnosForm=new BuiltInCatForm (&script_day_ptr_list,this);
            pUnosForm->show();
        }
   if(listCategories->currentItem()==item_Sequence)
        {
            pUnosForm=new BuiltInCatForm (&sequence_ptr_list,this);
            pUnosForm->show();
        }
   if(listCategories->currentItem()==item_Unit)
        {
            pUnosForm=new BuiltInCatForm (&unit_ptr_list,this);
            pUnosForm->show();
        }
   if(listCategories->currentItem()==item_Location)
        {
            pUnosForm=new BuiltInCatForm (&location_ptr_list,this);
            pUnosForm->show();
        }
   if(listCategories->currentItem()==item_Set)
        {
            pUnosForm=new BuiltInCatForm (&set_ptr_list,this);
            pUnosForm->show();
        }                
		//else
		//return;
}
void ElManForm::initTable()
{
  if((listCategories->currentItem())==item_CastMembers)
    {
        elementsTable->setNumRows(el_cast_ptr_list.count());
        for(int br_red=0;br_red<elementsTable->numRows();br_red++)
          {
            QCheckTableItem *ch_item= new QCheckTableItem(elementsTable,"Locked");
            elementsTable->setItem( br_red, 1,ch_item  );
          }
        
        for(unsigned int i=0;i<el_cast_ptr_list.count();i++)
          {
            //elementsTable->setNumRows(i);
            QString sid;
	          sid=el_cast_ptr_list.at(i)->getBoardID();
	          elementsTable->setText(i,0,sid);
	          elementsTable->setText(i,2,el_cast_ptr_list.at(i)->getElementName());
              /////////////////////////////ovde je ubacen deo za start date
              elementsTable->setText(i,3,QString::number(el_cast_ptr_list.at(i)->getOccurrence()));
              elementsTable->setText(i,4,el_cast_ptr_list.at(i)->getStartDate());
             elementsTable->setText(i,5,el_cast_ptr_list.at(i)->getFinishedDate());
                elementsTable->setText(i,6,QString::number(el_cast_ptr_list.at(i)->getTotalDays()));
              QCheckTableItem *ch_item2=(QCheckTableItem*)elementsTable->item(i,1);
	            ch_item2->setChecked(el_cast_ptr_list.at(i)->getLocked());
            //ElManForm::updateTable();
         }
      }
   QListViewItem* item_temp=listCategories->currentItem();
   PayElPtrList* p_temp=0;
            if(item_temp==item_Add_Labor)
                p_temp=&el_addlab_ptr_list;
            if(item_temp==item_Animals)
                p_temp=&el_animals_ptr_list;
            if(item_temp==item_Animal_Wrangler)
                p_temp=&el_anwrangler_ptr_list;
            if(item_temp==item_BackActors)
                p_temp=&el_bact_ptr_list;
            if(item_temp==item_Sounds)
                p_temp=&el_sound_ptr_list;
            if(item_temp==item_Set_Dress)
                p_temp=&el_setdress_ptr_list;
            if(item_temp==item_Mech_Eff)
                p_temp=&el_mechanical_ptr_list;
            if(item_temp==item_Makeup_Hair)
                p_temp=&el_mkupheir_ptr_list;
            if(item_temp==item_Music)
                p_temp=&el_music_ptr_list;
            if(item_temp==item_Greenery)
                p_temp=&el_green_ptr_list;
            if(item_temp==item_Spec_Equip)
                p_temp=&el_specequ_ptr_list;
            if(item_temp==item_Security)
                p_temp=&el_secur_ptr_list;
            if(item_temp==item_Spec_Eff)
                p_temp=&el_speceff_ptr_list;
            if(item_temp==item_Vis_Eff)
                p_temp=&el_vieff_ptr_list;
            if(item_temp==item_Props)
                p_temp=&el_props_ptr_list;
        unsigned int i=0;
        if(p_temp)
        {
            elementsTable->setNumRows(p_temp->count());
            for(int br_red=0;br_red<elementsTable->numRows();br_red++)
          {
            QCheckTableItem *ch_item= new QCheckTableItem(elementsTable,"Locked");
            elementsTable->setItem( br_red, 1,ch_item  );
          }

         //Prolaz kroz listu preko iteratora
        ///////////////////////////////////
           QPtrListIterator<PayElements> it_pel(*p_temp );

         PayElements *pel;
            it_pel.toFirst();
            while ( (pel = it_pel.current()) != 0 )
            {
                ++it_pel;
                QString str;
                str=pel->getBoardID();
  //ovde se postavljaju vrednosti za elemente u tabelu-elmanform
   /////////////////////////////ovde je ubacen deo za start date  
                elementsTable->setText(i,0,str);
                elementsTable->setText(i,2,pel->getElementName()); 
              elementsTable->setText(i,3,QString::number(p_temp->at(i)->getOccurrence()));
             elementsTable->setText(i,4,p_temp->at(i)->getStartDate());
             elementsTable->setText(i,5,p_temp->at(i)->getFinishedDate());
                elementsTable->setText(i,6,QString::number(p_temp->at(i)->getTotalDays()));
                QCheckTableItem *ch_item2=(QCheckTableItem*)elementsTable->item(i,1);
                ch_item2->setChecked(pel->getLocked());
                ++i;
             }
        }
      

 if((listCategories->currentItem())==item_Stunts)
    {
        unsigned int i=0;
        elementsTable->setNumRows(el_stunts_ptr_list.count());
        for( br_redova=0;br_redova<el_stunts_ptr_list.count();br_redova++)
          {
            QCheckTableItem *ch_item= new QCheckTableItem(elementsTable,"Locked");
            elementsTable->setItem( br_redova, 1,ch_item  );
          }

        //Prolaz kroz listu preko iteratora
        ///////////////////////////////////
           QPtrListIterator<Stunts> it_stunts( el_stunts_ptr_list );
           
           Stunts *stunt;
             it_stunts.toFirst();
            while ( (stunt = it_stunts.current()) != 0 )
            {
                ++it_stunts;
                QString str;
                str=stunt->getBoardID();
  //ovde se postavljaju vrednosti za elemente u tabelu-elmanform              
                elementsTable->setText(i,0,str);
                elementsTable->setText(i,2,stunt->getElementName());
                elementsTable->setText(i,3,QString::number(stunt->getOccurrence()));
             elementsTable->setText(i,4,stunt->getStartDate());
             elementsTable->setText(i,5,stunt->getFinishedDate());
                elementsTable->setText(i,6,QString::number(stunt->getTotalDays()));
                
                QCheckTableItem *ch_item2=(QCheckTableItem*)elementsTable->item(i,1);
                ch_item2->setChecked(stunt->getLocked());
                ++i;
             }
    }
    if((listCategories->currentItem())==item_Script_Day)
    {
        unsigned int i=0;
        elementsTable->setNumRows(script_day_ptr_list.count());
        for(int br_red=0;br_red<elementsTable->numRows();br_red++)
          {
            QCheckTableItem *ch_item= new QCheckTableItem(elementsTable,"Locked");
            elementsTable->setItem( br_red, 1,ch_item  );
          }

         //Prolaz kroz listu preko iteratora
        ///////////////////////////////////
           QPtrListIterator<Elements> it_built( script_day_ptr_list );

           Elements *builtin;
            it_built.toFirst();
            while ( (builtin = it_built.current()) != 0 )
            {
                ++it_built;
                elementsTable->setText(i,2,builtin->getElementName());
                 elementsTable->setText(i,3,QString::number(builtin->getOccurrence()));
             elementsTable->setText(i,4,builtin->getStartDate());
             elementsTable->setText(i,5,builtin->getFinishedDate());
                elementsTable->setText(i,6,QString::number(builtin->getTotalDays()));
                
                ++i;

             }
      }
      if((listCategories->currentItem())==item_Set)
    {
        unsigned int i=0;
        elementsTable->setNumRows(set_ptr_list.count());
        for(int br_red=0;br_red<elementsTable->numRows();br_red++)
          {
            QCheckTableItem *ch_item= new QCheckTableItem(elementsTable,"Locked");
            elementsTable->setItem( br_red, 1,ch_item  );
          }

         //Prolaz kroz listu preko iteratora
        ///////////////////////////////////
           QPtrListIterator<Elements> it( set_ptr_list );

           Elements *builtin;

            while ( (builtin = it.current()) != 0 )
            {
                ++it;
                elementsTable->setText(i,2,builtin->getElementName());         
                    elementsTable->setText(i,3,QString::number(builtin->getOccurrence()));
             elementsTable->setText(i,4,builtin->getStartDate());
             elementsTable->setText(i,5,builtin->getFinishedDate());
                elementsTable->setText(i,6,QString::number(builtin->getTotalDays()));
                ++i;
             }
      }
  if((listCategories->currentItem())==item_Unit)
    {
        unsigned int i=0;
        elementsTable->setNumRows(unit_ptr_list.count());
        for(int br_red=0;br_red<elementsTable->numRows();br_red++)
          {
            QCheckTableItem *ch_item= new QCheckTableItem(elementsTable,"Locked");
            elementsTable->setItem( br_red, 1,ch_item  );
          }

         //Prolaz kroz listu preko iteratora
        ///////////////////////////////////
           QPtrListIterator<Elements> it( unit_ptr_list );

           Elements *builtin;

            while ( (builtin = it.current()) != 0 )
            {
                ++it;
                elementsTable->setText(i,2,builtin->getElementName());
                    elementsTable->setText(i,3,QString::number(builtin->getOccurrence()));
             elementsTable->setText(i,4,builtin->getStartDate());
             elementsTable->setText(i,5,builtin->getFinishedDate());
                elementsTable->setText(i,6,QString::number(builtin->getTotalDays()));
                ++i;
             }
      }
  if((listCategories->currentItem())==item_Sequence)
    {
        unsigned int i=0;
        elementsTable->setNumRows(sequence_ptr_list.count());
        for(int br_red=0;br_red<elementsTable->numRows();br_red++)
          {
            QCheckTableItem *ch_item= new QCheckTableItem(elementsTable,"Locked");
            elementsTable->setItem( br_red, 1,ch_item  );
          }

         //Prolaz kroz listu preko iteratora
        ///////////////////////////////////
           QPtrListIterator<Elements> it( sequence_ptr_list );

           Elements *builtin;

            while ( (builtin = it.current()) != 0 )
            {
                ++it;
                elementsTable->setText(i,2,builtin->getElementName());
                    elementsTable->setText(i,3,QString::number(builtin->getOccurrence()));
             elementsTable->setText(i,4,builtin->getStartDate());
             elementsTable->setText(i,5,builtin->getFinishedDate());
                elementsTable->setText(i,6,QString::number(builtin->getTotalDays()));
                ++i;
             }
      }
  if((listCategories->currentItem())==item_Location)
    {
        unsigned int i=0;
        elementsTable->setNumRows(location_ptr_list.count());
        for(int br_red=0;br_red<elementsTable->numRows();br_red++)
          {
            QCheckTableItem *ch_item= new QCheckTableItem(elementsTable,"Locked");
            elementsTable->setItem( br_red, 1,ch_item  );
          }

         //Prolaz kroz listu preko iteratora
        ///////////////////////////////////
           QPtrListIterator<Elements> it( location_ptr_list );

           Elements *builtin;

            while ( (builtin = it.current()) != 0 )
            {
                ++it;
                elementsTable->setText(i,2,builtin->getElementName());
                    elementsTable->setText(i,3,QString::number(builtin->getOccurrence()));
             elementsTable->setText(i,4,builtin->getStartDate());
             elementsTable->setText(i,5,builtin->getFinishedDate());
                elementsTable->setText(i,6,QString::number(builtin->getTotalDays()));
                ++i;
             }
      }
      elementsTable->adjustColumn(2);
}

void ElManForm::copyCurrElem()
{
  qWarning("Funkcija COPY jos nije implementirana!");
}

void ElManForm::delCurrElem()
{
	int br_el=-1;
        br_el=elementsTable->currentRow();
        if (br_el>=0)
            {
				if((listCategories->currentItem())==item_CastMembers)
		        {
  			       el_cast_ptr_list.remove(br_el);
                    //sheet.cast_list.remove(br_el);
                    updateSheet();
				    initTable();
					return;
		        }
	         if((listCategories->currentItem())==item_BackActors)
		        {
			        el_bact_ptr_list.remove(br_el);
                    updatePayElmSheet(el_bact_ptr_list);
					initTable();
					return;

		        }
            if((listCategories->currentItem())==item_Stunts)
		        {
			        el_stunts_ptr_list.remove(br_el);
                    updateSheet();
                    initTable();
					return;
                }
            ////////////////////////////////////////////////////////
            if((listCategories->currentItem())==item_Add_Labor)
		        {
  			       el_addlab_ptr_list.remove(br_el);
                    //sheet.cast_list.remove(br_el);
                    updatePayElmSheet(el_addlab_ptr_list);
				    initTable();
					return;
		        }           
	         if((listCategories->currentItem())==item_Animal_Wrangler)
		        {
			        el_anwrangler_ptr_list.remove(br_el);
                    updatePayElmSheet(el_anwrangler_ptr_list);
					initTable();
					return;

		        }
            if((listCategories->currentItem())==item_Animals)
		        {
			        el_animals_ptr_list.remove(br_el);
                    updatePayElmSheet(el_animals_ptr_list);
                    initTable();
					return;
                }
            if((listCategories->currentItem())==item_Sounds)
		        {
  			       el_sound_ptr_list.remove(br_el);
                    //sheet.cast_list.remove(br_el);
                     updatePayElmSheet(el_sound_ptr_list);
				    initTable();
					return;
		        }
	         if((listCategories->currentItem())==item_Set_Dress)
		        {
			        el_setdress_ptr_list.remove(br_el);
                    updatePayElmSheet(el_setdress_ptr_list);
					initTable();
					return;

		        }
            if((listCategories->currentItem())==item_Mech_Eff)
		        {
			        el_mechanical_ptr_list.remove(br_el);
                    updatePayElmSheet(el_mechanical_ptr_list);
                    initTable();
					return;
                }
                if((listCategories->currentItem())==item_Makeup_Hair)
		        {
  			       el_mkupheir_ptr_list.remove(br_el);
                    //sheet.cast_list.remove(br_el);
                    updatePayElmSheet(el_mkupheir_ptr_list);
				    initTable();
					return;
		        }
	         if((listCategories->currentItem())==item_Music)
		        {
			        el_music_ptr_list.remove(br_el);
                    updatePayElmSheet(el_music_ptr_list);
					initTable();
					return;

		        }
            if((listCategories->currentItem())==item_Greenery)
		        {
			        el_green_ptr_list.remove(br_el);
                    updatePayElmSheet(el_green_ptr_list);
                    initTable();
					return;
                }
                if((listCategories->currentItem())==item_Spec_Equip)
		        {
  			       el_specequ_ptr_list.remove(br_el);
                    //sheet.cast_list.remove(br_el);
                    updatePayElmSheet(el_specequ_ptr_list);
				    initTable();
					return;
		        }
	         if((listCategories->currentItem())==item_Security)
		        {
			        el_secur_ptr_list.remove(br_el);
                    updatePayElmSheet(el_secur_ptr_list);
					initTable();
					return;

		        }
            if((listCategories->currentItem())==item_Spec_Eff)
		        {
			        el_speceff_ptr_list.remove(br_el);
                    updatePayElmSheet(el_speceff_ptr_list);
                    initTable();
					return;
                }
            if((listCategories->currentItem())==item_Vis_Eff)
                {
                    el_vieff_ptr_list.remove(br_el);
                     updatePayElmSheet(el_vieff_ptr_list);
                    initTable();
                    return;
                }
            if((listCategories->currentItem())==item_Props)
                {
                    el_props_ptr_list.remove(br_el);
                    updatePayElmSheet(el_props_ptr_list); 
                    initTable();
                    return;
                }
            if((listCategories->currentItem())==item_Script_Day)
                {
                    script_day_ptr_list.remove(br_el);
                    updateBuiltSheet(script_day_ptr_list);
                    initTable();
                    return;
                }
            if((listCategories->currentItem())==item_Set)
                {
                    set_ptr_list.remove(br_el);
                    updateBuiltSheet(set_ptr_list);
                    initTable();
                    return;
                }
              if((listCategories->currentItem())==item_Unit)
                {
                    unit_ptr_list.remove(br_el);
                     updateBuiltSheet(unit_ptr_list);
                    initTable();
                    return;
                }
              if((listCategories->currentItem())==item_Location)
                {
                    location_ptr_list.remove(br_el);
                     updateBuiltSheet(location_ptr_list);
                    initTable();
                    return;
                }
              if((listCategories->currentItem())==item_Sequence)
                {
                    sequence_ptr_list.remove(br_el);
                    updateBuiltSheet(sequence_ptr_list);
                    initTable();
                    return;
                }
 //osvezvanje tabele u brkdwnsh formi
                    if(getPointtoAppWin()->getPointToBrkShForm())
                        this->getPointtoAppWin()->getPointToBrkShForm()->initTable();  
           
            }
		else
		return;
 // qWarning("Funkcija DELETE jos nije implementirana!");
}
void ElManForm::sortList(int bk)
{
      //br_el=elementsTable->currentRow();
       // if (br_el>=0)
            
				if((listCategories->currentItem())==item_CastMembers)
		        {
  		            el_cast_ptr_list.sort(bk);
                   //qHeapSort(QPtrList::iterator<el_cast_ptr_list> it.begin(),QPtrList::iterator<el_cast_ptr_list> it.end());
                   initTable();
				    return;
		        }
	             if((listCategories->currentItem())==item_BackActors)
		        {
			        //PayWithSort l_bact_sort=(PayWithSort*)el_bact_ptr_list;
              
                  //el_bact_ptr_list.sort(bk);
					initTable();
					return;

		        }
            if((listCategories->currentItem())==item_Stunts)
		        {
                   el_stunts_ptr_list.sort(bk);
                    initTable();
					return;
                }
            ////////////////////////////////////////////////////////
            if((listCategories->currentItem())==item_Add_Labor)
		        {
  			      el_addlab_ptr_list.sort(bk);
                    
				    initTable();
					return;
		        }
	         if((listCategories->currentItem())==item_Animal_Wrangler)
		        {
			       el_anwrangler_ptr_list.sort(bk);
                   
					initTable();
					return;

		        }
            if((listCategories->currentItem())==item_Animals)
		        {
			       el_animals_ptr_list.sort(bk);
                    
                    initTable();
					return;
                }
            if((listCategories->currentItem())==item_Sounds)
		        {
  			      el_sound_ptr_list.sort(bk);
                    
				    initTable();
					return;
		        }
	         if((listCategories->currentItem())==item_Set_Dress)
		        {
			       el_setdress_ptr_list.sort(bk);
                   
					initTable();
					return;

		        }
            if((listCategories->currentItem())==item_Mech_Eff)
		        {
			       el_mechanical_ptr_list.sort(bk);
                    
                    initTable();
					return;
                }
                if((listCategories->currentItem())==item_Makeup_Hair)
		        {
  			      el_mkupheir_ptr_list.sort(bk);
                   
				    initTable();
					return;
		        }
	         if((listCategories->currentItem())==item_Music)
		        {
			       el_music_ptr_list.sort(bk);
                   
					initTable();
					return;

		        }
            if((listCategories->currentItem())==item_Greenery)
		        {
			       el_green_ptr_list.sort(bk);
                    
                    initTable();
					return;
                }
                if((listCategories->currentItem())==item_Spec_Equip)
		        {
  			      el_specequ_ptr_list.sort(bk);
                    
				    initTable();
					return;
		        }
	         if((listCategories->currentItem())==item_Security)
		        {
			       el_secur_ptr_list.sort(bk);
                    
					initTable();
					return;

		        }
            if((listCategories->currentItem())==item_Spec_Eff)
		        {
			       el_speceff_ptr_list.sort(bk);
                    
                    initTable();
					return;
                }
            if((listCategories->currentItem())==item_Vis_Eff)
                {
                   el_vieff_ptr_list.sort(bk);
                    
                    initTable();
                    return;
                }
            if((listCategories->currentItem())==item_Props)
                {
                   el_props_ptr_list.sort(bk);
                    
                    initTable();
                    return;
                }
            if((listCategories->currentItem())==item_Script_Day)
                {
                    script_day_ptr_list.sort(bk);
                    //script_day_ptr_list.sort();
                    initTable();
                    return;
                }
            if((listCategories->currentItem())==item_Set)
                {
                    set_ptr_list.sort(bk);
                    // set_ptr_list.sort();
                    initTable();
                    return;
                }
              if((listCategories->currentItem())==item_Unit)
                {
                    unit_ptr_list.sort(bk);
                    //unit_ptr_list.sort();
                    initTable();
                    return;
                }
              if((listCategories->currentItem())==item_Location)
                {
                    location_ptr_list.sort(bk);
                     //location_ptr_list.sort();
                    initTable();
                    return;
                }
              if((listCategories->currentItem())==item_Sequence)
                {
                    sequence_ptr_list.sort(bk);
                    //sequence_ptr_list.sort();
                    initTable();
                    return;
                }

}
    
void MojaTabela::columnClicked(int bk)
{  
    if(bk==2||bk==3|bk==4||bk==6)
    {
        if(bk==2){
            p_elman_form->sortComboBox->setCurrentItem(0);
            p_elman_form->sortList(0);
            }
        if(bk==3){
            p_elman_form->sortComboBox->setCurrentItem(1);
            p_elman_form->sortList(1);
            }
        if(bk==4){
            p_elman_form->sortComboBox->setCurrentItem(2);
            p_elman_form->sortList(2);
            }
        if(bk==6){
            p_elman_form->sortComboBox->setCurrentItem(3);
            p_elman_form->sortList(3);
            }
     //  QTable::columnClicked(bk);
     }
}
