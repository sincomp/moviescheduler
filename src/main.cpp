/****************************************************************************
** $Id:  qt/main.cpp   3.0.5   edited Oct 12 2001 $
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of an example program for Qt.  This example
** program may be used, distributed and modified without limitation.
**
*****************************************************************************/

#include <kapplication.h>
#include <kcmdlineargs.h>
#include "application.h"
#include <qsplashscreen.h>

int main( int argc, char ** argv ) {

    KCmdLineArgs::init(argc,argv,"Linsched","Linux Movie Schaduling","v09");
    KApplication a(argc, argv );
    QSplashScreen *splash =
            new QSplashScreen(QPixmap("./images/logo/sch_novi_logo1.png"));
    splash->show();  
    ApplicationWindow * mw = new ApplicationWindow();
    mw->setCaption( "Linux Scheduler" );
    mw->showMaximized();
    a.connect( &a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()) );
    for(int i=0;i<1000000;i++)
        for(int j=0;j<3000;j++);
    splash->finish(mw);
    delete splash;
    int res = a.exec();
    return res;
}
