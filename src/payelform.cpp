#include "payelform.h"

#include<qmessagebox.h>
#include<qlineedit.h>
#include<qcheckbox.h>
#include<qlistbox.h>
/////////////////////////////////////////////////////////
//definisanje globalne promenljive za redni broj elemanta
bool prvi_payel=false;
/////////////////////////////////////////////////////////

/* 
 *  Constructs a PayElForm which is a child of 'parent', with the
 *  name 'name' and widget flags set to 'f' 
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
PayElForm::PayElForm( PayElPtrList* bpl,ElManForm* parent,int br_el, const char* name, bool modal, WFlags fl )
    : PayElForm_base( parent, name, modal, fl )
{
    broj_elementa=br_el;
	un_payelm_ptr_list=bpl;
    un_payelm_ptr_list->setAutoDelete(TRUE);
    p_parent=parent;
    initForm();
    //nameLineEdit->setText("New Element");
    nameLineEdit->setFocus();
}

/*  
 *  Destroys the object and frees any allocated resources
 */
PayElForm::~PayElForm()
{
    // no need to delete child widgets, Qt does it all for us
}


void PayElForm::napuniKategoriju()
{      
      if((payelm=un_payelm_ptr_list->at(broj_elementa))==0)
        payelm=new PayElements;
        else payelm=un_payelm_ptr_list->at(broj_elementa);
        //payelm->setSheetNum(p_parent->current_sheet->getSheetNum());
        //payelm->setSheetNum(2);

        payelm->setBoardID(boardIDLineEdit->text());
        payelm->setElementName(nameLineEdit->text());
        payelm->setLocked(lockIDCheckBox->isChecked());
        payelm->setExcludeFromStripBoard(excFromCheckBox->isChecked());
        payelm->setPay(LineEdit16->text());
        payelm->setPay1(LineEdit17->text());
        payelm->setMinimum(LineEdit18->text());
       /* QDate d=p_parent->getPointtoAppWin()->current_sheet->getDate();
        if(d.isValid())
            payelm->setStartDate(d.toString("dd.MMMM 'yy"));   */
        //unsigned int s=0;
		if(broj_elementa<0)
		{
			if(proveriID())
			{
				un_payelm_ptr_list->append(payelm);
				//s=1;
				this->accept();
                p_parent->updatePayElmSheet(*un_payelm_ptr_list);
                p_parent->initTable();

            }
		}
		if(broj_elementa>=0)
		{
    		if(proveriID())
            {
			 //   un_payelm_ptr_list->remove(broj_elementa);
         //       un_payelm_ptr_list->insert(broj_elementa,payelm);
                //kod laptopa ne postoji replace
                //un_payelm_ptr_list->replace(broj_elementa,payelm);

                this->accept();
                p_parent->updatePayElmSheet(*un_payelm_ptr_list);
                p_parent->initTable();
            }

		}

        //p_parent->updatePayElmSheet(*un_payelm_ptr_list);
        

}
void PayElForm::initForm()
{
    if(broj_elementa>=0)
    {
        PayElements *payelm;
        payelm=un_payelm_ptr_list->at(broj_elementa);
        nameLineEdit->setText(QString(payelm->getElementName()) );
        boardIDLineEdit->setText(payelm->getBoardID() );
        lockIDCheckBox->setChecked(payelm->getLocked() );
        excFromCheckBox->setChecked(payelm->getExcludeFromStripBoard() );
        LineEdit16->setText(payelm->getPay() );
        LineEdit17->setText(payelm->getPay1() );
        LineEdit18->setText(payelm->getMinimum() );
      if(payelm->isStartSet())
        {
            LineEdit11->setEnabled( true );
            LineEdit12->setEnabled( true );
            LineEdit13->setEnabled( true );
            LineEdit14->setEnabled( true );
            LineEdit11->setText(QString::number(payelm->getOccurrence()));
            LineEdit12->setText(payelm->getStartDate());
            LineEdit13->setText(payelm->getFinishedDate());
            LineEdit14->setText(QString::number(payelm->getTotalDays()));
        }
      else
        {
            LineEdit11->setEnabled( false );
            LineEdit12->setEnabled( false);
            LineEdit13->setEnabled( false);
            LineEdit14->setEnabled( false);
        }
    }
}

 bool PayElForm::proveriID()
{
    unsigned int i=0;
	if(payelm->getBoardID()!="") //element ima boardID
  	{
        if(un_payelm_ptr_list->first())//postoji li prvi element u listi
            {
                prvi_payel=true;
            }
        if(prvi_payel)     // lista nije prazna
            {
                for(i=0; i<un_payelm_ptr_list->count(); ++i)
                {
                    if(((int)i==broj_elementa) && (i<un_payelm_ptr_list->count())) ++i;
                        if(i==un_payelm_ptr_list->count())
                         return true;
                    if( (un_payelm_ptr_list->at(i)->getBoardID() )==( boardIDLineEdit->text()) )
                    {

                        QMessageBox::warning(this,"Linux Scheduler",
                        "Uneli ste identifikacioni broj koji vec postoji!\n Mozete promeniti vrednost polja Borad ID.");
                        boardIDLineEdit->setFocus();
                        return false;
                    }

                }
               	return true;


            }

    }
    else  //element nema boardID
    payelm->setBoardID("");
	return true;
}

