#ifndef PAYELFORM_H
#define PAYELFORM_H
#include "payelform_base.h"

class PayElForm : public PayElForm_base
{
    Q_OBJECT

public:
    PayElForm( PayElPtrList* bpl=0,ElManForm* parent=0,int br_el=-1, const char* name=0, bool modal=TRUE, WFlags fl=0 );
    ~PayElForm();

public slots:
    void napuniKategoriju();
    bool proveriID();
    void initForm();

};

#endif // PAYELFORM_H
