/****************************************************************************
** Form implementation generated from reading ui file 'payelform_base.ui'
**
** Created: Tue Apr 12 20:38:20 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.2.1   edited May 19 14:22 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "payelform_base.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qcheckbox.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

/*
 *  Constructs a PayElForm_base as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
PayElForm_base::PayElForm_base( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
	setName( "PayElForm_base" );
    setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, sizePolicy().hasHeightForWidth() ) );
    setMaximumSize( QSize( 580, 590 ) );
//    setModal( TRUE );
    PayElForm_baseLayout = new QGridLayout( this, 1, 1, 11, 6, "PayElForm_baseLayout"); 

    excFromCheckBox = new QCheckBox( this, "excFromCheckBox" );
    excFromCheckBox->setMaximumSize( QSize( 165, 22 ) );

    PayElForm_baseLayout->addWidget( excFromCheckBox, 2, 0 );

    layout2 = new QHBoxLayout( 0, 0, 6, "layout2"); 
    QSpacerItem* spacer = new QSpacerItem( 375, 16, QSizePolicy::Fixed, QSizePolicy::Minimum );
    layout2->addItem( spacer );

    okPushButton = new QPushButton( this, "okPushButton" );
    okPushButton->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, okPushButton->sizePolicy().hasHeightForWidth() ) );
    layout2->addWidget( okPushButton );

    cancelPushButton = new QPushButton( this, "cancelPushButton" );
    cancelPushButton->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, cancelPushButton->sizePolicy().hasHeightForWidth() ) );
    layout2->addWidget( cancelPushButton );

    PayElForm_baseLayout->addLayout( layout2, 6, 0 );

    GroupBox2 = new QGroupBox( this, "GroupBox2" );
    GroupBox2->setMinimumSize( QSize( 556, 105 ) );
    GroupBox2->setMaximumSize( QSize( 556, 105 ) );

    TextLabel6 = new QLabel( GroupBox2, "TextLabel6" );
    TextLabel6->setGeometry( QRect( 10, 20, 111, 21 ) );

    TextLabel7 = new QLabel( GroupBox2, "TextLabel7" );
    TextLabel7->setGeometry( QRect( 10, 70, 135, 21 ) );

    TextLabel8 = new QLabel( GroupBox2, "TextLabel8" );
    TextLabel8->setGeometry( QRect( 250, 65, 43, 21 ) );

    LineEdit12 = new QLineEdit( GroupBox2, "LineEdit12" );
    LineEdit12->setEnabled( FALSE );
    LineEdit12->setGeometry( QRect( 150, 65, 90, 23 ) );

    LineEdit13 = new QLineEdit( GroupBox2, "LineEdit13" );
    LineEdit13->setEnabled( FALSE );
    LineEdit13->setGeometry( QRect( 295, 65, 105, 23 ) );

    TextLabel9 = new QLabel( GroupBox2, "TextLabel9" );
    TextLabel9->setGeometry( QRect( 405, 65, 71, 21 ) );

    LineEdit14 = new QLineEdit( GroupBox2, "LineEdit14" );
    LineEdit14->setEnabled( FALSE );
    LineEdit14->setGeometry( QRect( 480, 65, 60, 23 ) );

    LineEdit11 = new QLineEdit( GroupBox2, "LineEdit11" );
    LineEdit11->setEnabled( FALSE );
    LineEdit11->setGeometry( QRect( 150, 20, 50, 23 ) );

    PayElForm_baseLayout->addWidget( GroupBox2, 4, 0 );

    GroupBox3 = new QGroupBox( this, "GroupBox3" );
    GroupBox3->setMinimumSize( QSize( 556, 130 ) );
    GroupBox3->setMaximumSize( QSize( 556, 130 ) );

    TextLabel12 = new QLabel( GroupBox3, "TextLabel12" );
    TextLabel12->setGeometry( QRect( 11, 95, 53, 24 ) );

    TextLabel10 = new QLabel( GroupBox3, "TextLabel10" );
    TextLabel10->setGeometry( QRect( 11, 35, 53, 24 ) );

    TextLabel11 = new QLabel( GroupBox3, "TextLabel11" );
    TextLabel11->setGeometry( QRect( 11, 65, 53, 24 ) );

    LineEdit18 = new QLineEdit( GroupBox3, "LineEdit18" );
    LineEdit18->setGeometry( QRect( 70, 95, 472, 24 ) );

    LineEdit17 = new QLineEdit( GroupBox3, "LineEdit17" );
    LineEdit17->setGeometry( QRect( 70, 65, 472, 24 ) );

    LineEdit16 = new QLineEdit( GroupBox3, "LineEdit16" );
    LineEdit16->setGeometry( QRect( 70, 35, 472, 24 ) );

    PayElForm_baseLayout->addWidget( GroupBox3, 5, 0 );

    GroupBox1 = new QGroupBox( this, "GroupBox1" );
    GroupBox1->setMinimumSize( QSize( 556, 105 ) );
    GroupBox1->setMaximumSize( QSize( 556, 105 ) );
    GroupBox1->setFrameShape( QGroupBox::Box );
    GroupBox1->setFrameShadow( QGroupBox::Sunken );

    TextLabel5 = new QLabel( GroupBox1, "TextLabel5" );
    TextLabel5->setGeometry( QRect( 240, 65, 184, 24 ) );

    LineEdit10 = new QLineEdit( GroupBox1, "LineEdit10" );
    LineEdit10->setGeometry( QRect( 430, 60, 110, 24 ) );
    LineEdit10->setMaximumSize( QSize( 110, 24 ) );

    CheckBox1 = new QCheckBox( GroupBox1, "CheckBox1" );
    CheckBox1->setGeometry( QRect( 20, 30, 163, 22 ) );

    CheckBox2 = new QCheckBox( GroupBox1, "CheckBox2" );
    CheckBox2->setGeometry( QRect( 20, 65, 163, 22 ) );

    PayElForm_baseLayout->addWidget( GroupBox1, 3, 0 );

    layout5 = new QHBoxLayout( 0, 0, 6, "layout5"); 

    TextLabel1 = new QLabel( this, "TextLabel1" );
    TextLabel1->setMaximumSize( QSize( 95, 24 ) );
    layout5->addWidget( TextLabel1 );

    nameLineEdit = new QLineEdit( this, "nameLineEdit" );
    nameLineEdit->setMaximumSize( QSize( 360, 24 ) );
    layout5->addWidget( nameLineEdit );
    nameLineEdit->setText("New Element");
    nameLineEdit->setFocus();
    QSpacerItem* spacer_2 = new QSpacerItem( 90, 16, QSizePolicy::Preferred, QSizePolicy::Minimum );
    layout5->addItem( spacer_2 );

    PayElForm_baseLayout->addLayout( layout5, 0, 0 );

    layout6 = new QHBoxLayout( 0, 0, 6, "layout6"); 

    TextLabel2 = new QLabel( this, "TextLabel2" );
    TextLabel2->setMaximumSize( QSize( 51, 24 ) );
    layout6->addWidget( TextLabel2 );

    boardIDLineEdit = new QLineEdit( this, "boardIDLineEdit" );
    boardIDLineEdit->setMaximumSize( QSize( 98, 24 ) );
    layout6->addWidget( boardIDLineEdit );

    lockIDCheckBox = new QCheckBox( this, "lockIDCheckBox" );
    layout6->addWidget( lockIDCheckBox );
    QSpacerItem* spacer_3 = new QSpacerItem( 331, 16, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout6->addItem( spacer_3 );

    PayElForm_baseLayout->addLayout( layout6, 1, 0 );
    languageChange();
    resize( QSize(580, 590).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( okPushButton, SIGNAL( clicked() ), this, SLOT( napuniKategoriju() ) );
    connect( cancelPushButton, SIGNAL( clicked() ), this, SLOT( reject() ) );

    // tab order
    setTabOrder( nameLineEdit, boardIDLineEdit );
    setTabOrder( boardIDLineEdit, lockIDCheckBox );
    setTabOrder( lockIDCheckBox, excFromCheckBox );
    setTabOrder( excFromCheckBox, LineEdit16 );
    setTabOrder( LineEdit16, LineEdit17 );
    setTabOrder( LineEdit17, LineEdit18 );
    setTabOrder( LineEdit18, CheckBox1 );
    setTabOrder( CheckBox1, CheckBox2 );
    setTabOrder( CheckBox2, LineEdit10 );
    setTabOrder( LineEdit10, okPushButton );
    setTabOrder( okPushButton, cancelPushButton );
    setTabOrder( cancelPushButton, LineEdit11 );
    setTabOrder( LineEdit11, LineEdit12 );
    setTabOrder( LineEdit12, LineEdit13 );
    setTabOrder( LineEdit13, LineEdit14 );
}

/*                                                             totalDaysLineEdit
 *  Destroys the object and frees any allocated resources
 */
PayElForm_base::~PayElForm_base()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void PayElForm_base::languageChange()
{
    setCaption( tr( "Edit Element:" ) );
    excFromCheckBox->setText( tr( "Exclude From Stripboard" ) );
    okPushButton->setText( tr( "&OK" ) );
    okPushButton->setAccel( QKeySequence( tr( "Alt+O" ) ) );
    cancelPushButton->setText( tr( "&Cancel" ) );
    cancelPushButton->setAccel( QKeySequence( tr( "Alt+C" ) ) );
    GroupBox2->setTitle( tr( "Usage" ) );
    TextLabel6->setText( tr( "Total Occurrences:" ) );
    TextLabel7->setText( tr( "Active Schedule Start:" ) );
    TextLabel8->setText( tr( "Finish:" ) );
    TextLabel9->setText( tr( "Total Days:" ) );
    GroupBox3->setTitle( tr( "Element Properties" ) );
    TextLabel12->setText( tr( "Minimum" ) );
    TextLabel10->setText( tr( "Pay" ) );
    TextLabel11->setText( tr( "Pay1" ) );
    GroupBox1->setTitle( tr( "Day Out Of Days" ) );
    TextLabel5->setText( tr( "Min.Days Between Drop/Pickup" ) );
    CheckBox1->setText( tr( "Allow Hold Days" ) );
    CheckBox2->setText( tr( "Allow Drop/Pickup Days" ) );
    TextLabel1->setText( tr( "Element Name" ) );
    TextLabel2->setText( tr( "Board ID" ) );
    lockIDCheckBox->setText( tr( "Lock ID" ) );
}

void PayElForm_base::napuniKategoriju()
{
    qWarning( "PayElForm_base::napuniKategoriju(): Not implemented yet" );
}

bool PayElForm_base::proveriID()
{
    qWarning( "PayElForm_base::proveriID(): Not implemented yet" );
    return FALSE;
}

void PayElForm_base::initForm()
{
    qWarning( "PayElForm_base::initForm(): Not implemented yet" );
}

