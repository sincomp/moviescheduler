/****************************************************************************
** Form interface generated from reading ui file 'payelform_base.ui'
**
** Created: Tue Apr 12 20:38:20 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.2.1   edited May 19 14:22 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef PAYELFORM_BASE_H
#define PAYELFORM_BASE_H

#include <qvariant.h>
#include <qdialog.h>
#include "elmanform.h"

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QCheckBox;
class QPushButton;
class QGroupBox;
class QLabel;
class QLineEdit;

class PayElForm_base : public QDialog
{
    Q_OBJECT

public:
    PayElForm_base( QWidget* parent = 0, const char* name = 0, bool modal = true, WFlags fl = 0 );
    ~PayElForm_base();

    QCheckBox* excFromCheckBox;
    QPushButton* okPushButton;
    QPushButton* cancelPushButton;
    QGroupBox* GroupBox2;
    QLabel* TextLabel6;
    QLabel* TextLabel7;
    QLabel* TextLabel8;
    QLineEdit* LineEdit12;
    QLineEdit* LineEdit13;
    QLabel* TextLabel9;
    QLineEdit* LineEdit14;
    QLineEdit* LineEdit11;
    QGroupBox* GroupBox3;
    QLabel* TextLabel12;
    QLabel* TextLabel10;
    QLabel* TextLabel11;
    QLineEdit* LineEdit18;
    QLineEdit* LineEdit17;
    QLineEdit* LineEdit16;
    QGroupBox* GroupBox1;
    QLabel* TextLabel5;
    QLineEdit* LineEdit10;
    QCheckBox* CheckBox1;
    QCheckBox* CheckBox2;
    QLabel* TextLabel1;
    QLineEdit* nameLineEdit;
    QLabel* TextLabel2;
    QLineEdit* boardIDLineEdit;
    QCheckBox* lockIDCheckBox;

public slots:
    virtual void napuniKategoriju();
    virtual bool proveriID();
    virtual void initForm();

protected:
    int broj_elementa;
    ElManForm* p_parent;
    PayElements *payelm;
    PayElPtrList *un_payelm_ptr_list;

    QGridLayout* PayElForm_baseLayout;
    QHBoxLayout* layout2;
    QHBoxLayout* layout5;
    QHBoxLayout* layout6;

protected slots:
    virtual void languageChange();

};

#endif // PAYELFORM_BASE_H
