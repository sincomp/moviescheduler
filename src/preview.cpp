/***************************************************************************
                          preview.cpp  -  description
                             -------------------
    begin                : Sat Apr 9 2005
    copyright            : (C) 2005 by Sinisa Denic
    email                : sincomp@ptt.yu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 #include"preview.h"
 #include<qpainter.h>
 #include<qprinter.h>
 #include<qwidget.h>
 #include<qlayout.h>
 #include<qpushbutton.h>
 #include"sheet.h"

 Preview::Preview(QWidget* parent,Sheet* cur_sheet,const char* name,QPrinter* pr, int Wflags):QDialog(parent,name,Wflags)
 {                                                        
   printer=pr;
   current_sheet=cur_sheet;
   setCaption("Print Preview");
 
    printPushButton = new QPushButton( this, "printPushButton" );
    printPushButton->setGeometry( QRect( 430, 800, 100, 30 ) );

    cancelPushButton = new QPushButton( this, "cancelPushButton" );
    cancelPushButton->setGeometry( QRect( 545, 800, 100, 30 ) );
    printPushButton->setText( tr( "Print" ) );
    cancelPushButton->setText( tr( "Cancel" ) );
   resize( QSize(660, 870).expandedTo(minimumSizeHint()) );
   
   connect(printPushButton,SIGNAL(clicked()),this,SLOT ( accept() ) );
   connect(cancelPushButton,SIGNAL(clicked()),this,SLOT (reject() ) );
 }
 void Preview::paintEvent(QPaintEvent*)
 {

       //this->setPalette(QPalette(QColor(250,250,245) ) );
        //this->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Expanding);
        QPainter p( this );
        p.setPen( black );
        p.setFont( QFont( "Courier", 14, QFont::Normal) );

        //QPaintDeviceMetrics metrics( printer ); // need width/height
     //  int yPos        = 0;
	p.drawText( 270, 74,"Breakdown Sheet");
	p.setFont( QFont( "Courier", 12, QFont::Normal ) );
    QFontMetrics fm = p.fontMetrics();
    p.drawText( 10, 51,"Scene#:" );
    int razmak=fm.width("Scene#:")+10;
    p.drawText( 10+razmak,51,current_sheet->getScenes() );
    
	p.drawText( 10, 74,"Script Page:" );
    razmak=fm.width("Script Page:")+10;
    p.drawText( 10+razmak,74,current_sheet->getScriptPages() );

    p.drawText( 10, 97,"Page Count:" );
    razmak=fm.width("Page Count:")+10;
    p.drawText( 10+razmak,97,current_sheet->getPage1()+current_sheet->getPage2()+"/8" );

    p.drawText( 10, 161,"Scene Description:" );
    razmak=fm.width("Scene Description:")+10;
    p.drawText( 10+razmak,161,current_sheet->getSynopsis() );

    p.drawText( 10, 184,"Settings:" );
    razmak=fm.width("Settings:")+10;
    p.drawText( 10+razmak,184,current_sheet->getSet() );


    p.drawText( 10, 207,"Location:" );
    razmak=fm.width("Location:")+10;
    p.drawText( 10+razmak,207,current_sheet->getLocation() );
    
	p.drawText( 10, 230,"Sequence:" );
    razmak=fm.width("Sequence:")+10;
    p.drawText( 10+razmak,230,current_sheet->getSequence() );
    
	p.drawText( 520,51,"Sheet:" );
    razmak=fm.width("Sheet:")+10;
    p.drawText( 520+razmak,51,QString::number(current_sheet->getSheetNum()) );
    
    p.drawText( 520,74,"Int/Ext:" );
    razmak=fm.width("Int/Ext:")+10;
    p.drawText( 520+razmak,74,current_sheet->getIntExtStr() );
    
    p.drawText( 520,97,"Day/Night:" );
    razmak=fm.width("Day/Night:")+10;
    p.drawText( 520+razmak,97,current_sheet->getDayNightStr() );
    
    p.drawText( 438, 230,"Script Day:" );
    razmak=fm.width("Script Day:")+10;
    p.drawText( 438+razmak,230,current_sheet->getScriptDay() );
    
    p.drawLine( 10, 130, 645, 130 );
    
    
    QRect r_2 = QRect( 226, 261, 208, 123 );
    QRect r_11( 440, 647, 208, 123 );
    QRect r_6( 10, 518, 208, 123 );
    QRect r_1( 10, 261, 208, 251 );
	QRect r_5( 440, 390, 208, 123 );
    QRect r_3( 440, 261, 208, 123 );
    QRect r_9( 10, 647, 208, 123 );
    QRect r_7( 226, 518, 208, 123 );
    QRect r_8( 440, 518, 208, 123 );
    QRect r_4( 226, 390, 208, 123 );
    QRect r_10( 226, 647, 208, 123 );
    p.drawRect(r_2);
    p.drawRect(r_1);
    p.drawRect(r_11);
    p.drawRect(r_6);
    p.drawRect(r_5);
    p.drawRect(r_3);
    p.drawRect(r_9);
    p.drawRect(r_10);
    p.drawRect(r_7);
    p.drawRect(r_4);
    p.drawRect(r_8);
    QPoint p_1( 10, 261);
    QPoint p_2( 226, 261);
    QPoint p_11( 440, 647 );
    QPoint p_6( 10, 518 );
	QPoint p_5( 440, 390);
    QPoint p_3( 440, 261 );
   
    QPoint p_7( 226, 518);
    QPoint p_8(440, 518);
    QPoint p_4( 226, 390);
    QPoint  p_10( 226, 647);
    QPoint p_9( 10, 647 );
    p.setFont( QFont( "Courier", 10, QFont::Normal ) );
      //QPoint temp;
      QValueList<QStringList> kateg_list;
      kateg_list.append(current_sheet->cast_list);
      kateg_list.append(current_sheet->bact_list);
      kateg_list.append(current_sheet->stunts_list);
      kateg_list.append(current_sheet->addlab_list);
      kateg_list.append(current_sheet->animals_list);
      kateg_list.append(current_sheet->anwrangler_list);
      kateg_list.append(current_sheet->greenary_list);
      kateg_list.append(current_sheet->mecheff_list);
      kateg_list.append(current_sheet->mkuphair_list);
      kateg_list.append(current_sheet->props_list);
      kateg_list.append(current_sheet->security_list);
      kateg_list.append(current_sheet->setdress_list);
      kateg_list.append(current_sheet->sound_list);
      kateg_list.append(current_sheet->speceff_list);
      kateg_list.append(current_sheet->specequip_list);
      kateg_list.append(current_sheet->viseff_list);
      kateg_list.append(current_sheet->music_list);
        QValueList<QStringList>::iterator kateg_iter;
        QValueList<QPoint> point_list;
      point_list.append(p_1);point_list.append(p_2);point_list.append(p_3);point_list.append(p_4);
      point_list.append(p_5);point_list.append(p_6);point_list.append(p_7);point_list.append(p_8);
      point_list.append(p_9);point_list.append(p_10);point_list.append(p_11);
      QValueList<QPoint>::iterator point_iter;
      point_iter=point_list.begin();         
        for(kateg_iter=kateg_list.begin();kateg_iter!=kateg_list.end();)
        {
            QStringList temp;
            bool ispisao=false;
            //QPoint temp_point;
            
            while(kateg_iter!=kateg_list.end())
            {
                temp=*kateg_iter;
                if(temp.count()>1){
                
                ispisiKategoriju((*point_iter),(*kateg_iter),p);
                if(point_iter!=point_list.end()) ++point_iter;
                ++kateg_iter;
                ispisao=true;
                break;
                }
                if(!ispisao){
                   // if(point_iter!=point_list.end()) ++point_iter;
                ++kateg_iter;
                }
             }

                 
       }


        
        
}
void Preview::ispisiKategoriju(QPoint& p_kat, QStringList& str_l,QPainter&p)
{
    int yPos=0;
    QFontMetrics fm = p.fontMetrics();
    fm = p.fontMetrics();
    p_kat.setX(p_kat.x()+3);//x+3
    for ( QStringList::Iterator it0 = str_l.begin(); it0 != str_l.end(); ++it0 )
            {
                p_kat.setY(p_kat.y()+fm.lineSpacing());
                p.drawText( p_kat,*it0);
                yPos = yPos + fm.lineSpacing();
             }
}
void Preview::accept()
{
          QPainter p;
          p.begin( printer);
         
        p.setPen( black );
        p.setFont( QFont( "Courier", 12, QFont::Normal ) );

        //QPaintDeviceMetrics metrics( printer ); // need width/height
      	p.drawText( 270, 15,"Breakdown Sheet");
	p.setFont( QFont( "Courier", 10, QFont::Normal ) );
    QFontMetrics fm = p.fontMetrics();
    p.drawText( 10, 51,"Scene#:" );
    int razmak=fm.width("Scene#:");//+10;
    p.drawText( 10+razmak,51,current_sheet->getScenes() );

	p.drawText( 10, 74,"Script Page:" );
    razmak=fm.width("Script Page:");//+10;
    p.drawText( 10+razmak,74,current_sheet->getScriptPages() );

    p.drawText( 10, 97,"Page Count:" );
    razmak=fm.width("Page Count:");//+10;
    p.drawText( 10+razmak,97,current_sheet->getPage1()+current_sheet->getPage2()+"/8" );

    p.drawText( 10, 161,"Scene Description:" );
    razmak=fm.width("Scene Description:");//+10;
    p.drawText( 10+razmak,161,current_sheet->getSynopsis() );

    p.drawText( 10, 184,"Settings:" );
    razmak=fm.width("Settings:");//+10;
    p.drawText( 10+razmak,184,current_sheet->getSet() );


    p.drawText( 10, 207,"Location:" );
    razmak=fm.width("Location:");//+10;
    p.drawText( 10+razmak,207,current_sheet->getLocation() );

	p.drawText( 10, 230,"Sequence:" );
    razmak=fm.width("Sequence:");//+10;
    p.drawText( 10+razmak,230,current_sheet->getSequence() );

	p.drawText( 440,51,"Sheet:" );
    razmak=fm.width("Sheet:");//+10;
    p.drawText( 440+razmak,51,QString::number(current_sheet->getSheetNum()) );

    p.drawText( 440,74,"Int/Ext:" );
    razmak=fm.width("Int/Ext:");//+10;
    p.drawText( 440+razmak,74,current_sheet->getIntExtStr() );

    p.drawText( 440,97,"Day/Night:" );
    razmak=fm.width("Day/Night:");//+10;
    p.drawText( 440+razmak,97,current_sheet->getDayNightStr() );

    p.drawText( 408, 230,"Script Day:" );
    razmak=fm.width("Script Day:");//+10;
    p.drawText( 408+razmak,230,current_sheet->getScriptDay() );

    p.drawLine( 10, 130, 550, 130 );


    QRect r_2 = QRect( 176, 261, 160, 108 );
    QRect r_11( 347, 602, 160, 108 );
    QRect r_6( 10, 488, 160, 108 );
    QRect r_1( 10, 261, 160, 221 );
	QRect r_5( 347, 375, 160, 108 );
    QRect r_3( 347, 261, 160, 108 );
    QRect r_9( 10, 602, 160, 108 );
    QRect r_7( 176, 488, 160, 108 );
    QRect r_8( 347, 488, 160, 108 );
    QRect r_4( 176, 375, 160, 108 );
    QRect r_10( 176, 602, 160, 108 );

    p.drawRect(r_2);
    p.drawRect(r_1);
    p.drawRect(r_11);
    p.drawRect(r_6);
    p.drawRect(r_5);
    p.drawRect(r_3);
    p.drawRect(r_9);
    p.drawRect(r_10);
    p.drawRect(r_7);
    p.drawRect(r_4);
    p.drawRect(r_8);
    //tacke gornjeg levog ugla pravougaonikaa
    QPoint p_1( 10, 261);
    QPoint p_2( 176, 261);
    QPoint p_11( 347, 602 );
    QPoint p_6( 10, 488 );
	QPoint p_5( 347, 375);
    QPoint p_3( 347, 261 );
    QPoint p_7( 176, 488);
    QPoint p_8(347, 548);
    QPoint p_4( 176, 375);
    QPoint  p_10( 176, 602);
    QPoint p_9( 10, 602 );
    /////////////////////
    
    p.setFont( QFont( "Courier", 10, QFont::Bold ) );
     QValueList<QStringList> kateg_list;
      kateg_list.append(current_sheet->cast_list);
      kateg_list.append(current_sheet->bact_list);
      kateg_list.append(current_sheet->stunts_list);
      kateg_list.append(current_sheet->addlab_list);
      kateg_list.append(current_sheet->animals_list);
      kateg_list.append(current_sheet->anwrangler_list);
      kateg_list.append(current_sheet->greenary_list);
      kateg_list.append(current_sheet->mecheff_list);
      kateg_list.append(current_sheet->mkuphair_list);
      kateg_list.append(current_sheet->props_list);
      kateg_list.append(current_sheet->security_list);
      kateg_list.append(current_sheet->setdress_list);
      kateg_list.append(current_sheet->sound_list);
      kateg_list.append(current_sheet->speceff_list);
      kateg_list.append(current_sheet->specequip_list);
      kateg_list.append(current_sheet->viseff_list);
      kateg_list.append(current_sheet->music_list);
        QValueList<QStringList>::iterator kateg_iter;
        QValueList<QPoint> point_list;
      point_list.append(p_1);point_list.append(p_2);point_list.append(p_3);point_list.append(p_4);
      point_list.append(p_5);point_list.append(p_6);point_list.append(p_7);point_list.append(p_8);
      point_list.append(p_9);point_list.append(p_10);point_list.append(p_11);
      QValueList<QPoint>::iterator point_iter;
      point_iter=point_list.begin();
        for(kateg_iter=kateg_list.begin();kateg_iter!=kateg_list.end();)
        {
            QStringList temp;
            bool ispisao=false;
            //QPoint temp_point;

            while(kateg_iter!=kateg_list.end())
            {
                temp=*kateg_iter;
                if(temp.count()>1){

                ispisiKategoriju((*point_iter),(*kateg_iter),p);
                if(point_iter!=point_list.end()) ++point_iter;
                ++kateg_iter;
                ispisao=true;
                break;
                }
                if(!ispisao){
                   // if(point_iter!=point_list.end()) ++point_iter;
                ++kateg_iter;
                }
             }


       }


          
         
    
             p.end();
    QDialog::accept();
    delete(this);
}
