/***************************************************************************
                          preview.h  -  description
                             -------------------
    begin                : Sat Apr 9 2005
    copyright            : (C) 2005 by Sinisa Denic
    email                : sincomp@ptt.yu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
// #include<qpainter.h>
 #include<qdialog.h>
 class Sheet;
 //class QPushButton;
 class Preview:public QDialog
{
    public:
    Preview(){};
    ~Preview(){};
    Preview(QWidget* parent=0,Sheet* cur_sheet=0,const char* name=0,QPrinter* pr=0,int WFlags=0);
    protected:
    void  paintEvent(QPaintEvent*);
    protected slots:
    void accept();
    private:
    void ispisiKategoriju(QPoint& p_kat, QStringList& str_l,QPainter&p);
    Sheet* current_sheet;
    QPrinter* printer;
    QPushButton* printPushButton;
    QPushButton* cancelPushButton;
};
