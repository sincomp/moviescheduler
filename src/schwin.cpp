/***************************************************************************
                          schwin.cpp  -  description
                             -------------------
    begin                : Tue Mar 29 2005
    copyright            : (C) 2005 by Sinisa Denic
    email                : sincomp@ptt.yu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include"schwin.h"
Sheet* SchWin::current_sheet; 
void SchWin::updateSheet()
{
    //ovde mozda treba nesto dodati da se nebi rusio program
    current_sheet->cast_list.clear();
    QString name_of_categ(" Cast Members");
    current_sheet->cast_list.push_front(name_of_categ);
    QString el,sn,id;
    if(el_cast_ptr_list.first())
    {
        QPtrListIterator<CastMembers> it(el_cast_ptr_list);
        CastMembers* cm;

        while((cm=it.current())!=0)
        {
            ++it;                
            if(cm->getSheetNum(current_sheet->getSheetNum()) )
            {
                
//priprema imena  CastMembers elemaneta za ispis u tabeli brksheet

                el=cm->getElementName();
                
                id=cm->getBoardID();
                if(id.isEmpty())
               
                id+="    ";
                else
                {
                     int len=4-id.length();
                     for(int i=0;i<len;++i)
                     id.prepend(" ");
                    id+=". ";
                }
                
                
//formiranje stringa za prikazivanje  u tabeli brksheet
                id+=el;id+=" ";//id+=sn;id+=".";
                current_sheet->cast_list.push_back(id);
            }

        }
    }
    
    current_sheet->stunts_list.clear();
    current_sheet->stunts_list.push_front(" Stunts");
    if(el_stunts_ptr_list.first())
    {
        QPtrListIterator<Stunts> it(el_stunts_ptr_list);
        Stunts* st;
        while((st=it.current())!=0)
        {
            ++it;
            if(st->getSheetNum(current_sheet->getSheetNum()) )
            {
                el=st->getElementName();
                if((id=st->getBoardID()).isEmpty() )
                 id+="    ";
                 else
                {
                     int len=4-id.length();
                     for(int i=0;i<len;++i)
                     id.prepend(" ");
                    id+=". ";
                }
                 id+=el;
                current_sheet->stunts_list.push_back(id);
            }
        }
    }
    
}
void SchWin::unsetBuiltElemSheetNum(BuiltInCategories& built_ptr_list)
{
    if(!built_ptr_list.isEmpty())
    //if(built_ptr_list.first())
    {
        QPtrListIterator<Elements> it(built_ptr_list);
        Elements* element;
        while((element=it.current())!=0)
        {
            ++it;

            if(built_ptr_list==set_ptr_list)
            {
                if(element->getSheetNum(current_sheet->getSheetNum()) )
                    element->setSheetNum(current_sheet->getSheetNum());
            }
            if(built_ptr_list==unit_ptr_list)
            {
                if(element->getSheetNum(current_sheet->getSheetNum()) )
                    element->setSheetNum(current_sheet->getSheetNum());
            }    
            if(built_ptr_list==location_ptr_list)
            {
                if(element->getSheetNum(current_sheet->getSheetNum()) )
                    element->setSheetNum(current_sheet->getSheetNum());
            }
            if(built_ptr_list==sequence_ptr_list)
            {
                if(element->getSheetNum(current_sheet->getSheetNum()) )
                    element->setSheetNum(current_sheet->getSheetNum());
            }
             if(built_ptr_list==script_day_ptr_list)
            {
                if(element->getSheetNum(current_sheet->getSheetNum()) )
                    element->setSheetNum(current_sheet->getSheetNum());
            }

        }
    }

}
void SchWin::unsetPayElemSheetNum()
{
    unsigned int i=0;
 while(i<=14)
  {
        PayElPtrList p_payelm;
        switch(i)
        {
            case 0: p_payelm=el_addlab_ptr_list;
                break;
            case 1: p_payelm=el_animals_ptr_list;
                break;
            case 2: p_payelm=el_anwrangler_ptr_list;
                break;
            case 3: p_payelm=el_setdress_ptr_list;
                break;
            case 4: p_payelm=el_sound_ptr_list;
                break;
            case 5: p_payelm=el_mechanical_ptr_list;
                break;
            case 6: p_payelm=el_mkupheir_ptr_list;
                break;
            case 7: p_payelm=el_music_ptr_list;
                break;
            case 8: p_payelm=el_green_ptr_list;
                break;

            case 9: p_payelm=el_specequ_ptr_list;
                break;
            case 10: p_payelm=el_secur_ptr_list;
                break;
            case 11: p_payelm=el_vieff_ptr_list;
                break;
            case 12: p_payelm=el_props_ptr_list;
                break;
            case 13: p_payelm=el_speceff_ptr_list;
                break;
            case 14: p_payelm=el_bact_ptr_list;
                break;
        }

        QPtrListIterator<PayElements> it3(p_payelm);
        PayElements* pay_elm;
        while((pay_elm=it3.current())!=0)
        {
             PayElements* pe=pay_elm;
            ++it3;
            if(pe->getSheetNum(current_sheet->getSheetNum()) )
            pe->setSheetNum(current_sheet->getSheetNum());
        }
        ++i;
    }
}
void SchWin::updateBuiltSheet(BuiltInCategories& built_ptr_list)
//void SchWin::updateBuiltSheet(QPtrList<Elements>& built_ptr_list) 
{
    if(!built_ptr_list.isEmpty())
    //if(built_ptr_list.first())
    {
        QPtrListIterator<Elements> it(built_ptr_list);
        Elements* element;
        while((element=it.current())!=0)
        {
            ++it;

            if(built_ptr_list==set_ptr_list)
            {
                if(element->getSheetNum(current_sheet->getSheetNum()) )
                    current_sheet->setSet(element->getElementName());
            }
            if(built_ptr_list==unit_ptr_list)
            {
                if(element->getSheetNum(current_sheet->getSheetNum()) )
                    current_sheet->setUnit(element->getElementName());
            }
            if(built_ptr_list==location_ptr_list)
            {
                if(element->getSheetNum(current_sheet->getSheetNum()) )
                    current_sheet->setLocation(element->getElementName());
            }
            if(built_ptr_list==sequence_ptr_list)
            {
                if(element->getSheetNum(current_sheet->getSheetNum()) )
                    current_sheet->setSequence(element->getElementName());
            }
             if(built_ptr_list==script_day_ptr_list)
            {
                if(element->getSheetNum(current_sheet->getSheetNum()) )
                    current_sheet->setScriptDay(element->getElementName());
            }

        }
    }

}
//void SchWin::updatePayElmSheet(QPtrList<PayElements>& payelm_ptr_list)
void SchWin::updatePayElmSheet(PayElPtrList& payelm_ptr_list) 
{
        //QPtrListIterator<PayElements> it_p_elem(payelm_ptr_list);
         QString name;
         QStringList* temp_pel_strlist=0;
         QString el,sn,id;
        if(payelm_ptr_list==el_addlab_ptr_list)
        {
            name=" Additional Labor";
            current_sheet->addlab_list.clear();
            current_sheet->addlab_list.push_front(name);
            //paznja!!!
            temp_pel_strlist=(&current_sheet->addlab_list);
        }
        if(payelm_ptr_list==el_animals_ptr_list)
        {
            name=" Animals";
            current_sheet->animals_list.clear();
            current_sheet->animals_list.push_front(name);
            temp_pel_strlist=(&current_sheet->animals_list);
        }
        if(payelm_ptr_list==el_anwrangler_ptr_list)
        {
            
            name=" Animal Wrangler";
            current_sheet->anwrangler_list.clear();
            current_sheet->anwrangler_list.push_front(name);
            temp_pel_strlist=(&current_sheet->anwrangler_list);
        }
        if(payelm_ptr_list==el_bact_ptr_list)
        {
            
            name=" Background Actors";
            current_sheet->bact_list.clear();
            current_sheet->bact_list.push_front(name);
            temp_pel_strlist=(&current_sheet->bact_list);
        }           
        if(payelm_ptr_list==el_setdress_ptr_list)
        {
            name=" Set Dressing" ;
            current_sheet->setdress_list.clear();   
            current_sheet->setdress_list.push_front(name);
            //paznja!!!
            temp_pel_strlist=(&current_sheet->setdress_list);
        }
        if(payelm_ptr_list==el_specequ_ptr_list)
        {
            name=" Special Equipment";
            current_sheet->specequip_list.clear();
            current_sheet->specequip_list.push_front(name);
            temp_pel_strlist=(&current_sheet->specequip_list);
        }
        if(payelm_ptr_list==el_secur_ptr_list)
        {

            name=" Security";
            current_sheet->security_list.clear();
            current_sheet->security_list.push_front(name);
            temp_pel_strlist=(&current_sheet->security_list);
        }
        if(payelm_ptr_list==el_green_ptr_list)
        {

            name=" Geenery";
            current_sheet->greenary_list.clear();
            current_sheet->greenary_list.push_front(name);
            temp_pel_strlist=(&current_sheet->greenary_list);
        }
       if(payelm_ptr_list==el_mechanical_ptr_list)
        {
            name=" Machanical Effects";
            current_sheet->mecheff_list.clear();
            current_sheet->mecheff_list.push_front(name);
            //paznja!!!
            temp_pel_strlist=(&current_sheet->addlab_list);
        }
        if(payelm_ptr_list==el_mkupheir_ptr_list)
        {
            name=" Makeup/Hair";
            current_sheet->mkuphair_list.clear();
            current_sheet->mkuphair_list.push_front(name);
            temp_pel_strlist=(&current_sheet->mkuphair_list);
        }
        if(payelm_ptr_list==el_props_ptr_list)
        {

            name=" Props";
            current_sheet->props_list.clear();
            current_sheet->props_list.push_front(name);
            temp_pel_strlist=(&current_sheet->props_list);
        }
        if(payelm_ptr_list==el_music_ptr_list)
        {

            name=" Music" ;
            current_sheet->music_list.clear();
            current_sheet->music_list.push_front(name);
            temp_pel_strlist=(&current_sheet->music_list);
        }
         if(payelm_ptr_list==el_speceff_ptr_list)
        {

            name=" Special Effects";
            current_sheet->speceff_list.clear();
            current_sheet->speceff_list.push_front(name);
            temp_pel_strlist=(&current_sheet->speceff_list);
        }
        if(payelm_ptr_list==el_sound_ptr_list)
        {

            name=" Sounds";
            current_sheet->sound_list.clear();
            current_sheet->sound_list.push_front(name);
            temp_pel_strlist=(&current_sheet->sound_list);
        }
        if(payelm_ptr_list==el_vieff_ptr_list)
        {

            name=" Visual Effects" ;
            current_sheet->viseff_list.clear();
            current_sheet->viseff_list.push_front(name);
            temp_pel_strlist=(&current_sheet->viseff_list);
        }
    if(payelm_ptr_list.first())
    {
        QPtrListIterator<PayElements> it(payelm_ptr_list);
        PayElements* pay_element;
        while((pay_element=it.current())!=0)
        {
            ++it;
            if(pay_element->getSheetNum(current_sheet->getSheetNum()) )
            {
                el=pay_element->getElementName();
                if((id=pay_element->getBoardID()).isEmpty())
                id+="    ";
                else
                {
                     int len=4-id.length();
                     for(int i=0;i<len;++i)
                     id.prepend(" ");
                    id+=". ";
                }
                 id+=el;
                current_sheet->pushBackOnList(temp_pel_strlist,id);
            }
        }
    }
}
SheetsPtrList SchWin::sheets_list;
void SchWin::setElementDate(Elements*cm)
{
    
   //deo koji se bavi dodeljivanjem start datuma za element cm
   //pri direktnom unosenju na raspored
              // if(cm->getOccurrence()==1)
               if(cm->isStartSet())
               {

                    Sheet* sh;
                     QPtrListIterator<Sheet> itsheet(sheets_list);
                     itsheet.toFirst();
                    while((sh=itsheet.current())!=0)
                    {
                        ++itsheet;
                        if(cm->getSheetNum(sh->getSheetNum()))
                        {
                            QDate d=sh->getDate();
                            if(d.isValid())
                                cm->setStartDate(d.toString("dd.MMMM 'yy"));
                                 
                            cm->setStartIsSet(true);
                            if(cm->isElemRemoved())
                            {
                               // cm->decOccur();
                                cm->setElemIsRemoved(false);
                               // if(cm->getOccurrence()==0)
                                 //   cm->setStartIsSet(false);
                            }
                            //else
                              //  cm->incOccur();
                            break;
                        }
                    }
                }
                else
                {
                    cm->setStartDate("");
                    cm->setFinishedDate("");
                }
                postaviDatumeZaElemente(true);
}
//ova finkcija bi trebalo da se poziva pri svakom unosu ili promeni datuma pocetka na kalendaru

void SchWin::postaviDatumeZaElemente(bool force)
{

    Sheet* sh;
    QPtrListIterator<Sheet> itsheet(sheets_list);
    QPtrListIterator<CastMembers> it(el_cast_ptr_list);
    CastMembers* cm;
    QDate start;//(y,m,days);
    int total;
//resetovanje broja pojavljivanja kod svih elmenata;
   if(force)
    while((cm=it.current())!=0)
            {
                ++it;
                cm->setOccurrence(0);
                cm->setStartIsSet(false);
            }

    itsheet.toFirst();
    while((sh=itsheet.current())!=0)
    {
        if(sh->getDayBreak())
        {
             ++itsheet;
             continue;
             }
        ++itsheet;
        if((cm=el_cast_ptr_list.first())!=0)
        {
            it.toFirst();
            while((cm=it.current())!=0)
            {
                ++it;
                if(cm->getSheetNum(sh->getSheetNum()))
                {
                    QDate d(sh->getDate());
                    if(d.isValid())
                        if(!cm->isStartSet())
                        {
                               //   QMessageBox::information(this,cm->getElementName()+" "+QString::number(sh->getSheetNum()),"Prosao !isStarSet start je "+start.toString(Qt::TextDate));
                            cm->setStartDate(d.toString("dd.MMMM 'yy"));
                            cm->setStartDay(d);
                            start=d;
                            cm->setStartIsSet(true);
                        }
                    cm->incOccur();
                    if(cm->isStartSet())
                    cm->setFinishedDate(d.toString("dd.MMMM 'yy"));
                    //QMessageBox::information(this,cm->getElementName()+" "+QString::number(sh->getSheetNum()),"start je "+start.toString(Qt::TextDate)+"\n"+
                    //    "finish je "+d.toString(Qt::TextDate));
                            //break;
                  if(start.isValid())
                  {
                         total=cm->getStartDay().daysTo(d);
                      //   QMessageBox::information(this,cm->getElementName(),"total je "+QString::number(start.daysTo(d)));
                        cm->setTotalDays(total+1);

                  }
                  else
                  cm->setTotalDays(0);


                }

            }

        }
    }
     unsigned int i=0;
 while(i<=14)
  {
        PayElPtrList p_payelm;
        switch(i)
        {
            case 0: p_payelm=el_addlab_ptr_list;
                break;
            case 1: p_payelm=el_animals_ptr_list;
                break;
            case 2: p_payelm=el_anwrangler_ptr_list;
                break;
            case 3: p_payelm=el_setdress_ptr_list;
                break;
            case 4: p_payelm=el_sound_ptr_list;
                break;
            case 5: p_payelm=el_mechanical_ptr_list;
                break;
            case 6: p_payelm=el_mkupheir_ptr_list;
                break;
            case 7: p_payelm=el_music_ptr_list;
                break;
            case 8: p_payelm=el_green_ptr_list;
                break;

            case 9: p_payelm=el_specequ_ptr_list;
                break;
            case 10: p_payelm=el_secur_ptr_list;
                break;
            case 11: p_payelm=el_vieff_ptr_list;
                break;
            case 12: p_payelm=el_props_ptr_list;
                break;
            case 13: p_payelm=el_speceff_ptr_list;
                break;
            case 14: p_payelm=el_bact_ptr_list;
                break;
        }
        QPtrListIterator<PayElements> itpay(p_payelm);
        PayElements* pay_elm;
        //resetovanje broja pojavljivanja kod svih elmenata;
        if(force)
        while((pay_elm=itpay.current())!=0)
            {
                ++itpay;
                pay_elm->setOccurrence(0);
                pay_elm->setStartIsSet(false);
            }

        itsheet.toFirst();
        while((sh=itsheet.current())!=0)
        {
            if(sh->getDayBreak())
        {
             ++itsheet;
             continue;
             }
        ++itsheet;
        if((pay_elm=p_payelm.first())!=0)
        {
            itpay.toFirst();
            while((pay_elm=itpay.current())!=0)
            {
                ++itpay;
                if(pay_elm->getSheetNum(sh->getSheetNum()))
                {
                    QDate d(sh->getDate());
                    if(d.isValid())
                        if(!pay_elm->isStartSet())
                        {
                               //   QMessageBox::information(this,pay_elm->getElementName()+" "+QString::number(sh->getSheetNum()),"Prosao !isStarSet start je "+start.toString(Qt::TextDate));
                            pay_elm->setStartDate(d.toString("dd.MMMM 'yy"));
                            pay_elm->setStartDay(d);
                            start=d;
                            pay_elm->setStartIsSet(true);
                        }
                    pay_elm->incOccur();
                    if(pay_elm->isStartSet())
                    pay_elm->setFinishedDate(d.toString("dd.MMMM 'yy"));
                    //QMessageBox::information(this,pay_elm->getElementName()+" "+QString::number(sh->getSheetNum()),"start je "+start.toString(Qt::TextDate)+"\n"+
                    //    "finish je "+d.toString(Qt::TextDate));
                            //break;
                  if(start.isValid())
                  {
                         total=pay_elm->getStartDay().daysTo(d);
                      //   QMessageBox::information(this,pay_elm->getElementName(),"total je "+QString::number(start.daysTo(d)));
                        pay_elm->setTotalDays(total+1);

                  }
                  else
                  pay_elm->setTotalDays(0);


                }

            }
         }
    }
    ++i;//promena brojaca koji switch-uje categories
  }
    QPtrListIterator<Stunts> it_stunts(el_stunts_ptr_list);
    Stunts* stunts;
//resetovanje broja pojavljivanja kod svih elmenata;
   if(force)
    while((stunts=it_stunts.current())!=0)
            {
                ++it_stunts;
                stunts->setOccurrence(0);
                stunts->setStartIsSet(false);
            }

    itsheet.toFirst();
    while((sh=itsheet.current())!=0)
    {
        if(sh->getDayBreak())
        {
             ++itsheet;
             continue;
             }
        ++itsheet;
        if((stunts=el_stunts_ptr_list.first())!=0)
        {
            it_stunts.toFirst();
            while((stunts=it_stunts.current())!=0)
            {
                ++it_stunts;
                if(stunts->getSheetNum(sh->getSheetNum()))
                {
                    QDate d(sh->getDate());
                    if(d.isValid())
                        if(!stunts->isStartSet())
                        {
                               //   QMessageBox::information(this,stunts->getElementName()+" "+QString::number(sh->getSheetNum()),"Prosao !isStarSet start je "+start.toString(Qt::TextDate));
                            stunts->setStartDate(d.toString("dd.MMMM 'yy"));
                            stunts->setStartDay(d);
                            start=d;
                            stunts->setStartIsSet(true);
                        }
                    stunts->incOccur();
                    if(stunts->isStartSet())
                    stunts->setFinishedDate(d.toString("dd.MMMM 'yy"));
                    //QMessageBox::information(this,stunts->getElementName()+" "+QString::number(sh->getSheetNum()),"start je "+start.toString(Qt::TextDate)+"\n"+
                    //    "finish je "+d.toString(Qt::TextDate));
                            //break;
                  if(start.isValid())
                  {
                         total=stunts->getStartDay().daysTo(d);
                      //   QMessageBox::information(this,stunts->getElementName(),"total je "+QString::number(start.daysTo(d)));
                        stunts->setTotalDays(total+1);

                  }
                  else
                  stunts->setTotalDays(0);


                }

            }

        }
    }
  i=0;
  while(i<=4)
  {
        BuiltInCategories p_built;
        switch(i)
        {
            case 0: p_built=script_day_ptr_list;
                break;
            case 1: p_built=set_ptr_list;
                break;
            case 2: p_built=sequence_ptr_list;
                break;
            case 3: p_built=unit_ptr_list;
                break;
            case 4: p_built=location_ptr_list;
                break;
        }

        QPtrListIterator<Elements> it_built(p_built);
        Elements* built_el;
	if(force)
        while((built_el=it_built.current())!=0)
        {
            	 ++it_built;
                built_el->setOccurrence(0);
                built_el->setStartIsSet(false);
            }

        itsheet.toFirst();
        while((sh=itsheet.current())!=0)
        {
            if(sh->getDayBreak())
        {
             ++itsheet;
             continue;
             }
        ++itsheet;
        if((built_el=p_built.first())!=0)
        {
            it_built.toFirst();
            while((built_el=it_built.current())!=0)
            {
                ++it_built;
                if(built_el->getSheetNum(sh->getSheetNum()))
                {
                    QDate d(sh->getDate());
                    if(d.isValid())
                        if(!built_el->isStartSet())
                        {
                               //   QMessageBox::information(this,built_el->getElementName()+" "+QString::number(sh->getSheetNum()),"Prosao !isStarSet start je "+start.toString(Qt::TextDate));
                            built_el->setStartDate(d.toString("dd.MMMM 'yy"));
                            built_el->setStartDay(d);
                            start=d;
                            built_el->setStartIsSet(true);
                        }
                    built_el->incOccur();
                    if(built_el->isStartSet())
                    built_el->setFinishedDate(d.toString("dd.MMMM 'yy"));
                    //QMessageBox::information(this,built_el->getElementName()+" "+QString::number(sh->getSheetNum()),"start je "+start.toString(Qt::TextDate)+"\n"+
                    //    "finish je "+d.toString(Qt::TextDate));
                            //break;
                  if(start.isValid())
                  {
                         total=built_el->getStartDay().daysTo(d);
                      //   QMessageBox::information(this,built_el->getElementName(),"total je "+QString::number(start.daysTo(d)));
                        built_el->setTotalDays(total+1);

                  }
                  else
                  built_el->setTotalDays(0);


                }

            }
         }
    }
    ++i;//promena brojaca koji switch-uje kate
    }   
}


