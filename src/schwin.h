/***************************************************************************
                          schwin.h  -  description
                             -------------------
    begin                : Sat Mar 5 2005
    copyright            : (C) 2005 by Sinisa Denic
    email                : sincomp@ptt.yu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 #ifndef SCHWIN_H
 #define SCHWIN_H
 #include <qmainwindow.h>
 #include "categories.h"
 #include "sheet.h"
// #include "mojaptrlista.h"
 #include <qmessagebox.h>
class SchWin:public QMainWindow
{
    Q_OBJECT
    
    public:
        SchWin(QWidget* parent=0, const char* name=0,WFlags f=0):
        QMainWindow(parent,name,f){};
        ~SchWin(){};
        static  Sheet* current_sheet;
        void updateSheet();
     //   void updateBuiltSheet(QPtrList<Elements>&);
          void updateBuiltSheet(BuiltInCategories&);
        void unsetBuiltElemSheetNum(BuiltInCategories&);
        void unsetPayElemSheetNum();
        void updatePayElmSheet(PayElPtrList&);
     //      void updatePayElmSheet(QPtrList<PayElements>&);
        void setElementDate(Elements*);
//ako zelimo forsirano(bez obzira da li je postavljeno) postavljanje startDatuma f-ju pozivamo sa:
        void postaviDatumeZaElemente(bool); //postaviDatumeZaElemente(true);
    protected:
    static  CastPtrList el_cast_ptr_list;
    static  StuntsPtrList el_stunts_ptr_list;
    static  PayElPtrList el_bact_ptr_list,el_props_ptr_list,el_speceff_ptr_list,el_setdress_ptr_list,
    el_sound_ptr_list,el_mechanical_ptr_list,el_mkupheir_ptr_list,el_animals_ptr_list,el_anwrangler_ptr_list,
    el_music_ptr_list,el_green_ptr_list,el_specequ_ptr_list,el_secur_ptr_list,el_addlab_ptr_list,el_vieff_ptr_list;
    static  BuiltInCategories location_ptr_list,script_day_ptr_list,sequence_ptr_list,unit_ptr_list,set_ptr_list;
    static SheetsPtrList sheets_list;
    static  Sheet sheet;
};
   
 #endif

