/***************************************************************************
                          sheet.cpp  -  description
                             -------------------
    begin                : Wed Mar 9 2005
    copyright            : (C) 2005 by Sinisa Denic
    email                : sincomp@ptt.yu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include"sheet.h"
#include<qfont.h>

unsigned int Sheet::num_of_sheets=1;
QDate Sheet::glob_date;
Sheet::Sheet()
{
    setSheetNum(getNumOfSheets());
     incNumOfSheets();
     date=this->getGlobalDate();
   /* cast_list.clear();
     bact_list.clear();
     stunts_list.clear();
      addlab_list.clear();
         animals_list.clear();
         anwrangler_list.clear();
         setdress_list.clear();
         sound_list.clear();
         mecheff_list.clear();
         mkuphair_list.clear();
         music_list.clear();
         greenary_list.clear();
         specequip_list.clear();
         security_list.clear();
         viseff_list.clear();
         speceff_list.clear();
         props_list.clear(); */
         this->setDayNight(0);
         this->setIntExt(0);
    //setDayBreak(false);
    day_break=false;  
}
Sheet::Sheet(bool db)
{
    this->setSheetNum(0);
    this->setDayBreak(db);
    this->setDayNight(0);
         this->setIntExt(0);
         this->setSet("***DAY BREAK***");
    }
Sheet::Sheet( const QDomElement &e )
{
    sheet_num=e.attribute( "sheet_num","" ).toInt();
    // cast_list=e.attribute( "cast_list","" );
    // bact_list=e.attribute( "bact_list","" );
    // stunts_list=e.attribute( " stunts_list","" );
     scenes=e.attribute( "scenes","" );
     synopsis=e.attribute( "synopsis","" );
     gen_notes=e.attribute( "gen_notes","" );
     page1=e.attribute( "page1","" );
     page2=e.attribute( "page2","" );
     script_pages=e.attribute( "script_pages","" );
     location=e.attribute( "location","" );
     script_day=e.attribute( "script_day","" );
     sequence=e.attribute( "sequence","" );
     set=e.attribute( "set","" );
     unit=e.attribute( "unit","" );
     int_ext=e.attribute( "int_ext","" ).toInt();
     day_night=e.attribute( "day_night","" ).toInt();
     day_break=e.attribute( "day_break","" ).toInt();
     QString t_date= e.attribute("sheet_date","");
     date=QDate::fromString(t_date,Qt::TextDate);
}
QDomElement Sheet::SheetToXMLNode( QDomDocument &d,const Sheet& sh )
{

     QDomElement cn = d.createElement( "sheet" );
    cn.setAttribute( "sheet_num", QString::number(sh.sheet_num) );
//    cn.setAttribute( "cast_list", sh.cast_list );
  //  cn.setAttribute( "bact_list", sh.bact_list );
  //  cn.setAttribute( "stunts_list", sh.stunts_list );
  cn.setAttribute( "scenes", sh.scenes );
  cn.setAttribute( "gen_notes", sh.gen_notes);
  cn.setAttribute( "page1", sh.page1 );
  cn.setAttribute( "page2", sh.page2 );
  cn.setAttribute( "script_pages", sh.script_pages );
  cn.setAttribute( "synopsis", sh.synopsis );
  cn.setAttribute( "sequence", sh.sequence );
  cn.setAttribute( "script_day",sh.script_day);
  cn.setAttribute( "location", sh.location );
  cn.setAttribute( "set", sh.set );
  cn.setAttribute( "unit", sh.unit );
  cn.setAttribute( "int_ext", QString::number(sh.int_ext) );
  cn.setAttribute( "day_night", QString::number(sh.day_night));
  cn.setAttribute( "day_break", QString::number(sh.day_break));
  cn.setAttribute("sheet_date",sh.date.toString(Qt::TextDate));
  return cn;
}
Sheet::~Sheet(){
          //decNumOfSheets();
           cast_list.clear();
         bact_list.clear();
         stunts_list.clear();
         addlab_list.clear();
         animals_list.clear();
         anwrangler_list.clear();
         setdress_list.clear();
         sound_list.clear();
         mecheff_list.clear();
         mkuphair_list.clear();
         music_list.clear();
         greenary_list.clear();
         specequip_list.clear();
         security_list.clear();
         viseff_list.clear();
         speceff_list.clear();
         props_list.clear();
        script_day=QString::null;
         set=QString::null;
         unit=QString::null;
        location=QString::null;
         sequence=QString::null;  

      }
void Sheet::appendInfo(Sheet* s, const QDomElement &e )
{
    sheet_num=e.attribute( "sheet_num","" ).toInt();
    // cast_list=e.attribute( "cast_list","" );
    // bact_list=e.attribute( "bact_list","" );
    // stunts_list=e.attribute( " stunts_list","" );
     s->scenes=e.attribute( "scenes","" );
     s->synopsis=e.attribute( "synopsis","" );
     s->gen_notes=e.attribute( "gen_notes","" );
     s->page1=e.attribute( "page1","" );
     s->page2=e.attribute( "page2","" );
     s->script_pages=e.attribute( "script_pages","" );
     s->location=e.attribute( "location","" );
     s->script_day=e.attribute( "script_day","" );
     s->sequence=e.attribute( "sequence","" );
     s->set=e.attribute( "set","" );
     s->unit=e.attribute( "unit","" );
     s->int_ext=e.attribute( "int_ext","" ).toInt();
     s->day_night=e.attribute( "day_night","" ).toInt();
}
Sheet& Sheet::operator=(const Sheet& sh)
{
     sheet_num=sh.getSheetNum();
     cast_list=sh.cast_list;
     bact_list=sh.bact_list;
     addlab_list=sh.addlab_list;
     animals_list=sh.animals_list;
     anwrangler_list=sh.anwrangler_list;
     setdress_list=sh.setdress_list;
     sound_list=sh.sound_list;
     mecheff_list=sh.mecheff_list;
     mkuphair_list=sh.mkuphair_list;
     music_list=sh.music_list;
     greenary_list=sh.greenary_list;
     specequip_list=sh.specequip_list;
     security_list=sh.security_list;
     viseff_list=sh.viseff_list;
     speceff_list=sh.speceff_list;
     props_list=sh.props_list;
     stunts_list=sh.stunts_list;
     scenes=sh.getScenes();               
     synopsis=sh.getSynopsis();
     gen_notes=sh.getGenNotes();
     page1=sh.getPage1();
     page2=sh.getPage2();
     script_pages=sh.getScriptPages();
     location=sh.location;
     script_day=sh.script_day;
     sequence=sh.sequence;
     set=sh.set;
     unit=sh.unit;
     return *this;
}   
bool Sheet::isEmpty()
{
    bool cast_empty=0,bact_empty=0,stunts_empty=0;
    bool addlab_e=0,animals_e=0,anwrangler_e=0,greenary_e=0,
    mecheff_e=0,mkuphair_e=0,music_e=0,security_e=0,setdress_e=0,sound_e=0,speceff_e=0;
    bool specequip_e=0,viseff_e=0,props_e=0;
   // bool valid_date=0;
    if(cast_list.count()<=1) cast_empty=true;
    if(bact_list.count()<=1) bact_empty=true;
    if(stunts_list.count()<=1) stunts_empty=true;
    if(addlab_list.count()<=1) addlab_e=true;
         if(animals_list.count()<=1) animals_e=true;
         if(anwrangler_list.count()<=1) anwrangler_e=true;
         if(setdress_list.count()<=1) setdress_e=true;
         if(sound_list.count()<=1) sound_e=true;
         if(mecheff_list.count()<=1) mecheff_e=true;
         if(mkuphair_list.count()<=1) mkuphair_e=true;
         if(music_list.count()<=1) music_e =true;
         if(greenary_list.count()<=1) greenary_e=true;
         if(specequip_list.count()<=1) specequip_e=true;
         if(security_list.count()<=1)  security_e=true;
         if(viseff_list.count()<=1) viseff_e =true;
         if(speceff_list.count()<=1) speceff_e=true;
         if(props_list.count()<=1) props_e=true;
         //if(!date.isNull()) valid_date=true;
    if(cast_empty&&bact_empty&&stunts_empty&&speceff_e&&security_e&&viseff_e&&greenary_e&&
    music_e&&mkuphair_e&&mecheff_e&&sound_e&&setdress_e&&anwrangler_e&&animals_e&&addlab_e&&specequip_e&&props_e)    
    return true;
    else
    return false;
}
void Sheet::pushBackOnList(QStringList* strl,QString& id)
{
    if(strl==&addlab_list) addlab_list.push_back(id);
         if(strl==&animals_list)  animals_list.push_back(id);
         if(strl==&anwrangler_list)   anwrangler_list.push_back(id);
         if(strl==&setdress_list)     setdress_list.push_back(id);
         if(strl==&sound_list)      sound_list.push_back(id);
         if(strl==&mecheff_list)    mecheff_list.push_back(id);
         if(strl==&mkuphair_list)   mkuphair_list.push_back(id);
         if(strl==&music_list)      music_list.push_back(id);
         if(strl==&greenary_list)   greenary_list.push_back(id);
         if(strl==&specequip_list)  specequip_list.push_back(id);
         if(strl==&security_list)   security_list.push_back(id);
         if(strl==&viseff_list)     viseff_list.push_back(id);
         if(strl==&speceff_list)    speceff_list.push_back(id);
         if(strl==&props_list)      props_list.push_back(id);
         if(strl==&bact_list)       bact_list.push_back(id);
}
