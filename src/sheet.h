/***************************************************************************
                          sheet.h  -  description
                             -------------------
    begin                : Tue Mar 8 2005
    copyright            : (C) 2005 by Sinisa Denic
    email                : sincomp@ptt.yu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef SHEET_H
#define SHEET_H

#include<qdom.h>
#include<qstringlist.h>
#include<qdatetime.h>

class Sheet
{
    public:
        
        Sheet();
        Sheet(bool);
        //~Sheet();
      virtual~Sheet();
      Sheet( const QDomElement &e );
      virtual QDomElement SheetToXMLNode( QDomDocument &d,const Sheet& sh );
        Sheet& operator=(const Sheet&);

        void appendInfo(Sheet* s, const QDomElement &e );

        void setSheetNum(int sn){sheet_num=sn;}
        //void setNumOfSheets(unsigned int ns){num_of_sheets=ns;}
        void setPage1(const QString& p1){page1=p1;}
        void setPage2(const QString& p2){page2=p2;}
        void setSynopsis(const QString& sy){synopsis=sy;}
        void setScenes(const QString& sc){scenes=sc;}
        void setScriptPages(const QString& scrp){script_pages=scrp;}
        void setGenNotes(const QString& gn){gen_notes=gn;}

        void setScriptDay(const QString& scd){script_day=scd;}
        void setSet(const QString&s){set=s;}
        void setUnit(const QString& u){unit=u;}
        void setLocation(const QString& loc){location=loc;}
        void setSequence(const QString& seq){sequence=seq;}

        inline void setGlobalDate(QDate gd){glob_date=gd;}
        inline void setDate(QDate d){date=d;}
        inline void setDayBreak(bool db){ day_break=db;}
        
        void incSheetNum(){++sheet_num;}
        void decSheetNum(){--sheet_num;}
        void incNumOfSheets() {++num_of_sheets;}
        //qWarning( "sheet::incNumOfSheets" );}
        void setNumOfSheets(int n){num_of_sheets=n;}
        void decNumOfSheets() {--num_of_sheets;}
        //ovo doraditi
        void setIntExt(int h){int_ext=h;}
        void setDayNight(int dn){day_night=dn;}

        unsigned int getSheetNum()const {return sheet_num;}
        unsigned int getNumOfSheets() const {return num_of_sheets;}
        QString getPage1()const {return page1;}
        QString getPage2()const {return page2;}
        QString getScenes()const {return scenes;}
        QString getScriptPages()const {return script_pages;}
        QString getGenNotes()const {return gen_notes;}
        QString getSynopsis()const{return synopsis;}
        QString getScriptDay()const {return script_day;}
        QString getSet()const{return set;}
        QString getUnit()const{return unit;}
        QString getLocation()const{return location;}
        QString getSequence()const{return sequence;}

        inline QDate getDate()const{return date;}
        inline QDate getGlobalDate(){return glob_date;}
        inline bool getDayBreak()const{return day_break;}
        //ovo doraditi
        int getIntExt(){return int_ext;}
        QString getIntExtStr()const {if(int_ext!=0)                
                                        if(int_ext==1) return "Int";
                                        else return "Ext";
                                    else return "";}
        int getDayNight(){return day_night;}
        QString getDayNightStr()const {if(day_night!=0)
                                            if(day_night==1) return "Day";
                                            else return "Night";
                                        else return "";}
        
        bool isEmpty();
        void pushBackOnList(QStringList*,QString&);
        QStringList cast_list;
        QStringList bact_list;
        QStringList stunts_list;
        QStringList addlab_list;
        QStringList animals_list;
        QStringList anwrangler_list;
        QStringList setdress_list;
        QStringList sound_list;
        QStringList mecheff_list;
        QStringList mkuphair_list;
        QStringList music_list;
        QStringList greenary_list;
        QStringList specequip_list;
        QStringList security_list;
        QStringList viseff_list;
        QStringList speceff_list;
        QStringList props_list;
        QString script_day;
        QString set;
        QString unit;
        QString location;
        QString sequence;  
        
    private:
    static unsigned int num_of_sheets;
    static QDate glob_date;
    unsigned int sheet_num;
    QString page1;
    QString page2;
    QString scenes;
    QString script_pages;
    QString gen_notes;
    QString synopsis;
    int int_ext;
    int day_night;
    QDate date;
    bool day_break;
};

typedef QPtrList<Sheet> SheetsPtrList;
#endif
