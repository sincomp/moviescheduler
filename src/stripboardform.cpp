/****************************************************************************
** Form implementation generated from reading ui file 'stripboardform.ui'
**
** Created: Tue Apr 26 00:14:48 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.2.1   edited May 19 14:22 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "stripboardform.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include "striptrack.h"
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qptrlist.h>
#include <qpopupmenu.h>
//#include"sheet.h"
#include "application.h"
#include <qptrlist.h>
/*
 *  Constructs a StripBoardForm as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
StripBoardForm::StripBoardForm( QWidget* parent,BrkDwnShForm* brk, const char* name, WFlags fl )
    : SchWin( parent, name, fl )
{
    brkdwn=brk;
    stripTrack=0;
    lista_traka.setAutoDelete(true);
    if ( !name )
	setName( "StripBoardForm" );
    StripBoardFormLayout = new QGridLayout( this, 1, 1, 0, 6, "StripBoardFormLayout"); 

    layout10 = new QVBoxLayout( 0, 0, 6, "layout10"); 

    horizLayout = new QHBoxLayout( 0, 0, 6, "horizLayout"); 
    QSpacerItem* spacer = new QSpacerItem( 860, 16, QSizePolicy::Expanding, QSizePolicy::Minimum );
    horizLayout->addItem( spacer );

    cancelPushButton = new QPushButton(this, "cancelPushButton" );
    cancelPushButton->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, cancelPushButton->sizePolicy().hasHeightForWidth() ) );
    horizLayout->addWidget( cancelPushButton );
    QSpacerItem* spacer_2 = new QSpacerItem( 25, 10, QSizePolicy::Maximum, QSizePolicy::Minimum );
    horizLayout->addItem( spacer_2 );
    layout10->addLayout( horizLayout );
    QSpacerItem* spacer_3 = new QSpacerItem( 10, 21, QSizePolicy::Minimum, QSizePolicy::Minimum );
    layout10->addItem( spacer_3 );

    StripBoardFormLayout->addLayout( layout10, 1, 0 );

    layout11 = new QVBoxLayout( 0, 0, 0, "layout11"); 

 //ispisivanje traka u stripboardu
   
    crtajTrake();
    
 
    QSpacerItem* spacer_4 = new QSpacerItem( 16, 695, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout11->addItem( spacer_4 );

    StripBoardFormLayout->addLayout( layout11, 0, 0 );
    languageChange();
    resize( QSize(1024, 768).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( cancelPushButton, SIGNAL( clicked() ), this, SLOT( close() ) );
    
}

/*
 *  Destroys the object and frees any allocated resources
 */
StripBoardForm::~StripBoardForm()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void StripBoardForm::languageChange()
{
    setCaption( tr( "Stripboard" ) );
    cancelPushButton->setText( tr( "Close" ) );
}

BrkDwnShForm* StripBoardForm::getPointtoBrkDwn()const
{
    if(brkdwn)
    return brkdwn;
    else return 0;
}
void StripBoardForm::crtajTraku(QColor* c,const QString& s)
{
    
    frame3 = new StripTrack( this, s,"frame3" );
    lista_traka.append(frame3);
    frame3->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, frame3->sizePolicy().hasHeightForWidth() ) );
    frame3->setMinimumSize( QSize( 935, 16 ) );
    frame3->setPaletteBackgroundColor( *c );
    frame3->setFrameShape( QFrame::StyledPanel );
    frame3->setFrameShadow( QFrame::Plain );
    layout11->insertWidget( widget_pos++,frame3 );
    frame3->show();
   connect( frame3,SIGNAL(selected(const QColor&,StripTrack*)),this,SLOT(selTrackChanged(const QColor&,StripTrack*)) );
   connect( frame3,SIGNAL(rightClickOnTrack(unsigned int)),this,SLOT(openPop(unsigned int))); 
}
void StripBoardForm::crtajTrake()
{
    Sheet* sh=0;
    Sheet* cur_sh=0;
    cur_sh=brkdwn->getPointtoSheetsList()->current();
    QColor* black=new QColor(0,0,0);
    const QString dbr("*** Day Break ***");
    widget_pos=0;
   for(sh=brkdwn->getPointtoSheetsList()->first();sh!=0;sh=brkdwn->getPointtoSheetsList()->next())
   {
       if(sh->getDayBreak())
       {
        //    QMessageBox::information(this,QString::null,"DAY BREAK TRACK!!!");
         crtajTraku(black,dbr);
       }
       else
       {
        crtajTraku(brkdwn->preuzmiStripBoju(sh),
        brkdwn->getPointtoSheetsList()->current()->getDate().toString("dd. MMMM yyyy - ddd")+
        " - "+brkdwn->getPointtoSheetsList()->current()->getSet());
      // QMessageBox::information(this,QString::null,"NORMAL TRACK!!!");
       }
   }
       //if(!cur_sh->getDayBreak())
       current_sheet=brkdwn->getPointtoSheetsList()->at(brkdwn->getPointtoSheetsList()->findRef(cur_sh));
       //else current_sheet=brkdwn->getPointtoSheetsList()->at(brkdwn->getPointtoSheetsList()->find(cur_sh)-1);
       //brkdwn->getPointtoSheetsList()->first();
      
}
void StripBoardForm::selTrackChanged(const QColor& c,StripTrack* stripTrack)
{
    //QMessageBox::warning(this,QString::null,"Ovo radi!- "+QString::number(i));
    if(getStripTrack())
         getStripTrack()->setPaletteBackgroundColor(getTrackColor());
         if(getStripTrack()!=stripTrack){
        setTrackColor(c);
        //setSBFRdbrTrake(i);
        setStripTrack(stripTrack);
        stripTrack->setPaletteBackgroundColor(QColor(150,140,130));
        }
}
void StripBoardForm::openPop(unsigned int i)
{
     this->setSBFRdbrTrake(i);
    QPopupMenu* popup1 = new QPopupMenu(this);
		Q_CHECK_PTR( popup1);
		popup1->insertItem("Insert Day Break",this,SLOT(insertDayBreak(int)) );
        popup1->insertItem("Remove Day Break",this,SLOT(removeDayBreak(int)) );
		popup1->exec( QCursor::pos() );
    }
void StripBoardForm::insertDayBreak(int)
{
    Sheet* cur_sh=0;
    cur_sh=brkdwn->getPointtoSheetsList()->current();
    Sheet* s=new Sheet(true); 
    brkdwn->getPointtoSheetsList()->insert(this->getSBFRdbrTrake(),s); 
    //brkdwn->getPointtoSheetsList()->first();
    if(current_sheet->getDate().isValid())
    brkdwn->getPointtoAppWin()->setSheetsDate();
    //refreshStrip();
    if(!cur_sh->getDayBreak())
       current_sheet=brkdwn->getPointtoSheetsList()->at(brkdwn->getPointtoSheetsList()->findRef(cur_sh));
       else current_sheet=brkdwn->getPointtoSheetsList()->at(brkdwn->getPointtoSheetsList()->findRef(cur_sh)-1);
    refreshStrip();
}
void StripBoardForm::removeDayBreak(int)
{
    Sheet* cur_sh=0;
    cur_sh=brkdwn->getPointtoSheetsList()->current();
    if(brkdwn->getPointtoSheetsList()->at(this->getSBFRdbrTrake())->getDayBreak())
    {
        brkdwn->getPointtoSheetsList()->remove(this->getSBFRdbrTrake());
        brkdwn->getPointtoAppWin()->setSheetsDate();
      //  brkdwn->getPointtoAppWin()->setDateOnFrame();
//da LI CE OVO raditi bez findPrev()
        //current_sheet=brkdwn->findNext(false);
        refreshStrip();
    }
    current_sheet=brkdwn->getPointtoSheetsList()->at(brkdwn->getPointtoSheetsList()->findRef(cur_sh));
}
void StripBoardForm::refreshStrip()
{
    QPtrListIterator<StripTrack> it(lista_traka);//->clear();
    StripTrack *st;
    while((st=it.current())!=0)
    {
        ++it;
          //za laptop
          layout11->removeChild(st);
        //layout11->remove(st);
    }
    //layout11->remove((QWidget*)spacer_4);
    lista_traka.clear(); 
    this->crtajTrake();
}
