/****************************************************************************
** Form interface generated from reading ui file 'stripboardform.ui'
**
** Created: Tue Apr 26 00:14:47 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.2.1   edited May 19 14:22 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef STRIPBOARDFORM_H
#define STRIPBOARDFORM_H

#include <qvariant.h>
#include <qmessagebox.h>
#include <qguardedptr.h>
#include "brkdwnshform.h"
#include "sheet.h"
class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QPushButton;
class QFrame;
class StripTrack;
class QSpacerItem;
class StripBoardForm : public SchWin
{
    Q_OBJECT

public:
    StripBoardForm( QWidget* parent = 0,BrkDwnShForm* brk=0, const char* name = 0,  WFlags fl = 0 );
    ~StripBoardForm();
    QPushButton* cancelPushButton;
    StripTrack* frame3;
    QSpacerItem* spacer_4;
    BrkDwnShForm* getPointtoBrkDwn()const;
    inline void setTrackColor(const QColor&c){track_color=c;}
    inline QColor getTrackColor()const{return track_color;}
    inline void setSBFRdbrTrake(unsigned int nmb){sbf_rdbr_trake=nmb;}
    inline unsigned int getSBFRdbrTrake()const{return sbf_rdbr_trake;}
    inline void setStripTrack(StripTrack* st){ stripTrack=st;}
    inline StripTrack* getStripTrack()const { return stripTrack;}
public slots:
    void refreshStrip();

protected:
    QGridLayout* StripBoardFormLayout;
    QVBoxLayout* layout10;
    QHBoxLayout* horizLayout;
    QVBoxLayout* layout11;
protected slots:

    virtual void languageChange();
private slots:
    void selTrackChanged(const QColor&,StripTrack*);//{QMessageBox::warning(this,QString::null,"Ovo radi!- "+QString::number(i));}
    void openPop(unsigned int);
    void insertDayBreak(int);
    void removeDayBreak(int) ;
    void crtajTraku(QColor*,const QString&);
    void crtajTrake();
private:

    QGuardedPtr<BrkDwnShForm> brkdwn;
    int widget_pos;
    QColor track_color;
    unsigned int sbf_rdbr_trake;
    StripTrack* stripTrack;
    QPtrList<StripTrack> lista_traka;

};

#endif // STRIPBOARDFORM_H
