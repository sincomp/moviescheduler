/***************************************************************************
                          stripframe.cpp  -  description
                             -------------------
    begin                : Fri Apr 22 2005
    copyright            : (C) 2005 by Sinisa Denic
    email                : sincomp@ptt.yu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

 #include"stripframe.h"
 #include"application.h"
 #include<qpainter.h>
 #include"brkdwnshform.h"
 StripFrame::StripFrame(){}
 
 StripFrame::StripFrame(QWidget* parent,BrkDwnShForm* brk,QWidget* appws,const char* name,WFlags f):QFrame(parent,name,f)
 {
    p_parent=parent;
    ws=appws;
    brkdwn=brk;
    
 }
 
 void StripFrame::mouseDoubleClickEvent(QMouseEvent*)
 {
     brkdwn->getPointtoAppWin()->openStrip();
    
}   
 void StripFrame::paintEvent(QPaintEvent*)
 {
    QPainter p;
    p.begin(this,true);
 
                p.setPen( black );
                p.setFont( QFont( "Courier", 13, QFont::Bold) );
                if(brkdwn->current_sheet->getDate().isValid())
                p.drawText( 10,20,brkdwn->current_sheet->getDate().toString("dd. MMMM yyyy - ddd") +" - "+ brkdwn->current_sheet->getSet());
             
       p.end();
}
       
