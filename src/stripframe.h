/***************************************************************************
                          stripframe.h  -  description
                             -------------------
    begin                : Fri Apr 22 2005
    copyright            : (C) 2005 by Sinisa Denic
    email                : sincomp@ptt.yu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef STRIPFRAME_H
#define STRIPFRAME_H

#include<qframe.h>
#include<qguardedptr.h>

class BrkDwnShForm;
class StripBoardForm;
class StripFrame:public QFrame
{
    Q_OBJECT

    public:
    StripFrame();
    StripFrame(QWidget* parent=0,BrkDwnShForm* brk=0,QWidget* appws=0,const char* name=0,WFlags f=0);
    
    protected:
    void mouseDoubleClickEvent(QMouseEvent* e);
    void paintEvent(QPaintEvent*);
    private:
    BrkDwnShForm* brkdwn;
    QWidget*  p_parent;
    QWidget* ws;
    QGuardedPtr<StripBoardForm> stripBoard;
    
};
#endif
