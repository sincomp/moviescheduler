/***************************************************************************
                          striptrack.cpp  -  description
                             -------------------
    begin                : Tue Apr 26 2005
    copyright            : (C) 2005 by Sinisa Denic
    email                : sincomp@ptt.yu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 #include"striptrack.h"       
 #include"application.h"
 #include"stripboardform.h"
 #include<qpainter.h>
 //QString StripTrack::text_string;
 StripTrack::StripTrack(){}

 StripTrack::StripTrack(StripBoardForm* parent,QString t,const char* name):QFrame(parent,name)
 {
    rdbrTrake=brojTraka;
    incbrTraka();
    p_parent=parent;
    text_string=(t);
 }
UINT StripTrack::brojTraka=0;

void StripTrack::mouseDoubleClickEvent(QMouseEvent*)
 {
    
    if(p_parent->getPointtoBrkDwn())
        if(!p_parent->current_sheet->getDayBreak())
    {
            p_parent->getPointtoBrkDwn()->prikaziSheetBr(rdbrTrake);
            p_parent->getPointtoBrkDwn()->getPointtoAppWin()->showBrkdSh();
    }
 }
 void StripTrack::paintEvent(QPaintEvent*)
{
     QPainter p;
     p.begin(this);

        p.setPen( white );
        p.setFont( QFont( "Courier", 8, QFont::Bold) );
        if(p_parent->getPointtoBrkDwn()->current_sheet->getDate().isValid())
        p.drawText(3,12,text_string);
         p.end();
 }   
 void StripTrack::mousePressEvent(QMouseEvent*e)
 {
     if(e->button()==Qt::RightButton)
     emit rightClickOnTrack(rdbrTrake);
     if(e->button()==Qt::LeftButton)
     emit selected(paletteBackgroundColor(),this);
     
     }
 void StripTrack::incbrTraka()
 {
    ++brojTraka;
    }
 void StripTrack::decbrTraka()
 {
     if(brojTraka>0) --brojTraka;
     }
UINT StripTrack::getRdBrTrake() const
{
    return rdbrTrake;
    }
void StripTrack::setRdBrTrake(UINT rbt)
{
    rdbrTrake=rbt;
    }
