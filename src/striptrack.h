/***************************************************************************
                          striptrack.h  -  description
                             -------------------
    begin                : Tue Apr 26 2005
    copyright            : (C) 2005 by Sinisa Denic
    email                : sincomp@ptt.yu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 #ifndef STRIPTRACK_H
#define STRIPTRACK_H

#include<qframe.h>
class StripBoardForm;
class BrkDwnShForm;
class QColor;
typedef unsigned int UINT;
class StripTrack:public QFrame
{
    Q_OBJECT

    public:
    StripTrack();
    ~StripTrack(){decbrTraka();};
    StripTrack(StripBoardForm* parent=0,QString t=QString::null,const char* name=0);
    void incbrTraka();
    void decbrTraka();
    UINT getRdBrTrake() const;
    void setRdBrTrake(UINT rbt);
    void setTextString(const QString& st){text_string=st;}
  //  void upisiUTraku(QString&);
    protected:
    void mousePressEvent(QMouseEvent*);
    static UINT brojTraka;
    QString text_string;
    void mouseDoubleClickEvent(QMouseEvent*);
    void paintEvent(QPaintEvent*);
    signals:
        void rightClickOnTrack(unsigned int);
        void selected(const QColor&,StripTrack*);
    private:
    unsigned int rdbrTrake;
    BrkDwnShForm* brkdwn;
    StripBoardForm* p_parent;

};
   
#endif
