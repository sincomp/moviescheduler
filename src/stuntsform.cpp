/****************************************************************************
** Form implementation generated from reading ui file 'stuntsform.ui'
**
** Created: Tue Mar 1 02:00:15 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.2.1   edited May 19 14:22 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "stuntsform.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qcheckbox.h>
#include <qgroupbox.h>
#include <qlineedit.h>
#include <qtextedit.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

/*
 *  Constructs a StuntsForm as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
StuntsForm::StuntsForm( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
	setName( "StuntsForm" );
    //setModal( TRUE );
    StuntsFormLayout = new QGridLayout( this, 1, 1, 11, 6, "StuntsFormLayout"); 
    QSpacerItem* spacer = new QSpacerItem( 392, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    StuntsFormLayout->addMultiCell( spacer, 6, 6, 0, 2 );

    okPushButton = new QPushButton( this, "okPushButton" );

    StuntsFormLayout->addWidget( okPushButton, 6, 3 );

    cancelPushButton = new QPushButton( this, "cancelPushButton" );

    StuntsFormLayout->addWidget( cancelPushButton, 6, 4 );

    TextLabel2 = new QLabel( this, "TextLabel2" );

    StuntsFormLayout->addWidget( TextLabel2, 1, 0 );

    TextLabel1 = new QLabel( this, "TextLabel1" );

    StuntsFormLayout->addWidget( TextLabel1, 0, 0 );

    lockIDCheckBox = new QCheckBox( this, "lockIDCheckBox" );

    StuntsFormLayout->addWidget( lockIDCheckBox, 1, 2 );

    GroupBox1 = new QGroupBox( this, "GroupBox1" );
    GroupBox1->setFrameShape( QGroupBox::Box );
    GroupBox1->setFrameShadow( QGroupBox::Sunken );

    allowHDayCheckBox1 = new QCheckBox( GroupBox1, "allowHDayCheckBox1" );
    allowHDayCheckBox1->setGeometry( QRect( 20, 30, 120, 21 ) );

    allowDropCheckBox2 = new QCheckBox( GroupBox1, "allowDropCheckBox2" );
    allowDropCheckBox2->setGeometry( QRect( 20, 60, 170, 21 ) );

    TextLabel5 = new QLabel( GroupBox1, "TextLabel5" );
    TextLabel5->setGeometry( QRect( 250, 50, 190, 40 ) );

    minDayLineEdit10 = new QLineEdit( GroupBox1, "minDayLineEdit10" );
    minDayLineEdit10->setGeometry( QRect( 450, 60, 40, 23 ) );

    StuntsFormLayout->addMultiCellWidget( GroupBox1, 3, 3, 0, 4 );

    excFromCheckBox = new QCheckBox( this, "excFromCheckBox" );

    StuntsFormLayout->addMultiCellWidget( excFromCheckBox, 2, 2, 0, 2 );

    GroupBox2 = new QGroupBox( this, "GroupBox2" );

    TextLabel6 = new QLabel( GroupBox2, "TextLabel6" );
    TextLabel6->setGeometry( QRect( 10, 20, 111, 21 ) );

    TextLabel7 = new QLabel( GroupBox2, "TextLabel7" );
    TextLabel7->setGeometry( QRect( 10, 60, 135, 21 ) );

    totalOccLineEdit = new QLineEdit( GroupBox2, "totalOccLineEdit" );
    totalOccLineEdit->setEnabled( FALSE );
    totalOccLineEdit->setGeometry( QRect( 130, 20, 50, 23 ) );

    actSchStartLineEdit = new QLineEdit( GroupBox2, "actSchStartLineEdit" );
    actSchStartLineEdit->setEnabled( FALSE );
    actSchStartLineEdit->setGeometry( QRect( 140, 60, 90, 23 ) );

    TextLabel8 = new QLabel( GroupBox2, "TextLabel8" );
    TextLabel8->setGeometry( QRect( 240, 60, 43, 21 ) );

    finishLineEdit = new QLineEdit( GroupBox2, "finishLineEdit" );
    finishLineEdit->setEnabled( FALSE );
    finishLineEdit->setGeometry( QRect( 280, 60, 100, 23 ) );

    TextLabel9 = new QLabel( GroupBox2, "TextLabel9" );
    TextLabel9->setGeometry( QRect( 400, 60, 71, 21 ) );

    totalDaysLineEdit = new QLineEdit( GroupBox2, "totalDaysLineEdit" );
    totalDaysLineEdit->setEnabled( FALSE );
    totalDaysLineEdit->setGeometry( QRect( 470, 60, 40, 23 ) );

    StuntsFormLayout->addMultiCellWidget( GroupBox2, 4, 4, 0, 4 );

    GroupBox3 = new QGroupBox( this, "GroupBox3" );

    textLabel2 = new QLabel( GroupBox3, "textLabel2" );
    textLabel2->setGeometry( QRect( 10, 110, 61, 21 ) );

    textLabel1 = new QLabel( GroupBox3, "textLabel1" );
    textLabel1->setGeometry( QRect( 10, 40, 90, 21 ) );

    QWidget* privateLayoutWidget = new QWidget( GroupBox3, "layout5" );
    privateLayoutWidget->setGeometry( QRect( 140, 20, 350, 152 ) );
    layout5 = new QVBoxLayout( privateLayoutWidget, 11, 6, "layout5"); 

    contactTextEdit = new QTextEdit( privateLayoutWidget, "contactTextEdit" );
    layout5->addWidget( contactTextEdit );

    notesTextEdit = new QTextEdit( privateLayoutWidget, "notesTextEdit" );
    layout5->addWidget( notesTextEdit );

    StuntsFormLayout->addMultiCellWidget( GroupBox3, 5, 5, 0, 4 );

    nameLineEdit = new QLineEdit( this, "nameLineEdit" );
    nameLineEdit->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)7, (QSizePolicy::SizeType)0, 0, 0, nameLineEdit->sizePolicy().hasHeightForWidth() ) );

    StuntsFormLayout->addMultiCellWidget( nameLineEdit, 0, 0, 1, 3 );

    boardIDLineEdit = new QLineEdit( this, "boardIDLineEdit" );
    boardIDLineEdit->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0, boardIDLineEdit->sizePolicy().hasHeightForWidth() ) );

    StuntsFormLayout->addWidget( boardIDLineEdit, 1, 1 );
    languageChange();
    resize( QSize(575, 581).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( okPushButton, SIGNAL( clicked() ), this, SLOT( napuniKategoriju() ) );
    connect( cancelPushButton, SIGNAL( clicked() ), this, SLOT( reject() ) );

    // tab order
    setTabOrder( nameLineEdit, boardIDLineEdit );
    setTabOrder( boardIDLineEdit, lockIDCheckBox );
    setTabOrder( lockIDCheckBox, excFromCheckBox );
    setTabOrder( excFromCheckBox, contactTextEdit );
    setTabOrder( contactTextEdit, notesTextEdit );
    setTabOrder( notesTextEdit, allowHDayCheckBox1 );
    setTabOrder( allowHDayCheckBox1, allowDropCheckBox2 );
    setTabOrder( allowDropCheckBox2, minDayLineEdit10 );
    setTabOrder( minDayLineEdit10, okPushButton );
    setTabOrder( okPushButton, cancelPushButton );
    setTabOrder( cancelPushButton, totalOccLineEdit );
    setTabOrder( totalOccLineEdit, actSchStartLineEdit );
    setTabOrder( actSchStartLineEdit, finishLineEdit );
    setTabOrder( finishLineEdit, totalDaysLineEdit );
}

/*
 *  Destroys the object and frees any allocated resources
 */
StuntsForm::~StuntsForm()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void StuntsForm::languageChange()
{
    setCaption( tr( "Edit Element:" ) );
    okPushButton->setText( tr( "&OK" ) );
    okPushButton->setAccel( QKeySequence( tr( "Alt+O" ) ) );
    cancelPushButton->setText( tr( "&Cancel" ) );
    cancelPushButton->setAccel( QKeySequence( tr( "Alt+C" ) ) );
    TextLabel2->setText( tr( "Board ID" ) );
    TextLabel1->setText( tr( "Element Name" ) );
    lockIDCheckBox->setText( tr( "Lock ID" ) );
    GroupBox1->setTitle( tr( "Day Out Of Days" ) );
    allowHDayCheckBox1->setText( tr( "Allow Hold Days" ) );
    allowDropCheckBox2->setText( tr( "Allow Drop/Pickup Days" ) );
    TextLabel5->setText( tr( "Min.Days Between Drop/Pickup" ) );
    excFromCheckBox->setText( tr( "Exclude From Stripboard" ) );
    GroupBox2->setTitle( tr( "Usage" ) );
    TextLabel6->setText( tr( "Total Occurrences:" ) );
    TextLabel7->setText( tr( "Active Schedule Start:" ) );
    TextLabel8->setText( tr( "Finish:" ) );
    TextLabel9->setText( tr( "Total Days:" ) );
    GroupBox3->setTitle( tr( "Element Properties" ) );
    textLabel2->setText( tr( "Notes" ) );
    textLabel1->setText( tr( "Contact Names" ) );
}

void StuntsForm::initForm()
{
    qWarning( "StuntsForm::initForm(): Not implemented yet" );
}

void StuntsForm::napuniKategoriju()
{
    qWarning( "StuntsForm::napuniKategoriju(): Not implemented yet" );
}

