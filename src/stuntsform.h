/****************************************************************************
** Form interface generated from reading ui file 'stuntsform.ui'
**
** Created: Tue Mar 1 02:00:15 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.2.1   edited May 19 14:22 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef STUNTSFORM_H
#define STUNTSFORM_H

#include <qvariant.h>
#include <qdialog.h>
#include "elmanform.h"

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QPushButton;
class QLabel;
class QCheckBox;
class QGroupBox;
class QLineEdit;
class QTextEdit;

class StuntsForm : public QDialog
{
    Q_OBJECT

public:
    StuntsForm( QWidget* parent = 0, const char* name = 0, bool modal = TRUE, WFlags fl = 0 );
    ~StuntsForm();

    QPushButton* okPushButton;
    QPushButton* cancelPushButton;
    QLabel* TextLabel2;
    QLabel* TextLabel1;
    QCheckBox* lockIDCheckBox;
    QGroupBox* GroupBox1;
    QCheckBox* allowHDayCheckBox1;
    QCheckBox* allowDropCheckBox2;
    QLabel* TextLabel5;
    QLineEdit* minDayLineEdit10;
    QCheckBox* excFromCheckBox;
    QGroupBox* GroupBox2;
    QLabel* TextLabel6;
    QLabel* TextLabel7;
    QLineEdit* totalOccLineEdit;
    QLineEdit* actSchStartLineEdit;
    QLabel* TextLabel8;
    QLineEdit* finishLineEdit;
    QLabel* TextLabel9;
    QLineEdit* totalDaysLineEdit;
    QGroupBox* GroupBox3;
    QLabel* textLabel2;
    QLabel* textLabel1;
    QTextEdit* contactTextEdit;
    QTextEdit* notesTextEdit;
    QLineEdit* nameLineEdit;
    QLineEdit* boardIDLineEdit;

public slots:
    virtual void initForm();
    virtual void napuniKategoriju();

protected:
    StuntsPtrList* un_stunts_ptr_list;
    Stunts* stunts;
    int broj_elementa;
    ElManForm* p_parent;

    QGridLayout* StuntsFormLayout;
    QVBoxLayout* layout5;

protected slots:
    virtual void languageChange();

};

#endif // STUNTSFORM_H
