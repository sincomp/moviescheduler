#include "stuntsformimpl.h"
#include<qlineedit.h>
#include<qcheckbox.h>
#include<qtextedit.h>
#include "application.h"
/////////////////////////////////////////////////////////
//definisanje globalne promenljive za redni broj elemanta
bool prvi_stunt_el=TRUE;
/////////////////////////////////////////////////////////
/* 
 *  Constructs a StuntsFormImpl which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f' 
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
StuntsFormImpl::StuntsFormImpl(StuntsPtrList *spl, ElManForm* parent,int br_el, const char* name, bool modal, WFlags fl)
    : StuntsForm(parent, name, modal, fl )
{
	broj_elementa=br_el;
    un_stunts_ptr_list=spl;
    un_stunts_ptr_list->setAutoDelete(TRUE);
    p_parent=parent;
    nameLineEdit->setText("New Element");
    this->nameLineEdit->setFocus();
    initForm();
}

/*  
 *  Destroys the object and frees any allocated resources
 */
/*StuntsFormImpl::~StuntsFormImpl()
{
  */

/*
 * public slot
 */
void StuntsFormImpl::initForm()
{
	if(broj_elementa>=0)
    {
        Stunts *stunt;
        stunt=un_stunts_ptr_list->at(broj_elementa);
        nameLineEdit->setText(QString(stunt->getElementName()) );
        boardIDLineEdit->setText(stunt->getBoardID());
        lockIDCheckBox->setChecked(stunt->getLocked() );
        excFromCheckBox->setChecked(stunt->getExcludeFromStripBoard() );
        contactTextEdit->setText(stunt->getContactNames());
        notesTextEdit->setText(stunt->getNotes() );
    if(stunt->isStartSet())
    {
        totalOccLineEdit->setEnabled( true );
        actSchStartLineEdit->setEnabled( true );
        finishLineEdit->setEnabled( true );
        totalDaysLineEdit->setEnabled( true );
        totalOccLineEdit->setText(QString::number(stunt->getOccurrence()));
        actSchStartLineEdit->setText(stunt->getStartDate());
        finishLineEdit->setText(stunt->getFinishedDate());
        totalDaysLineEdit->setText(QString::number(stunt->getTotalDays()));
    }
    else
    {
        totalOccLineEdit->setEnabled( false );
        actSchStartLineEdit->setEnabled( false);
        finishLineEdit->setEnabled( false);
        totalDaysLineEdit->setEnabled( false);
    }
    }
    //qWarning( "StuntsFormImpl::initForm() not yet implemented!" );
}

/*
 * public slot
 */

void StuntsFormImpl::napuniKategoriju()
{

   if((stunts=un_stunts_ptr_list->at(broj_elementa))==0)
        stunts=new Stunts;
        //else cast=un_stunts_ptr_list->at(broj_elementa);
        //stunts->setSheetNum(p_parent->current_sheet->getSheetNum());
        //stunts->setSheetNum(0);
        stunts->setBoardID(boardIDLineEdit->text());
        stunts->setElementName(nameLineEdit->text());
        stunts->setLocked(lockIDCheckBox->isChecked());
        stunts->setExcludeFromStripBoard(excFromCheckBox->isChecked());
        stunts->setContactNames(contactTextEdit->text() );
        stunts->setNotes(notesTextEdit->text() );
       /* QDate d=p_parent->getPointtoAppWin()->current_sheet->getDate();
        if(d.isValid())
            stunts->setStartDate(d.toString("dd.MMMM 'yy"));   */
        //unsigned int s=0;
		if(broj_elementa<0)
		{
			if(proveriID())
			{
				un_stunts_ptr_list->append(stunts);
				//s=1;
				this->accept();
                p_parent->updateSheet();
                p_parent->initTable();
            }
		}
		if(broj_elementa>=0)
		{
		 	if(proveriID())
          {                                  
//			un_stunts_ptr_list->replace(broj_elementa,stunts);
    //  un_stunts_ptr_list->remove(broj_elementa);
    //un_stunts_ptr_list->remove(broj_elementa);
    //un_stunts_ptr_list->insert(broj_elementa,stunts); 
    
           this->accept();
            p_parent->updateSheet();
            p_parent->initTable();
           }
		}
}
bool StuntsFormImpl::proveriID()
 {
     unsigned int i=0;
	if(stunts->getBoardID()!="")
  	{
        if(prvi_stunt_el)
            {
                prvi_stunt_el=FALSE;
            }
        else
            {
                for(i=0; i<un_stunts_ptr_list->count(); ++i)
                {

                if(((int)i==broj_elementa) && (i<un_stunts_ptr_list->count())) ++i;
                        if(i==un_stunts_ptr_list->count())
                         return true;
                if( (un_stunts_ptr_list->at(i)->getBoardID() )==( boardIDLineEdit->text()) )
                    {
                        QMessageBox::warning(this,"Linux Scheduler",
                        "Uneli ste identifikacioni broj koji vec postoji!\n Mozete promeniti vrednost polja Borad ID.");
                        return false;
                        boardIDLineEdit->setFocus();
                    }
                }
					return true;
              }  
    }
    else
    stunts->setBoardID("");
	return true;

}
