#ifndef STUNTSFORMIMPL_H
#define STUNTSFORMIMPL_H
#include "stuntsform.h"
#include <qmessagebox.h>
class StuntsFormImpl : public StuntsForm
{
    Q_OBJECT

public:
    StuntsFormImpl(StuntsPtrList *spl=0, ElManForm* parent = 0,int br_el=-1, const char* name = 0, bool modal = TRUE, WFlags fl = 0 );

public slots:
    void initForm();
    void napuniKategoriju();
    bool proveriID();
};

#endif // STUNTSFORMIMPL_H
