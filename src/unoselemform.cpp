/****************************************************************************
** Form implementation generated from reading ui file 'unoselemform.ui'
**
** Created: Mon Jan 24 23:52:02 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.2.1   edited May 19 14:22 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "unoselemform.h"

#include <qvariant.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qcheckbox.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

#include "./unoselemform.ui.h"
/*
 *  Constructs a UnosElemForm as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
UnosElemForm::UnosElemForm(CastPtrList *cpl, ElManForm* parent,int br_el, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
//////////////////////////////////////////////////////////////////
    broj_elementa=br_el;
	un_cast_ptr_list=cpl;
    un_cast_ptr_list->setAutoDelete(TRUE);
    p_parent=parent;
//////////////////////////////////////////////////////////////////
    if ( !name )
	setName( "UnosElemForm" );
    //setModal( TRUE );

    GroupBox1 = new QGroupBox( this, "GroupBox1" );
    GroupBox1->setGeometry( QRect( 20, 130, 500, 100 ) );

    TextLabel5 = new QLabel( GroupBox1, "TextLabel5" );
    TextLabel5->setGeometry( QRect( 250, 60, 190, 21 ) );

    allowHoldDaysCheckBox1 = new QCheckBox( GroupBox1, "allowHoldDaysCheckBox1" );
    allowHoldDaysCheckBox1->setGeometry( QRect( 20, 30, 120, 21 ) );

    allow_Drop_Pickup_DaysCheckBox2 = new QCheckBox( GroupBox1, "allow_Drop_Pickup_DaysCheckBox2" );
    allow_Drop_Pickup_DaysCheckBox2->setGeometry( QRect( 20, 60, 170, 21 ) );

    min_Days_Between_Drop_PickupLineEdit10 = new QLineEdit( GroupBox1, "min_Days_Between_Drop_PickupLineEdit10" );
    min_Days_Between_Drop_PickupLineEdit10->setGeometry( QRect( 445, 60, 40, 23 ) );

    TextLabel2 = new QLabel( this, "TextLabel2" );
    TextLabel2->setGeometry( QRect( 20, 50, 55, 21 ) );

    TextLabel1 = new QLabel( this, "TextLabel1" );
    TextLabel1->setGeometry( QRect( 20, 10, 90, 21 ) );

    lockIDCheckBox = new QCheckBox( this, "CheckBox4" );
    lockIDCheckBox->setGeometry( QRect( 150, 50, 90, 21 ) );

    QWidget* privateLayoutWidget = new QWidget( this, "Layout8" );
    privateLayoutWidget->setGeometry( QRect( 380, 530, 210, 26 ) );
    Layout8 = new QHBoxLayout( privateLayoutWidget, 0, 6, "Layout8"); 

    okPushButton = new QPushButton( privateLayoutWidget, "okPushButton" );
    Layout8->addWidget( okPushButton );
    QSpacerItem* spacer = new QSpacerItem( 160, 0, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout8->addItem( spacer );

    cancelPushButton = new QPushButton( privateLayoutWidget, "cancelPushButton" );
    Layout8->addWidget( cancelPushButton );

    GroupBox3 = new QGroupBox( this, "GroupBox3" );
    GroupBox3->setGeometry( QRect( 20, 360, 570, 170 ) );
    GroupBox3->setColumnLayout(0, Qt::Vertical );
    GroupBox3->layout()->setSpacing( 6 );
    GroupBox3->layout()->setMargin( 11 );
    GroupBox3Layout = new QGridLayout( GroupBox3->layout() );
    GroupBox3Layout->setAlignment( Qt::AlignTop );

    TextLabel12 = new QLabel( GroupBox3, "TextLabel12" );

    GroupBox3Layout->addWidget( TextLabel12, 2, 0 );

    TextLabel13 = new QLabel( GroupBox3, "TextLabel13" );

    GroupBox3Layout->addWidget( TextLabel13, 3, 0 );

    TextLabel14 = new QLabel( GroupBox3, "TextLabel14" );

    GroupBox3Layout->addWidget( TextLabel14, 4, 0 );

    adressLineEdit = new QLineEdit( GroupBox3, "adressLineEdit17" );

    GroupBox3Layout->addWidget( adressLineEdit, 1, 1 );

    phoneLineEdit = new QLineEdit( GroupBox3, "phoneLineEdit18" );

    GroupBox3Layout->addWidget( phoneLineEdit, 2, 1 );

    agentLineEdit = new QLineEdit( GroupBox3, "agentLineEdit19" );

    GroupBox3Layout->addWidget( agentLineEdit, 3, 1 );

    agentPhLineEdit = new QLineEdit( GroupBox3, "agentPhLineEdit20" );

    GroupBox3Layout->addWidget( agentPhLineEdit, 4, 1 );

    TextLabel10 = new QLabel( GroupBox3, "TextLabel10" );

    GroupBox3Layout->addWidget( TextLabel10, 0, 0 );

    TextLabel11 = new QLabel( GroupBox3, "TextLabel11" );

    GroupBox3Layout->addWidget( TextLabel11, 1, 0 );

    fulNameLineEdit = new QLineEdit( GroupBox3, "fulNameLineEdit" );

    GroupBox3Layout->addWidget( fulNameLineEdit, 0, 1 );

    nameLineEdit = new QLineEdit( this, "nameLineEdit" );
    nameLineEdit->setGeometry( QRect( 130, 10, 350, 23 ) );
    nameLineEdit->setText("New Element");
    nameLineEdit->setFocus();
    
    boardIDLineEdit = new QLineEdit( this, "boardIDLineEdit" );
    boardIDLineEdit->setGeometry( QRect( 80, 50, 51, 23 ) );

    GroupBox2 = new QGroupBox( this, "GroupBox2" );
    GroupBox2->setGeometry( QRect( 20, 250, 570, 91 ) );

    TextLabel6 = new QLabel( GroupBox2, "TextLabel6" );
    TextLabel6->setGeometry( QRect( 10, 20, 111, 21 ) );

    TextLabel7 = new QLabel( GroupBox2, "TextLabel7" );
    TextLabel7->setGeometry( QRect( 10, 60, 135, 21 ) );

    TextLabel8 = new QLabel( GroupBox2, "TextLabel8" );
    TextLabel8->setGeometry( QRect( 260, 60, 43, 21 ) );

    LineEdit13 = new QLineEdit( GroupBox2, "LineEdit13" );
    LineEdit13->setEnabled( FALSE );
    LineEdit13->setGeometry( QRect( 300, 60, 125, 23 ) );

    LineEdit11 = new QLineEdit( GroupBox2, "LineEdit11" );
    LineEdit11->setEnabled( FALSE );
    LineEdit11->setGeometry( QRect( 130, 20, 50, 23 ) );

    TextLabel9 = new QLabel( GroupBox2, "TextLabel9" );
    TextLabel9->setGeometry( QRect( 430, 60, 71, 21 ) );

    LineEdit14 = new QLineEdit( GroupBox2, "LineEdit14" );
    LineEdit14->setEnabled( FALSE );
    LineEdit14->setGeometry( QRect( 500, 60, 60, 23 ) );

    LineEdit12 = new QLineEdit( GroupBox2, "LineEdit12" );
    LineEdit12->setEnabled( FALSE );
    LineEdit12->setGeometry( QRect( 140, 60, 110, 23 ) );

    excFromCheckBox = new QCheckBox( this, "excFromCheckBox" );
    excFromCheckBox->setGeometry( QRect( 20, 90, 170, 21 ) );
    languageChange();
    resize( QSize(600, 566).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( cancelPushButton, SIGNAL( clicked() ), this, SLOT( reject() ) );
    connect( okPushButton, SIGNAL( clicked() ), this, SLOT( napuniKategoriju() ) );
  
      // tab order
    setTabOrder( nameLineEdit, boardIDLineEdit );
    setTabOrder( boardIDLineEdit, lockIDCheckBox );
    setTabOrder( lockIDCheckBox, excFromCheckBox );
    initForm();
}
/*
 *  Destroys the object and frees any allocated resources
 */
UnosElemForm::~UnosElemForm()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void UnosElemForm::languageChange()
{
    setCaption( tr( "Edit Element:" ) );
    GroupBox1->setTitle( tr( "Day Out Of Days" ) );
    TextLabel5->setText( tr( "Min.Days Between Drop/Pickup" ) );
    allowHoldDaysCheckBox1->setText( tr( "Allow Hold Days" ) );
    allow_Drop_Pickup_DaysCheckBox2->setText( tr( "Allow Drop/Pickup Days" ) );
    TextLabel2->setText( tr( "Board ID" ) );
    TextLabel1->setText( tr( "Element Name" ) );
    lockIDCheckBox->setText( tr( "Lock ID" ) );
    okPushButton->setText( tr( "&OK" ) );
    okPushButton->setAccel( QKeySequence( tr( "Alt+O" ) ) );
    cancelPushButton->setText( tr( "&Cancel" ) );
    cancelPushButton->setAccel( QKeySequence( tr( "Alt+C" ) ) );
    GroupBox3->setTitle( tr( "Element Properties" ) );
    TextLabel12->setText( tr( "Phone" ) );
    TextLabel13->setText( tr( "Agent" ) );
    TextLabel14->setText( tr( "Agent Ph" ) );
    TextLabel10->setText( tr( "Full Name" ) );
    TextLabel11->setText( tr( "Address" ) );
    GroupBox2->setTitle( tr( "Usage" ) );
    TextLabel6->setText( tr( "Total Occurrences:" ) );
    TextLabel7->setText( tr( "Active Schedule Start:" ) );
    TextLabel8->setText( tr( "Finish:" ) );
    TextLabel9->setText( tr( "Total Days:" ) );
    excFromCheckBox->setText( tr( "Exclude From Stripboard" ) );
}

