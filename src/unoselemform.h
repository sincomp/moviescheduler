/****************************************************************************
** Form interface generated from reading ui file 'unoselemform.ui'
**
** Created: Mon Jan 24 23:52:01 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.2.1   edited May 19 14:22 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef UNOSELEMFORM_H
#define UNOSELEMFORM_H

#include <qvariant.h>
#include <qdialog.h>
#include "elmanform.h"
class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QGroupBox;
class QLabel;
class QCheckBox;
class QLineEdit;
class QPushButton;

class UnosElemForm : public QDialog
{
    Q_OBJECT

public:
    UnosElemForm( CastPtrList *cpl=0, ElManForm* parent = 0,int br_el=-1, const char* name = 0, bool modal = TRUE, WFlags fl = 0 );

    
    ~UnosElemForm();

    QGroupBox* GroupBox1;
    QLabel* TextLabel5;
    QCheckBox* allowHoldDaysCheckBox1;
    QCheckBox* allow_Drop_Pickup_DaysCheckBox2;
    QLineEdit* min_Days_Between_Drop_PickupLineEdit10;
    QLabel* TextLabel2;
    QLabel* TextLabel1;
    QCheckBox* lockIDCheckBox;
    QPushButton* okPushButton;
    QPushButton* cancelPushButton;
    QGroupBox* GroupBox3;
    QLabel* TextLabel12;
    QLabel* TextLabel13;
    QLabel* TextLabel14;
    QLineEdit* adressLineEdit;
    QLineEdit* phoneLineEdit;
    QLineEdit* agentLineEdit;
    QLineEdit* agentPhLineEdit;
    QLabel* TextLabel10;
    QLabel* TextLabel11;
    QLineEdit* fulNameLineEdit;
    QLineEdit* nameLineEdit;
    QLineEdit* boardIDLineEdit;
    QGroupBox* GroupBox2;
    QLabel* TextLabel6;
    QLabel* TextLabel7;
    QLabel* TextLabel8;
    QLineEdit* LineEdit13;
    QLineEdit* LineEdit11;
    QLabel* TextLabel9;
    QLineEdit* LineEdit14;
    QLineEdit* LineEdit12;
    QCheckBox* excFromCheckBox;
    
public slots:
    virtual void napuniKategoriju();
    virtual void initForm();
    bool proveriID();
protected:
    //ElManForm*  p_el_mng;
    QHBoxLayout* Layout8;
    QGridLayout* GroupBox3Layout;

protected slots:
    virtual void languageChange();
private:
    int broj_elementa;
  ElManForm* p_parent;
	CastMembers *cast;
	CastPtrList *un_cast_ptr_list;

};

#endif // UNOSELEMFORM_H
