/***************************************************************************
                          unoselemform.ui.h  -  description
                             -------------------
    begin                : Thu Mar 10 2005
    copyright            : (C) 2005 by Sinisa Denic
    email                : sincomp@ptt.yu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include<qmessagebox.h>

/////////////////////////////////////////////////////////
//definisanje globalne promenljive za redni broj elemanta
bool prvi_el=false;
/////////////////////////////////////////////////////////

void UnosElemForm ::napuniKategoriju()
{
    if((cast=un_cast_ptr_list->at(broj_elementa))==0)  //ako ne postoji element pod broj_elementa
        cast=new CastMembers;      //element se ne edituje vec kreiraj novi
        else cast=un_cast_ptr_list->at(broj_elementa); //ako postoji samo preuzmi njegovu adresu
        //cast->setSheetNum(p_parent->current_sheet->getSheetNum());
        //cast->setSheetNum(0);
        cast->setBoardID(boardIDLineEdit->text());
        cast->setElementName(nameLineEdit->text());
        cast->setLocked(lockIDCheckBox->isChecked());
        cast->setExcludeFromStripBoard(excFromCheckBox->isChecked());
        cast->setFullName(fulNameLineEdit->text());
        cast->setAddress(adressLineEdit->text());
        cast->setPhone(phoneLineEdit->text());
        cast->setAgent(agentLineEdit->text());
        cast->setAgentPhone(agentPhLineEdit->text());
        /////////////////////////////postavljanje datuma za Cast
    /*    QDate d=p_parent->getPointtoAppWin()->current_sheet->getDate();
        if(d.isValid())
            cast->setStartDate(d.toString("dd.MMMM 'yy"));
        cast->incOccur();      */
        //unsigned int s=0;
       
        if(broj_elementa<0)//novi element  
		{

            if(proveriID())
			{
				un_cast_ptr_list->append(cast);
				//s=1;
				this->accept();
                 p_parent->updateSheet();
                p_parent->initTable();
            }
		}
		if(broj_elementa>=0)//element se edituje
		{
            if(proveriID())
            {
		//	un_cast_ptr_list->remove(broj_elementa);
    //  un_cast_ptr_list->insert(broj_elementa,cast);// prepisi element preko postojeceg
//            un_cast_ptr_list->replace(broj_elementa,cast);
            this->accept();
             p_parent->updateSheet();
             p_parent->initTable();
           }
		}
        
  //trebalo bi da se ubaci pokazivac na BrkDwnShForm i da se pozove initForm
  //jer kada se izbrise ili izmeni neki element potrebno ga je prikazati ispravno     


}
void UnosElemForm ::initForm()
{
    if(broj_elementa>=0)
    {
        CastMembers *cast_member;
        cast_member=un_cast_ptr_list->at(broj_elementa);
        nameLineEdit->setText(QString(cast_member->getElementName()) );
        //boardIDLineEdit->setText(QString::number(cast_member->getBoardID() ) );
        boardIDLineEdit->setText(cast_member->getBoardID() );
        lockIDCheckBox->setChecked(cast_member->getLocked() );
        excFromCheckBox->setChecked(cast_member->getExcludeFromStripBoard() );
        fulNameLineEdit->setText(cast_member->getFullName() );
        adressLineEdit->setText(cast_member->getAddress());
        phoneLineEdit->setText(cast_member->getPhone() );
        agentLineEdit->setText(cast_member->getAgent());
        agentPhLineEdit->setText(cast_member->getAgentPhone() );
    
    if(cast_member->isStartSet())
    {
        LineEdit11->setEnabled( true );
        LineEdit12->setEnabled( true );
        LineEdit13->setEnabled( true );
        LineEdit14->setEnabled( true );
        LineEdit11->setText(QString::number(cast_member->getOccurrence()));
        LineEdit12->setText(cast_member->getStartDate());
        LineEdit13->setText(cast_member->getFinishedDate());
        LineEdit14->setText(QString::number(cast_member->getTotalDays()));
    }
    else
    {
        LineEdit11->setEnabled( false );
        LineEdit12->setEnabled( false);
        LineEdit13->setEnabled( false);
        LineEdit14->setEnabled( false);
    }
    }  
}
bool UnosElemForm::proveriID()
{
    unsigned int i=0;
	if(cast->getBoardID()!="") //element ima boardID
  	{
        if(un_cast_ptr_list->first())//postoji li prvi element u listi
            {
                prvi_el=true;
            }
        if(prvi_el)     // lista nije prazna
            {
                for(i=0; i<un_cast_ptr_list->count(); ++i)
                {
                    if(((int)i==broj_elementa) && (i<un_cast_ptr_list->count())) ++i;
                        if(i==un_cast_ptr_list->count())
                            return true;
                    if( (un_cast_ptr_list->at(i)->getBoardID() )==( boardIDLineEdit->text()) )
                    {
                        
                        QMessageBox::warning(this,"Linux Scheduler",
                        "Uneli ste identifikacioni broj koji vec postoji!\n Mozete promeniti vrednost polja Borad ID.");
                        boardIDLineEdit->setFocus();
                        return false;
                    }
                       
                }
               	return true;
                
                
            }      
            
    }
    else  //element nema boardID
    cast->setBoardID("");
	return true;
}
